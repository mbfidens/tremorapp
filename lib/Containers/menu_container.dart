import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:Tremor_App/Widgets/menu_item_widget.dart';

class MenuContainer extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
        return getItemsList(context);
    }

    Widget getItemsList(BuildContext context) => Column(
        children: List.generate(
            getListLength(context),
                (index) => Container(
                child: getItem(context, index),
                padding: getItemEdgeInsets(context, index),
            )));

    int getListLength(BuildContext context) => MyStatefulWidget.of(context).getOptionLength();

    EdgeInsets getItemEdgeInsets(BuildContext context, int index) => (getListLength(context) - 1) != index
        ? EdgeInsets.only(left: 20, right: 20)
        : EdgeInsets.only(left: 20, right: 20, bottom: MediaQuery.of(context).size.height * 0.1);

    Widget getItem(BuildContext context, int index) {
        return Container(
            margin: EdgeInsets.all(5),
            child: Card(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                elevation: 5.0,
                child: MenuItemWidget(index, true),
            ));
    }
}
