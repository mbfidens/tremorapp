import 'package:flutter/material.dart';
import 'package:Tremor_App/Widgets/fill_form_widget.dart';
import 'package:Tremor_App/Backend/app_localization.dart';

class UserProfileEditorContainer extends StatefulWidget {
  @override
  _UserProfileEditorContainerState createState() => _UserProfileEditorContainerState();
}

class _UserProfileEditorContainerState extends State<UserProfileEditorContainer> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _emailController2 = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordController2 = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _surnameController = TextEditingController();
  final TextEditingController _ageController = TextEditingController();

  String _dataInfoText = '';
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
      // Clean up the controller when the Widget is disposed
      _emailController.dispose();
      _passwordController.dispose();
      super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: formFields()
    );
  }

  Widget formFields() => Column(children: <Widget>[
        FillFormWidget(
            Icon(
              Icons.account_circle,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            AppLocalizations.of(context).translate("Name"),
            _nameController,
            false,
            context),
        FillFormWidget(
            Icon(
              Icons.account_circle,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            AppLocalizations.of(context).translate("Surname"),
            _surnameController,
            false,
            context),
        FillFormWidget(
            Icon(
              Icons.calendar_today,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            AppLocalizations.of(context).translate("Age"),
            _ageController,
            false,
            context),
        FillFormWidget(
            Icon(
              Icons.alternate_email,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            AppLocalizations.of(context).translate("Email"),
            _emailController,
            false,
            context),
        FillFormWidget(
            Icon(
              Icons.alternate_email,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            AppLocalizations.of(context).translate("Repeat Email"),
            _emailController2,
            false,
            context),
        FillFormWidget(
            Icon(
              Icons.lock,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            AppLocalizations.of(context).translate("Password"),
            _passwordController,
            true,
            context),
        FillFormWidget(
            Icon(
              Icons.lock,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            AppLocalizations.of(context).translate("Repeat password"),
            _passwordController2,
            true,
            context),
        Container(
          alignment: Alignment.center,
          child: Text(
            _dataInfoText,
            style: TextStyle(color: Colors.red, fontSize: 15),
          ),
        )
      ]);

  bool checkInputData() {
      if (!checkPasswordMatch()) {
          _dataInfoText =
              AppLocalizations.of(context).translate("password don't mach");
          return false;
      } else if (!checkEmailMatch()) {
          _dataInfoText =
              AppLocalizations.of(context).translate("email don't mach");
          return false;
      } else if (!_formKey.currentState.validate()){

      }
      else {
          return true;
      }
      return false;
  }

  bool checkPasswordMatch() =>
      _passwordController.text == _passwordController2.text;

  bool checkEmailMatch() => _emailController.text == _emailController2.text;
}
