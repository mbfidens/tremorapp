import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:flutter_circular_slider/flutter_circular_slider.dart';

class DonutContainer extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  DonutContainer(this.seriesList, {this.animate});

  factory DonutContainer.withSampleData(BuildContext context) {
    return new DonutContainer(
      _createSampleData(context),
      // Disable animations for image tests.
      animate: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return
      Container(
          child: charts.PieChart(seriesList,
              animate: animate,
              layoutConfig: charts.LayoutConfig(
                  topMarginSpec: charts.MarginSpec.fixedPixel(0),
                  bottomMarginSpec: charts.MarginSpec.fixedPixel(0),
                  leftMarginSpec: charts.MarginSpec.fixedPixel(0),
                  rightMarginSpec: charts.MarginSpec.fixedPixel(0)),
              defaultRenderer: charts.ArcRendererConfig(
                  arcWidth: 11, arcRendererDecorators: [])));
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<OptionsData, int>> _createSampleData(
      BuildContext context) {
    MyStatefulWidgetState state = MyStatefulWidget.of(context);
    int sum = state.getOptionResult();

    final data = [
      OptionsData(0, sum),
      OptionsData(1, 100 - sum)
    ];

    return [
      new charts.Series<OptionsData, int>(
        id: 'Options',
        domainFn: (OptionsData sales, _) => sales.index,
        measureFn: (OptionsData sales, _) => sales.value,
        data: data,
        colorFn: (_, index) {
          return index == 0
              ? charts.MaterialPalette.blue.makeShades(5)[1]
              : charts.MaterialPalette.blue.makeShades(5)[4];
        },
        labelAccessorFn: (OptionsData row, _) => 'Daily:(${55})',
      )
    ];
  }
}

/// Sample linear data type.
class OptionsData {
  final int index;
  final int value;

  OptionsData(this.index, this.value);
}

class CircularSliderContainer2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            width: MediaQuery.of(context).size.width * 0.45,
            height: MediaQuery.of(context).size.height * 0.45,
            child: SingleCircularSlider(
              100,
              67,
              selectionColor: Colors.transparent,
              handlerColor: ColorSub8,
              handlerOutterRadius: 3,
            )));
  }
}
