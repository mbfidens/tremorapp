import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:Tremor_App/Backend/Models/options_model.dart';

class HistoryGraphContainer extends StatefulWidget {
  HistoryGraphContainer();

  static List<charts.Series<OptionsModel, DateTime>> prepareData(BuildContext context) {
    MyStatefulWidgetState state = MyStatefulWidget.of(context);
    int activeButton = state.getChosenHistoryMenuItem();
    DateTime selectedDate = MyStatefulWidget.of(context).getCurrentlySelectedDate();

    return [
      new charts.Series<OptionsModel, DateTime>(
        id: 'History',
        domainFn: (OptionsModel points, i) => DateTime(points.date.year, points.date.month, points.date.day),
        measureFn: (OptionsModel points, i) => activeButton == null ? points.result : points.options[activeButton],
        data: generateData(context, state.getsHistoryInterval()),
        labelAccessorFn: (OptionsModel points, i) => activeButton == null
            ? points.result < 100
                ? points.result.toString()
                : '∞'
            : points.options[activeButton].toString(),
        colorFn: (OptionsModel points, index) {
          bool sameDate = true;
          if (points.date.day != selectedDate.day) {
            sameDate = false;
          }
          if (points.date.month != selectedDate.month) {
            sameDate = false;
          }
          if (points.date.year != selectedDate.year) {
            sameDate = false;
          }
          return sameDate ? charts.MaterialPalette.green.makeShades(5)[1] : charts.MaterialPalette.blue.makeShades(5)[1];
        },
      )
    ];
  }

  static List<OptionsModel> generateData(BuildContext context, int rangeInDaysBack) {
    List<OptionsModel> trimmedList = MyStatefulWidget.of(context).getSaved;
    int defaultRange = 7;

    DateTime lastDateInList = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 12)
        .subtract(Duration(days: rangeInDaysBack ?? defaultRange));

    trimmedList = trimmedList.where((element) => element.date.isAfter(lastDateInList)).toList();
    return trimmedList;
  }

  @override
  _HistoryGraphContainerState createState() => _HistoryGraphContainerState();
}

class _HistoryGraphContainerState extends State<HistoryGraphContainer> {
  List<charts.Series> seriesList;

  @override
  Widget build(BuildContext context) {
    return charts.TimeSeriesChart(
      HistoryGraphContainer.prepareData(context),
      animate: true,
      defaultRenderer: getRenderer(),
      primaryMeasureAxis: charts.NumericAxisSpec(
        showAxisLine: false,
        renderSpec: charts.NoneRenderSpec(),
        tickProviderSpec: charts.BasicNumericTickProviderSpec(desiredTickCount: 6),
      ),
      domainAxis: charts.DateTimeAxisSpec(
        showAxisLine: true,
      ),
      defaultInteractions: false,
      behaviors: [charts.SelectNearest(), charts.DomainHighlighter()],
    );
  }

  charts.SeriesRendererConfig getRenderer() => charts.BarRendererConfig<DateTime>(
        cornerStrategy: const charts.ConstCornerStrategy(30),
        barRendererDecorator: charts.BarLabelDecorator(
          labelPosition: charts.BarLabelPosition.inside,
          outsideLabelStyleSpec: charts.TextStyleSpec(fontSize: getLabelFontSize()),
          insideLabelStyleSpec: charts.TextStyleSpec(fontSize: getLabelFontSize(), color: charts.Color.white),
        ),
      );

  int getLabelFontSize() {
    MyStatefulWidgetState state = MyStatefulWidget.of(context);
    setState(() {
      if (state.getsHistoryInterval() <= 7) {
        return 20;
      } else if (state.getsHistoryInterval() <= 10) {
        return 15;
      } else {
        return 10;
      }
    });
    return 10;
  }
}
