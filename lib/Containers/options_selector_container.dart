import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:fading_edge_scrollview/fading_edge_scrollview.dart';
import 'package:Tremor_App/Widgets/menu_item_widget.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';

class OptionsSelectorContainer extends StatefulWidget {
  @override
  _OptionsSelectorContainerState createState() => _OptionsSelectorContainerState();
}

class _OptionsSelectorContainerState extends State<OptionsSelectorContainer> {
  int numberOfItems = 7;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 0), (){
        MyStatefulWidget.of(context).setHistoryButtonIndex(null);
    });
  }

  @override
  Widget build(BuildContext context) {
    numberOfItems = MyStatefulWidget.of(context).getOptionLength();
    return getOptionSelector();
  }

  Widget getOptionSelector() {
    return Container(
        width: 1.7976931348623157e+308,
        height: (MediaQuery.of(context).size.height - kBottomNavigationBarHeight - kToolbarHeight) * 0.53,
        child: Theme(
            data: ThemeData(
              highlightColor: Colors.blue.shade200,
            ), //Does not work
            child: getScrollBar()));
  }

  Widget getScrollBar() {
    ScrollController controller = ScrollController();
    return Scrollbar(
        controller: controller,
        isAlwaysShown: true,
        child: FadingEdgeScrollView.fromScrollView(
          gradientFractionOnStart: 0.1,
          gradientFractionOnEnd: 0.05,
          child: ListView.builder(
              physics: BouncingScrollPhysics(),
              controller: controller,
              padding: const EdgeInsets.all(2),
              itemCount: numberOfItems,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  child: getItem(context, index),
                  padding: const EdgeInsets.only(left: 20, right: 20),
                );
              }),
          //isAlwaysShown: true,
        ));
  }

  Widget getItem(BuildContext context, int index) {
    return index != 6 ? getNormalItem(index) : getLastItem(index);
  }

  Widget getNormalItem(int index) => Container(
      padding: EdgeInsets.all(1),
      margin: EdgeInsets.all(5),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        elevation: 5.0,
        child: MenuItemWidget(index, false),
      ));

  Widget getLastItem(int index) => Column(
        children: <Widget>[getNormalItem(index), getBottomPadding()],
      );

  Widget getBottomPadding() => Container(
        height: MediaQuery.of(context).size.height * 0.1,
      );
}
