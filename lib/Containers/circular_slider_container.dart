import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:flutter_circular_slider/flutter_circular_slider.dart';

class CircularSliderContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: Container(
        child: SingleCircularSlider(100, 30, selectionColor: ColorMain, handlerColor: ColorMain,))
    );
  }
}
