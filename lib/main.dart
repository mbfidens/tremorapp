import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:flutter/services.dart';
import 'package:Tremor_App/tab_navigator.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'Backend/app_language.dart';
import 'Backend/app_localization.dart';
import 'package:provider/provider.dart';
import 'package:Tremor_App/Backend/Sensors/inertial_sensors.dart';
import 'package:Tremor_App/Widgets/restart_widget.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  AppLanguage appLanguage = AppLanguage();
  await appLanguage.fetchLocale();

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    /* Platform.isIOS ?? */ SystemChrome.setEnabledSystemUIMode(
        SystemUiMode.immersive);
    runApp(MyApp(
      appLanguage: appLanguage,
    ));
  });
}

class MyApp extends StatelessWidget {
  final AppLanguage appLanguage;

  MyApp({this.appLanguage});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AppLanguage>(
      create: (_) => appLanguage,
      child: Consumer<AppLanguage>(builder: (context, model, child) {
        return RestartWidget(
          child: MaterialApp(
              builder: (BuildContext context, Widget child) {
                return MediaQuery(
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1.2),
                  child: child,
                );
              },
              locale: appLanguage.appLocal,
              localizationsDelegates: [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: [
                Locale('en', 'US'), // English
                Locale('lt', ''),
                Locale('ru', ''),
                // ... other locales the app supports
              ],
              debugShowCheckedModeBanner: false,
              title: 'Vilimed',
              theme: new ThemeData(
                primarySwatch: Colors.cyan,
                primaryColor: ColorMain,
                accentColor: const Color(0xFFfafafa),
                canvasColor: const Color(0xFFfafafa),
                accentTextTheme: TextTheme(
                  headline1: const TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w400),
                  headline2: const TextStyle(
                      color: Colors.grey,
                      fontSize: 30,
                      fontWeight: FontWeight.w400),
                ),
              ),
              home: MyStatefulWidget(
                child: Stack(
                  children: <Widget>[TabBarNavigator()],
                ),
              )),
        );
      }),
    );
  }
}
