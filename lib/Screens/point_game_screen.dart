import 'package:flutter/material.dart';

class PointGameScreen extends StatefulWidget {
  @override
  _PointGameScreenState createState() => _PointGameScreenState();
}

class _PointGameScreenState extends State<PointGameScreen> {
    double x = 0;
    double y =0;
  @override
  Widget build(BuildContext context) {
      x = MediaQuery.of(context).size.width * 0.5;
      y = MediaQuery.of(context).size.height * 0.5;
    return Container(
        padding: EdgeInsets.only(bottom: y, left: x),
        alignment: Alignment.bottomLeft,
        color: Colors.black,
        child: Text(
            '☼', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        ),
    );
  }
}
