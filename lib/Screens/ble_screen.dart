import 'dart:async';
import 'dart:io';
import 'package:Tremor_App/Backend/Storage/firebase_storage_controller.dart';
import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:Tremor_App/Backend/saved_bluetooth_devices_data.dart';
import 'package:Tremor_App/Widgets/navigator_button_widget.dart';
import 'package:android_intent/android_intent.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:encrypt/encrypt.dart' as enc;
import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/rendering.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_blue/gen/flutterblue.pb.dart' as proto;
import 'package:Tremor_App/Resources/BleResources.dart';
import 'package:wakelock/wakelock.dart';
import '../Backend/data_file.dart';
import '../Resources/AppColors.dart';
import 'package:intl/intl.dart';

import 'main_signin_screen.dart';

List<dynamic> savedDevices = [];
String savedBluetoothDevicesFileName = 'saved_bluetooth_devices.txt';
BluetoothCommunication protocol;
DatabaseReference ref = FirebaseDatabase.instance.ref();
DataSnapshot serialFromDatabase;
DataSnapshot serialFromDatabaseToValidate;
String bluetoothName;
String serialNumberKeyFromSharedPrefs;

final serialCheckKey = GlobalKey<FormState>();

GlobalKey<ScaffoldState> connectedDevicePageKey =
    new GlobalKey<ScaffoldState>();
GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
GlobalKey<ScaffoldState> scaffoldKeyDevicePage = new GlobalKey<ScaffoldState>();

//todo: flutter_blue: 0.7.3 is incompatible, doesnt work

/* widget:
ble_screen();
 */
/* build gradle:
minSdkVersion 19
 */
/* ANDROID manifest permission
 <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED"/>
    <uses-permission android:name="android.permission.VIBRATE" />
    <uses-permission android:name="android.permission.BLUETOOTH"
        />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN"
        />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"
        />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION"
        />
    <uses-feature
        android:name="android.hardware.bluetooth_le"
        android:required="true" />
 */
/* dependencies:
  flutter_blue: ^0.7.2
  encrypt: ^3.3.1
 */

/*class CreatedBluetoothDevice extends BluetoothDevice {
  CreatedBluetoothDevice.fromProto(BluetoothDevice p) : super.fromProto(p);

  DeviceIdentifier id = DeviceIdentifier(id: 456);
  BluetoothDevice sss = BluetoothDevice;

}*/

enum DeviceBtType { LE, CLASSIC, DUAL, UNKNOWN }

class DeviceListPage extends StatefulWidget {
  final String uid;

  const DeviceListPage({Key key, this.uid}) : super(key: key);

  @override
  _DeviceListPageState createState() => _DeviceListPageState();
}

class _DeviceListPageState extends State<DeviceListPage> {
  final border = RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(10.0),
  );
  ScrollController scrollBarController = ScrollController();

  @override
  void initState() {
    Future<SharedPreferences> sharedPrefs = SharedPreferences.getInstance();
    sharedPrefs.then((SharedPreferences prefs) async {
      String prefSerial = prefs.getString("serialNumberFromMap");
      if (prefSerial == null) {
        Future.delayed(Duration.zero, () => serialNumberDialog(context, false));
      } else {
        serialFromDatabase = await ref.child(prefSerial).get();
      }
    });
    Permission.bluetoothScan.request().isGranted;
    Permission.bluetoothAdvertise.request().isGranted;
    Permission.bluetoothConnect.request().isGranted;
    refreshSavedDeviceList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKeyDevicePage,
        body: SafeArea(
            child: Theme(
                data: ThemeData(
                  highlightColor: Colors.grey,
                ),
                child: Scrollbar(
                  controller: scrollBarController,
                  isAlwaysShown: true,
                  child: SingleChildScrollView(
                    controller: scrollBarController,
                    child: Column(
                        children: (serialNumberKeyFromSharedPrefs == null
                                ? listEmptyWidget()
                                : addNewDeviceButton()) +
                            [Divider(), getRegisteredSNDeviceButton()]),
                  ),
                ))));
  }

  List<Widget> listEmptyWidget() {
    return List.empty();
  }

  List<Widget> addNewDeviceButton() {
    return List.generate(savedDevices.length + 1, (index) {
      return InkWell(
          onLongPress: () {
            if (index != savedDevices.length) {
              _showDialog(index);
            }
          },
          onTap: () {
            if (index == savedDevices.length) {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => BleScan(refresh)),
              );
            } else {
              debugPrint('${savedDevices[index]}');
              BluetoothDevice savedDeviceToConnect =
                  createBluetoothDeviceFromSavedDevices(index);
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => BleDevices(
                          device: savedDeviceToConnect,
                          uid: widget.uid,
                        )), //cia kitkas
              );
            }
          },
          child: Container(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.1,
                  right: MediaQuery.of(context).size.width * 0.1,
                  top: MediaQuery.of(context).size.width * 0.07),
              height: MediaQuery.of(context).size.width * 0.4,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
                color: index < savedDevices.length ? ColorMain : Colors.blue,
                child: Center(
                    child: Text(
                  index < savedDevices.length
                      ? savedDevices[index].name.toString()
                      : AppLocalizations.of(context)
                          .translate('Add new Vilimed device'),
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: MediaQuery.of(context).size.width * 0.045),
                )),
              )));
    });
  }

  Widget getRegisteredSNDeviceButton() => Container(
      height: MediaQuery.of(context).size.width * 0.3,
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.1,
          vertical: MediaQuery.of(context).size.width * 0.05),
      child: InkWell(
        onTap: () {
          // if (widget.version.version) {
          serialNumberDialog(context, false);
          //  }
        },
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          color: Colors.grey,
          child: Center(
              child: Text(
                  serialNumberKeyFromSharedPrefs != null
                      ? AppLocalizations.of(context)
                              .translate("Change current SN:") +
                          "\n${serialNumberKeyFromSharedPrefs.toString().substring(2)}"
                      : AppLocalizations.of(context)
                          .translate("Enter Serial Number"),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                      fontSize: MediaQuery.of(context).size.width * 0.04))),
        ),
      ));

  BluetoothDevice createBluetoothDeviceFromSavedDevices(int index) {
    proto.BluetoothDevice p = proto.BluetoothDevice.create();
    p.name = savedDevices[index].name;
    p.remoteId = savedDevices[index].localid.toString();
    if (savedDevices[index].type == DeviceBtType.LE) {
      p.type = proto.BluetoothDevice_Type.LE;
    } else if (savedDevices[index].type == DeviceBtType.CLASSIC) {
      p.type = proto.BluetoothDevice_Type.CLASSIC;
    } else if (savedDevices[index].type == DeviceBtType.DUAL) {
      p.type = proto.BluetoothDevice_Type.DUAL;
    } else {
      p.type = proto.BluetoothDevice_Type.UNKNOWN;
    }
    return BluetoothDevice.fromProto(p);
  }

  void refresh() {
    refreshSavedDeviceList();
  }

  void refreshSavedDeviceList() async {
    try {
      String value = await DataFile(savedBluetoothDevicesFileName).readString();
      if (value != null && value.length > 0) {
        debugPrint(value.toString());
        List decodedJsonDevices = (json.decode(value) as List)
            .map((data) => SavedBluetoothDevicesData.fromJson(data))
            .toList();
        savedDevices.clear();
        savedDevices.addAll(decodedJsonDevices);
        setState(() {});
      }
    } catch (e) {
      debugPrint('BleScreen.refreshSavedDeviceList() ' + e.toString());
      setState(() {});
    }
  }

  _showDialog(int index) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(
              AppLocalizations.of(context).translate("Delete this device?")),
          content: new Text(
              AppLocalizations.of(context).translate("Device name: ") +
                  savedDevices[index].name),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new TextButton(
              child: new Text(
                AppLocalizations.of(context).translate("Close"),
                style: Theme.of(context)
                    .primaryTextTheme
                    .button
                    .copyWith(color: Colors.black),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new TextButton(
              child: new Text(
                AppLocalizations.of(context).translate("Delete"),
                style: Theme.of(context)
                    .primaryTextTheme
                    .button
                    .copyWith(color: Colors.red),
              ),
              onPressed: () {
                setState(() {
                  savedDevices.removeAt(index);
                  String json =
                      jsonEncode(savedDevices.map((i) => i.toJson()).toList())
                          .toString();
                  DataFile(savedBluetoothDevicesFileName).clean();
                  DataFile(savedBluetoothDevicesFileName).appendString(json);
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        );
      },
    );
  }

  Future<bool> checkIfInternetIsAvailable() async {
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  serialNumberDialog(BuildContext context, bool firstTimeOpened) async {
    final textField = TextEditingController();

    // flutter defined function
    Future<SharedPreferences> sharedPrefs = SharedPreferences.getInstance();
    showDialog(
      // barrierDismissible: chosenSerial == null ? false : true,
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            title: Text(AppLocalizations.of(context)
                .translate("Please enter your serial number")),
            content: Form(
              key: serialCheckKey,
              // onWillPop: () {},
              child: TextFormField(
                controller: textField,
                keyboardType: TextInputType.number,
                validator: (text) {
                  if (text == null || text.isEmpty) {
                    return AppLocalizations.of(context)
                        .translate('Please write your serial number');
                  }
                  if (text.length != 14) {
                    return AppLocalizations.of(context)
                        .translate('wrong serial number size');
                  }
                  if (serialFromDatabaseToValidate.exists) {
                    debugPrint('database : ' +
                        serialFromDatabaseToValidate.value.toString() +
                        ' key: ' +
                        serialFromDatabaseToValidate.key);
                  } else {
                    return AppLocalizations.of(context)
                        .translate("Serial number doesn't exists");
                  }

                  if (firstTimeOpened == false) {
                    setState(() {});
                  }
                  serialFromDatabase = serialFromDatabaseToValidate;
                  return null;
                },
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
                // Only numbers can be entered
                decoration: InputDecoration(
                  hintText: AppLocalizations.of(context).translate("e.g.") +
                      "01234567890123",
                ),
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text(
                  AppLocalizations.of(context).translate("Back"),
                  style: Theme.of(context)
                      .primaryTextTheme
                      .button
                      .copyWith(color: Colors.black),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text(
                  AppLocalizations.of(context).translate("Confirm"),
                  style: Theme.of(context)
                      .primaryTextTheme
                      .button
                      .copyWith(color: Colors.green),
                ),
                onPressed: () async {
                  if (await checkIfInternetIsAvailable() == false) {
                    final snackBarContent = SnackBar(
                      content: Center(
                          heightFactor: 1,
                          child: Text(AppLocalizations.of(context)
                              .translate("No internet connection"))),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBarContent);
                  } else {
                    serialFromDatabaseToValidate =
                        await ref.child('SN' + textField.text).get();
                    if (serialCheckKey.currentState.validate()) {
                      sharedPrefs.then((SharedPreferences prefs) {
                        bluetoothName = Map<String, dynamic>.from(
                            serialFromDatabaseToValidate.value)['bluetooth'];
                        prefs.setString(
                            "serialNumberBluetoothName", bluetoothName);
                        serialNumberKeyFromSharedPrefs = serialFromDatabase.key;
                        return (prefs.setString(
                            "serialNumberFromMap", serialFromDatabase.key));
                      });
                      Navigator.of(context).pop();
                    }
                  }
                },
              ),
            ],
          ),
        );
      },
    );
  }
}

class SerialNumberScreen extends StatefulWidget {
  @override
  _SerialNumberScreenState createState() => _SerialNumberScreenState();
}

class _SerialNumberScreenState extends State<SerialNumberScreen> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class BleScreen extends StatefulWidget {
  final String uid;

  const BleScreen({Key key, this.uid}) : super(key: key);

  @override
  _BleScreenState createState() => _BleScreenState();
}

class _BleScreenState extends State<BleScreen> {
  Future<SharedPreferences> sharedPrefs = SharedPreferences.getInstance();

  //bool isVersionSet = false;
  void openLocationSetting() async {
    final AndroidIntent intent = new AndroidIntent(
      action: 'android.settings.BLUETOOTH_SETTINGS',
    );
    await intent.launch();
  }

  void retrieveSerialNumberFromSharedPrefs() async {
    SharedPreferences prefs = await sharedPrefs;
    serialNumberKeyFromSharedPrefs = prefs.getString('serialNumberFromMap');
    bluetoothName = prefs.getString('serialNumberBluetoothName');
    /*if (serialNumberKeyFromSharedPrefs !=
        null
    ) {
      setState(() {});
    }*/
  }

  @override
  void initState() {
    super.initState();
    retrieveSerialNumberFromSharedPrefs();

  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: StreamBuilder<BluetoothState>(
            stream: FlutterBlue.instance.state,
            initialData: BluetoothState.unknown,
            builder: (c, snapshot) {
              final state = snapshot.data;
              if (state == BluetoothState.on) {
                return DeviceListPage(
                  uid: widget.uid,
                );
              } else if (state == BluetoothState.off) {
                openLocationSetting();
                return BleOff(state: state);
              } else if (state == null) {
                return BleOff(state: BluetoothState.unknown);
              } else
                return BleOff(state: state);
            }));
  }
}

class BluetoothCommunication {
  BluetoothCommunication(this.tuService);

  BluetoothService tuService;
  final CryptoAES crypter = CryptoAES();
  static const int stringLength = 16;
  bleStates currentBleState = bleStates.init;
  String currentExpectedAnswer = '';
  int bleTimeout = 0;
  int repeat = 0;
  int imagePosition = 0;

  final int imageOffset = HexImage.indexOf('!') + 1;
  final int portions =
      HexImage.substring(HexImage.indexOf('!') + 1).length ~/ stringLength;

  void performAllDataScrap() {
    sendVoltageRequest();
  }

  BluetoothCharacteristic getRx() {
    return tuService.characteristics.firstWhere(
        (BluetoothCharacteristic c) =>
            c.uuid.toString().toUpperCase() == BleConstants.TransUartRX,
        orElse: () => null);
  }

  BluetoothCharacteristic getTx() {
    return tuService.characteristics.firstWhere(
        (BluetoothCharacteristic c) =>
            c.uuid.toString().toUpperCase() == BleConstants.TransUartTx,
        orElse: () => null);
  }

  bool sendCommand(String dataToSend) {
    notify();
    final BluetoothCharacteristic rxChar = getRx();
    List<int> codedData = [];
    final int dataLength = dataToSend.length;
    String paddingString = '';

    if (rxChar != null) {
      if (dataToSend.length % 16 != 0) {
        for (int i = dataLength;
            i < (((dataLength ~/ stringLength) + 1) * stringLength);
            i++) {
          paddingString += '0';
        }
        dataToSend = dataToSend.substring(
                0, stringLength * (dataLength ~/ stringLength)) +
            paddingString +
            dataToSend.substring(stringLength * (dataLength ~/ stringLength));
      }
      debugPrint('**************Bluetooth data to send: ' +
          dataToSend +
          ' Mode: ' +
          currentBleState.toString());
      if (dataToSend.length > stringLength) {
        for (int i = stringLength; i <= dataToSend.length; i += stringLength) {
          codedData += crypter.encryptString(dataToSend.substring(i - 16, i));
          if (i == stringLength) {
            currentExpectedAnswer = String.fromCharCodes(codedData);
          }
        }
      } else {
        codedData = crypter.encryptString(dataToSend);
        currentExpectedAnswer = String.fromCharCodes(codedData);
      }

      rxChar.write(codedData).catchError((dynamic error) {
        debugPrint('**************Bluetooth error: ' + error.toString());
      }).then((dynamic value) {
        debugPrint('**************Bluetooth send: ' + value.toString());
        return true;
      }).timeout(const Duration(seconds: 1), onTimeout: () {
        debugPrint('**************Bluetooth timeout.');
        return false;
      });
    } else {
      //tuService.characteristics.elementAt(1).write(utf8.encode(dataToSend));
      return false;
    }
    return true;
  }

  bool sendCommandWithoutResponce(String dataToSend) {
    notify();
    final BluetoothCharacteristic rxChar = getRx();
    List<int> codedData = [];
    final int dataLength = dataToSend.length;
    String paddingString = '';

    if (rxChar != null) {
      if (dataToSend.length % 16 != 0) {
        for (int i = dataLength;
            i < (((dataLength ~/ stringLength) + 1) * stringLength);
            i++) {
          paddingString += '0';
        }
        dataToSend = dataToSend.substring(
                0, stringLength * (dataLength ~/ stringLength)) +
            paddingString +
            dataToSend.substring(stringLength * (dataLength ~/ stringLength));
      }
      debugPrint('**************Bluetooth data to send: ' +
          dataToSend +
          ' Mode: ' +
          currentBleState.toString());
      if (dataToSend.length > stringLength) {
        for (int i = stringLength; i <= dataToSend.length; i += stringLength) {
          codedData += crypter.encryptString(dataToSend.substring(i - 16, i));
          if (i == stringLength) {
            currentExpectedAnswer = String.fromCharCodes(codedData);
          }
        }
      } else {
        codedData = crypter.encryptString(dataToSend);
        currentExpectedAnswer = String.fromCharCodes(codedData);
      }

      rxChar
          .write(codedData, withoutResponse: true)
          .catchError((dynamic error) {
        debugPrint('**************Bluetooth error: ' + error.toString());
      }).then((dynamic value) {
        debugPrint('**************Bluetooth send: ' + value.toString());
        return true;
      }).timeout(const Duration(seconds: 1), onTimeout: () {
        debugPrint('**************Bluetooth timeout.');
        return false;
      });
    } else {
      //tuService.characteristics.elementAt(1).write(utf8.encode(dataToSend));
      return false;
    }
    return true;
  }

  List<int> receiveCommand(List<int> received) {
    final List<int> tempList = crypter.decryptString(received);
    return tempList ?? utf8.encode('AES error #1');
  }

  String repeatSend(String received) {
    if (repeat < 5) {
      switch (currentBleState) {
        case bleStates.init:
          break;
        case bleStates.waitingUpdateCRC:
          sendImageCrc();
          break;
        case bleStates.waitingUpdateLength:
          sendImageLength();
          break;
        case bleStates.waitingUpdatePortion:
          sendImagePackages();
          break;
        case bleStates.waitingUpdateFinishConfirmation:
          sendImagePackages();
          break;
        case bleStates.waitingTemperature:
          sendTemperatureRequest();
          break;
        case bleStates.waitingVoltage:
          sendVoltageRequest();
          break;
        case bleStates.waitingEepromData:
          sendScrapEepromDataRequest();
          break;
        default:
          break;
      }
      repeat++;
      return received;
    } else {
      repeat = 0;
      //currentBleState = bleStates.init;
      debugPrint('***************BLE fail - timeout - repeat overload');
      repeatSend('nope');
      return 'BLE fail - repeat overload';
    }
  }

  void bleWait() async {
    /*await Future<void>.delayed(Duration(milliseconds: bleTimeout)).then((dynamic onValue){
      debugPrint('***************TIMEOUT IN WAIT');
      repeatSend('null');
    });*/
    /*  Timer(Duration(milliseconds: bleTimeoutConstMs), (){
      debugPrint('***************TIMEOUT IN WAIT');
      if(currentBleState != bleStates.init) {
                repeatSend('null');
            } else{
              //TODO(Edvinas):add cancelation of timer
            }
          });*/
  }

  void notify() async {
    if (!getTx().isNotifying) {
      getTx().setNotifyValue(true);
    }
  }

  void sendTemperatureRequest() {
    currentBleState = bleStates.waitingVoltage;
    bleTimeout = DateTime.now().millisecondsSinceEpoch;
    sendCommand(BleCommands.GetTemperature);
    bleWait();
  }

  void sendVoltageRequest() {
    currentBleState = bleStates.waitingEepromData;
    bleTimeout = DateTime.now().millisecondsSinceEpoch;
    sendCommand(BleCommands.GetVoltage);
    bleWait();
  }

  void sendScrapEepromDataRequest() {
    currentBleState = bleStates.init;
    bleTimeout = DateTime.now().millisecondsSinceEpoch;
    sendCommand(BleCommands.ScrapEepromData);
    bleWait();
  }

  void sendImageCrc() {
    final String crc =
        HexImage.substring(HexImage.indexOf('#') + 1, HexImage.indexOf('@'));
    currentBleState = bleStates.waitingUpdateCRC;
    bleTimeout = DateTime.now().millisecondsSinceEpoch;

    sendCommand('00000000' + crc + '1703434068474139');
    Timer.periodic(Duration(milliseconds: bleTimeout), (timer) {
      debugPrint('***************TIMEOUT');
      repeatSend('null');
    });
    bleWait();
  }

  void sendImageLength() {
    final String length =
        HexImage.substring(HexImage.indexOf('@') + 1, HexImage.indexOf('!'));
    currentBleState = bleStates.waitingUpdateLength;
    bleTimeout = DateTime.now().millisecondsSinceEpoch;

    sendCommand('00000000' + length + '9875987621084309');
    bleWait();
  }

  // TODO(Edvinas): create stream or find better solution
  void sendImagePackages() {
    if (imagePosition < portions) {
      currentBleState = bleStates.waitingUpdatePortion;
      final int currentPosition = imageOffset + imagePosition * stringLength;
      sendCommand(
          HexImage.substring(currentPosition, currentPosition + stringLength) +
              '9852585245874120');
    } else if (imagePosition == portions) {
      currentBleState = bleStates.waitingUpdateFinishConfirmation;
      sendCommand('7158402101588458');
    } else if (imagePosition > portions) {
      currentBleState = bleStates.init;
    }
    bleTimeout = DateTime.now().millisecondsSinceEpoch;
    bleWait();
  }
}

class ViOTReceiver extends StatelessWidget {
  ViOTReceiver(this.txChar, this.receptionProtocol);

  final BluetoothCharacteristic txChar;
  final BluetoothCommunication receptionProtocol;

  String checkAnswer(String received) {
    final int timePassed =
        DateTime.now().millisecondsSinceEpoch - receptionProtocol.bleTimeout;
    debugPrint('***************CHECKING: ' + received);
    debugPrint(
        '***************EXPECTED: ' + receptionProtocol.currentExpectedAnswer);
    if (received == receptionProtocol.currentExpectedAnswer) {
      debugPrint('***************MATCH****************');
      if (timePassed < bleTimeoutConstMs) {
        switch (receptionProtocol.currentBleState) {
          case bleStates.init:
            break;
          case bleStates.waitingUpdateCRC:
            receptionProtocol.sendImageLength();
            break;
          case bleStates.waitingUpdateLength:
            receptionProtocol.imagePosition = 0;
            receptionProtocol.sendImagePackages();
            receptionProtocol.imagePosition++;
            break;
          case bleStates.waitingUpdatePortion:
            receptionProtocol.sendImagePackages();
            receptionProtocol.imagePosition++;
            break;
          case bleStates.waitingUpdateFinishConfirmation:
            receptionProtocol.currentBleState = bleStates.init;
            received = 'Success';
            break;
          case bleStates.waitingTemperature:
            receptionProtocol.sendTemperatureRequest();
            break;
          case bleStates.waitingVoltage:
            receptionProtocol.sendVoltageRequest();
            break;
          case bleStates.waitingEepromData:
            receptionProtocol.sendScrapEepromDataRequest();
            receptionProtocol.currentBleState = bleStates.init;
            break;
          default:
            break;
        }
      } else {
        debugPrint('***************TIMEOUT****************');
        received = receptionProtocol.repeatSend(received);
      }
    } else if (timePassed < bleTimeoutConstMs) {
      debugPrint('***************NON MATCH****************');
      received = receptionProtocol.repeatSend(received);
    } else if (timePassed > bleTimeoutConstMs) {
      debugPrint('***************NON MATCH + TIMEOUT****************');
      received = receptionProtocol.repeatSend(received);
    } else {
      debugPrint('***************NON MATCH + UNKNOWN ERROR****************');
      received = receptionProtocol.repeatSend(received);
    }
    return received;
  }

  @override
  Widget build(BuildContext context) {
    StreamBuilder<List<int>> builder = StreamBuilder(
        stream: txChar.value,
        builder: (context, asyncSnapshot) {
          if (asyncSnapshot.hasError) {
            return Text('Error! ' + asyncSnapshot.error.toString());
          } else if (asyncSnapshot.data == null) {
            return const Text('Received null');
          } else {
            String tempData = String.fromCharCodes(asyncSnapshot.data);
            debugPrint('***************RECEIVED: ' +
                tempData +
                ' Current state: ' +
                receptionProtocol.currentBleState.toString());
            if (receptionProtocol.currentBleState != bleStates.init) {
              tempData = checkAnswer(tempData);
            }
            return
                //Text(tempData.toString() +
                //  ' \n progresas: ' +
                //  receptionProtocol.imagePosition.toString() +
                //  ' / ' +
                //   receptionProtocol.portions.toString());
                Container();
          }
        });
    return Container(child: builder ?? const Text('AES loading'));
  }
}

class BleDevices extends StatefulWidget {
  BleDevices({Key key, this.device, this.uid}) : super(key: key);
  final BluetoothDevice device;
  final String uid;

  @override
  _BleDevicesState createState() => _BleDevicesState();
}

class _BleDevicesState extends State<BleDevices> {
  //ViOTScreen screen;
  BluetoothService tempService;
  var stringas = '';
  bool inactiveUpdateButton = false;
  BluetoothDeviceState deviceStates = BluetoothDeviceState.connecting;
  ValueNotifier<int> batteryLevelCurrent = ValueNotifier<int>(0);
  StreamSubscription streamSubscription;

  @override
  void initState() {
    widget.device.disconnect();
    connectAndScrap();
    super.initState();
  }

  void connectAndScrap() {
    widget.device
        .connect(timeout: const Duration(seconds: 5), autoConnect: false)
        .then((a) {
      widget.device.discoverServices().then((value) {
        debugPrint('--- services discovered----------- ');
        value.forEach((chosenService) {
          if (chosenService.uuid.toString().toUpperCase() ==
              BleConstants.TransUartUUID) {
            tempService = chosenService;
            protocol = BluetoothCommunication(tempService);
            try {
              protocol.getTx().setNotifyValue(true).then((value) {
                protocol.performAllDataScrap();
                //protocol.sendScrapEepromDataRequest();
              });
            } catch (e) {
              streamSubscription.cancel();
              debugPrint('init error ' + e.toString());
            }
            String usageData = '';
            String tremorData = '';
            bool usageDataAcquired = false;
            bool tremorDataAcquired = false;
            streamSubscription = protocol.getTx().value.listen((value) async {
              if (usageDataAcquired == false) {
                usageData = usageData + String.fromCharCodes(value);
                //debugPrint(' usageData length: ' + usageData.length.toString());
              } else {
                tremorData = tremorData + String.fromCharCodes(value);
              }
              String tempData = String.fromCharCodes(value);
              // debugPrint(' init 1st: ' + String.fromCharCodes(value));
              if (tempData.indexOf(':') != -1 && tempData.contains("Voltage")) {
                final int voltageInHundreds = int.parse(tempData.substring(
                    tempData.indexOf(':') + 2, tempData.indexOf('.')));
                batteryLevelCurrent.value = voltageInHundreds;
                Future.delayed(Duration.zero, () => setState(() {}));
                debugPrint('batteryLevelCurrentX: ' +
                    batteryLevelCurrent.value.toString());
                debugPrint('parsinta itampa : ' +
                    batteryLevelCurrent.value.toString());
                //   protocol.sendCommand(BleCommands.ScrapEepromData);
              }
              if (tempData.indexOf(':') != -1 &&
                  tempData.contains("Temperature")) {
                final int temperature = int.parse(tempData.substring(
                    tempData.indexOf(':') + 2, tempData.indexOf(':') + 5));
                debugPrint(
                    '**Parsed received Temperature: ' + temperature.toString());
                // protocol.sendCommand(BleCommands.GetVoltage);
              }
              if (tempData.indexOf(';') != -1) {
                //   final int voltageInHundreds =
                // int.parse(tempData.substring(tempData.indexOf(':') + 2, tempData.indexOf('.')));

                //debugPrint('parsinta eeproma : ');
              }
              int eepromUsageDataExpectedFullLength = 350;
              FirebaseStorageController firebaseStorageController =
                  FirebaseStorageController();
              if (usageData.length > eepromUsageDataExpectedFullLength &&
                  usageDataAcquired == false) {
                usageDataAcquired = true;
                String parsedUsageStatistics = parseUsageStatistics(usageData);
                debugPrint(parsedUsageStatistics);
                try {
                  await firebaseStorageController.storeFile(
                      tempLocalFileName: 'UsageStatistics',
                      cloudFolderName: 'VilimBallUsageStatistics/${widget.uid}',
                      cloudFileName: widget.uid +
                          DateFormat('_yyyy-MM-dd').format(DateTime.now()),
                      dataToSave: jsonEncode(usageData.substring(19) +
                          " " +
                          parsedUsageStatistics));
                  debugPrint('UsageStatistics.logFirebase() Stored in ID ' +
                      widget.uid);
                  usageData = '';
                } catch (e) {
                  debugPrint('UsageStatistics.logFirebase() ' + e.toString());
                  usageData = '';
                }
                Future<SharedPreferences> sharedPrefs =
                    SharedPreferences.getInstance();
                SharedPreferences prefs = await sharedPrefs;
                String lastScrapDate = prefs.getString(widget.device.name);
                if (lastScrapDate == null ||
                    DateTime.now()
                                .difference(DateFormat('yyyy-MM-dd')
                                    .parse(lastScrapDate))
                                .inDays >
                            3 &&
                        lastScrapDate.isNotEmpty) {
                  protocol.sendCommand(BleCommands.ScrapFlashData);

                  debugPrint('Scrap complete and date saved to shared prefs');
                } else {
                  debugPrint('Ball was already scrapped during 3 days period');
                }
              }
              if (tremorData.endsWith('!') &&
                  usageDataAcquired == true &&
                  tremorDataAcquired == false) {
                tremorDataAcquired = true;
                streamSubscription.cancel();
                try {
                  await firebaseStorageController.storeFile(
                      tempLocalFileName: 'VilimBallTherapyLogs',
                      cloudFolderName: 'VilimBallTherapyLogs/${widget.uid}',
                      cloudFileName: widget.uid +
                          DateFormat('_yyyy-MM-dd').format(DateTime.now()),
                      dataToSave: jsonEncode(tremorData));
                  debugPrint(
                      'VilimBallTherapyLogs.logFirebase() Stored in ID ' +
                          widget.uid);
                  tremorData = '';

                  Future<SharedPreferences> sharedPrefs =
                      SharedPreferences.getInstance();
                  SharedPreferences prefs = await sharedPrefs;
                  prefs.setString(widget.device.name,
                      DateFormat('yyyy-MM-dd').format(DateTime.now()));
                  debugPrint('Scrap complete and date saved to shared prefs');
                } catch (e) {
                  debugPrint(
                      'VilimBallTherapyLogs.logFirebase() ' + e.toString());
                  tremorData = '';
                }
              }
            });
          }
        });
      });
    });
  }

  int daysBetween(DateTime from, DateTime to) {
    from = DateTime(from.year, from.month, from.day);
    to = DateTime(to.year, to.month, to.day);
    return (to.difference(from).inHours / 24).round();
  }

  String parseUsageStatistics(String data) {
    int errorCount;
    int therapyLogIndex;
    int onOffCount;
    int activeModeTime;
    int chargingTime;
    int chargingCount;
    int buttonPressCount;
    int frequencyConstant;
    data = data.substring(19);
    List<String> cutData = data.split(";");

    errorCount =
        int.parse(cutData.elementAt(1) + cutData.elementAt(0), radix: 16);
    therapyLogIndex =
        int.parse(cutData.elementAt(3) + cutData.elementAt(2), radix: 16);
    onOffCount =
        int.parse(cutData.elementAt(5) + cutData.elementAt(4), radix: 16);
    activeModeTime = int.parse(
        cutData.elementAt(9) +
            cutData.elementAt(8) +
            cutData.elementAt(7) +
            cutData.elementAt(6),
        radix: 16);
    chargingTime = int.parse(
        cutData.elementAt(13) +
            cutData.elementAt(12) +
            cutData.elementAt(11) +
            cutData.elementAt(10),
        radix: 16);
    chargingCount =
        int.parse(cutData.elementAt(15) + cutData.elementAt(14), radix: 16);
    buttonPressCount =
        int.parse(cutData.elementAt(17) + cutData.elementAt(16), radix: 16);
    frequencyConstant =
        int.parse(cutData.elementAt(19) + cutData.elementAt(18), radix: 16);

    return "errorCount: " +
        errorCount.toString() +
        " therapyLogIndex: " +
        therapyLogIndex.toString() +
        " onOffCount: " +
        onOffCount.toString() +
        " activeModeTime: " +
        activeModeTime.toString() +
        " chargingTime: " +
        chargingTime.toString() +
        " chargingCount: " +
        chargingCount.toString() +
        " buttonPressCount: " +
        buttonPressCount.toString() +
        " frequencyConstant: " +
        frequencyConstant.toString();
  }

  @override
  void dispose() {
    widget.device.disconnect();
    stringas = '';
    if (streamSubscription != null) {
      streamSubscription.cancel();
    }
    debugPrint('--- cancelled subscriptionn----------- ');
    super.dispose();
  }

  BluetoothService getService(BluetoothDevice serviceDevice) {
    BluetoothService serviceToReturn;
    //In case if there are no Vilimed devices nearby
    serviceToReturn = null;
    Stream<List<BluetoothService>> stream =
        Stream.fromFuture(serviceDevice.discoverServices());
    stream.listen((List<BluetoothService> discoveredServices) {
      serviceToReturn = discoveredServices.firstWhere((BluetoothService item) =>
          item.uuid.toString().toUpperCase() == BleConstants.TransUartUUID);
    });
    return serviceToReturn;
  }

  Widget getBatteryIcon() {
    return batteryLevelCurrent.value > 400
        ? Icon(FlutterIcons.battery_mco)
        : batteryLevelCurrent.value > 380
            ? Icon(FlutterIcons.battery_80_mco)
            : batteryLevelCurrent.value > 360
                ? Icon(FlutterIcons.battery_50_mco)
                : batteryLevelCurrent.value > 340
                    ? Icon(FlutterIcons.battery_20_mco)
                    : batteryLevelCurrent.value > 300
                        ? Icon(FlutterIcons.battery_10_mco)
                        : Icon(Icons.battery_unknown);
  }

  Widget getBatteryIconAndPercentage() {
    double batteryLevelPercentage =
        (100 / 90 * (batteryLevelCurrent.value - 320));
    if (batteryLevelPercentage > 100) {
      batteryLevelPercentage = 100;
    } else if (batteryLevelPercentage < 5) {
      batteryLevelPercentage = 5;
    }
    if (batteryLevelCurrent.value == 0) {
      return getBatteryIcon();
    } else {
      return Row(
        children: [
          getBatteryIcon(),
          Text((100 / 90 * (batteryLevelCurrent.value - 320))
                  .toInt()
                  .toString() +
              '%'),
        ],
      );
    }
  }

  bool updateStarted = false;

  Widget updateButtonWidget() {
    return InkWell(
        onTap: () {
          Wakelock.enable();
          // protocol.sendImageCrc();
          if (updateStarted == false) {
            protocol.sendCommand(BleCommands.FirmwareUpdate);
            Future.delayed(const Duration(milliseconds: 500), () {
              protocol.sendImageCrc();
            });
          }
          updateStarted = true;
          setState(() {
            inactiveUpdateButton = false;
          });
          if (inactiveUpdateButton == true) {
            setState(() {
              final snackBarContent = SnackBar(
                content: Center(heightFactor: 1, child: Text("Update Started")),
                action: SnackBarAction(
                    label: AppLocalizations.of(context).translate("Hide"),
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    }),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBarContent);
            });
          } else {}
        },
        child: Container(
            padding: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.1,
                right: MediaQuery.of(context).size.width * 0.1,
                top: MediaQuery.of(context).size.width * 0.07),
            height: MediaQuery.of(context).size.width * 0.4,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              color: ColorMain,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "Update",
                      //  AppLocalizations.of(context)
                      //    .translate("Check for updates"),
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: MediaQuery.of(context).size.width * 0.05),
                    ),
                  ],
                ),
              ),
            )));
  }

  Widget HighSpeedButtonWidget() {
    return InkWell(
        onTap: () {
          Wakelock.enable();

          if (updateStarted == false) {
          //  protocol.sendCommand(BleCommands.LedColorGreen);
         //   protocol.sendCommand('00000000000000001703254861575084');
          //  protocol.sendCommand(BleCommands.ChangeFrequencyConst);

              protocol.sendCommand(BleCommands.LedColorGreen);
              Future.delayed(const Duration(milliseconds: 200), () {
                protocol.sendCommand('1703254861575084' + '1600');
                Future.delayed(const Duration(milliseconds: 200), () {
                  protocol.sendCommand(BleCommands.MotorON);
                });
              });

          }
         // updateStarted = true;
          setState(() {
            inactiveUpdateButton = false;
          });
        },
        child: Container(
            padding: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.1,
                right: MediaQuery.of(context).size.width * 0.1,
                top: MediaQuery.of(context).size.width * 0.07),
            height: MediaQuery.of(context).size.width * 0.2,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              color: ColorMain,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "High speed",
                      //  AppLocalizations.of(context)
                      //    .translate("Check for updates"),
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: MediaQuery.of(context).size.width * 0.05),
                    ),
                  ],
                ),
              ),
            )));
  }

  Widget MediumSpeedButtonWidget() {
    return InkWell(
        onTap: () {
          Wakelock.enable();

          if (updateStarted == false) {
            protocol.sendCommand(BleCommands.LedColorGreen);
            Future.delayed(const Duration(milliseconds: 200), () {
              protocol.sendCommand('1703254861575084' + '1200');
              Future.delayed(const Duration(milliseconds: 200), () {
                protocol.sendCommand(BleCommands.MotorON);
              });
            });
          }
          // updateStarted = true;
          setState(() {
            inactiveUpdateButton = false;
          });
        },
        child: Container(
            padding: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.1,
                right: MediaQuery.of(context).size.width * 0.1,
                top: MediaQuery.of(context).size.width * 0.07),
            height: MediaQuery.of(context).size.width * 0.2,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              color: ColorMain,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "Medium speed",
                      //  AppLocalizations.of(context)
                      //    .translate("Check for updates"),
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: MediaQuery.of(context).size.width * 0.05),
                    ),
                  ],
                ),
              ),
            )));
  }

  Widget LowSpeedButtonWidget() {
    return InkWell(
        onTap: () {
          Wakelock.enable();

          if (updateStarted == false) {
            protocol.sendCommand(BleCommands.LedColorGreen);
            Future.delayed(const Duration(milliseconds: 200), () {
              protocol.sendCommand('1703254861575084' + '800');
              Future.delayed(const Duration(milliseconds: 200), () {
                protocol.sendCommand(BleCommands.MotorON);
              });
            });
          }
          // updateStarted = true;
          setState(() {
            inactiveUpdateButton = false;
          });
        },
        child: Container(
            padding: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.1,
                right: MediaQuery.of(context).size.width * 0.1,
                top: MediaQuery.of(context).size.width * 0.07),
            height: MediaQuery.of(context).size.width * 0.2,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              color: ColorMain,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "Low speed",
                      //  AppLocalizations.of(context)
                      //    .translate("Check for updates"),
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: MediaQuery.of(context).size.width * 0.05),
                    ),
                  ],
                ),
              ),
            )));
  }

  Widget connectionButtonWidget() {
    return StreamBuilder<BluetoothDeviceState>(
      stream: widget.device.state,
      initialData: BluetoothDeviceState.connecting,
      builder: (c, snapshot) {
        VoidCallback onPressed;
        String text;
        switch (snapshot.data) {
          case BluetoothDeviceState.connected:
            inactiveUpdateButton = true;
            // onPressed = () => {if (streamSubscription != null) {streamSubscription.cancel()},widget.device.disconnect()}; //if (streamSubscription != null) {streamSubscription.cancel();}
            text = AppLocalizations.of(context).translate("Connected");
            break;
          case BluetoothDeviceState.disconnected:
            inactiveUpdateButton = false;

            onPressed = () => {
                  if (streamSubscription != null) {streamSubscription.cancel()},
                  widget.device.disconnect(),
                  connectAndScrap()
                };
            text = AppLocalizations.of(context).translate("Connect");
            break;
          default:
            onPressed = null;
            text = snapshot.data.toString().substring(21).toUpperCase();
            break;
        }
        return InkWell(
          onTap: onPressed,
          child: Container(
            padding: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.1,
                right: MediaQuery.of(context).size.width * 0.1,
                top: MediaQuery.of(context).size.width * 0.07),
            height: MediaQuery.of(context).size.width * 0.4,
            child: Card(
              shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: snapshot.data != BluetoothDeviceState.connected
                        ? Colors.transparent
                        : Colors.black,
                    width: 2.0),
                borderRadius: BorderRadius.circular(30.0),
              ),
              color: snapshot.data != BluetoothDeviceState.connected
                  ? ColorSub2
                  : ColorSub10,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      text,
                      style: TextStyle(
                          color: snapshot.data != BluetoothDeviceState.connected
                              ? Colors.white
                              : Colors.black,
                          fontWeight: FontWeight.w600,
                          fontSize: MediaQuery.of(context).size.width * 0.05),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget deviceNameConnectionBatteryWidget() {
    return StreamBuilder<BluetoothDeviceState>(
        stream: widget.device.state,
        initialData: BluetoothDeviceState.connecting,
        builder: (c, snapshot) => Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      widget.device.name,
                      textAlign: TextAlign.center,
                    ),
                    (snapshot.data == BluetoothDeviceState.connected)
                        ? Icon(
                            Icons.bluetooth_connected,
                          )
                        : Icon(
                            Icons.bluetooth_disabled,
                          ),
                    (snapshot.data == BluetoothDeviceState.connected)
                        ? getBatteryIconAndPercentage()
                        : Icon(
                            Icons.battery_unknown,
                            color: Colors.grey,
                          ),
                  ],
                ),
              ],
            ));
  }

  Widget vilimedDeviceIcon() {
    return IconButton(
      iconSize: MediaQuery.of(context).size.height * 0.15,
      icon: (widget.device.name.contains('RN'))
          ? Image.asset('lib/Assets/Images/vilim_ball.png')
          : Image.asset('lib/Assets/Images/unknown_device.png'),
      onPressed: () {},
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: connectedDevicePageKey,
      body: SingleChildScrollView(
        child: SafeArea(
          child: SizedBox(
            height: MediaQuery.of(context).size.height -
                MediaQuery.of(context).padding.top,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                vilimedDeviceIcon(),
                deviceNameConnectionBatteryWidget(),
                protocol != null
                    ? ViOTReceiver(protocol.getTx(), protocol)
                    : Container(),
                connectionButtonWidget(),
               /* serialNumberKeyFromSharedPrefs != null ?
                (serialNumberKeyFromSharedPrefs == "SN01014000100052" &&
                        inactiveUpdateButton &&
                        updateStarted == false)
                    ? updateButtonWidget()
                    : Container() : Container(),*/
                serialNumberKeyFromSharedPrefs != null ?
                (serialNumberKeyFromSharedPrefs == "SN01014000100052" &&
                    inactiveUpdateButton &&
                    updateStarted == false)
                    ? HighSpeedButtonWidget()
                    : Container() : Container(),
                serialNumberKeyFromSharedPrefs != null ?
                (serialNumberKeyFromSharedPrefs == "SN01014000100052" &&
                    inactiveUpdateButton &&
                    updateStarted == false)
                    ? MediumSpeedButtonWidget()
                    : Container() : Container(),
                serialNumberKeyFromSharedPrefs != null ?
                (serialNumberKeyFromSharedPrefs == "SN01014000100052" &&
                    inactiveUpdateButton &&
                    updateStarted == false)
                    ? LowSpeedButtonWidget()
                    : Container() : Container(),

                Spacer(),
                updateStarted == false ? backButton(context) : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class BleScan extends StatefulWidget {
  final Function() refreshBLEScreen;

  BleScan(this.refreshBLEScreen);

  @override
  _BleScanState createState() => _BleScanState(refreshBLEScreen);
}

class _BleScanState extends State<BleScan> {
  final Function() refreshBLEScreen;

  _BleScanState(this.refreshBLEScreen);

  BluetoothService service;
  int scanDuration = 10; // seconds

  @override
  void initState() {
    FlutterBlue.instance.startScan(timeout: Duration(seconds: scanDuration));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: SingleChildScrollView(
        child: SafeArea(
          child: SizedBox(
            height: MediaQuery.of(context).size.height -
                MediaQuery.of(context).padding.top -
                MediaQuery.of(context).padding.bottom,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.width * 0.1,
                      horizontal: MediaQuery.of(context).size.width * 0.03),
                  child: Text(
                    AppLocalizations.of(context).translate(
                        'If your device is not listed, turn the device to Bluetooth mode and search again'),
                    style: Theme.of(context)
                        .primaryTextTheme
                        .subtitle1
                        .copyWith(
                            color: Colors.grey,
                            fontSize: MediaQuery.of(context).size.width * 0.05),
                    textAlign: TextAlign.center,
                  ),
                ),
                Expanded(
                  child: StreamBuilder<List<ScanResult>>(
                      stream: FlutterBlue.instance.scanResults,
                      initialData: [],
                      builder: (c, snapshot) {
                        return ListView.builder(
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, int index) {
                              return snapshot.data[index].device.name
                                      .contains(bluetoothName)
                                  ? ScanResultTile(
                                      refreshBLEScreen: refreshBLEScreen,
                                      result: snapshot.data[index],
                                      protocol: protocol,
                                      onTap: () => Navigator.of(context).push(
                                          MaterialPageRoute(builder: (context) {
                                        return null;
                                      })),
                                      device: snapshot.data[index].device,
                                    )
                                  : Container();
                            });
                      }),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    backButton(context),
                    StreamBuilder<bool>(
                      stream: FlutterBlue.instance.isScanning,
                      initialData: false,
                      builder: (c, snapshot) {
                        if (snapshot.data) {
                          return Container();
                        } else {
                          return NavigatorButtonWidget(
                            iconRight: Icons.search,
                            color: ColorSub2,
                            text: AppLocalizations.of(context)
                                .translate('Search again'),
                            onPressedCallback: () {
                              FlutterBlue.instance.startScan(
                                  timeout: Duration(seconds: scanDuration));
                            },
                          );
                        }
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class BleOff extends StatelessWidget {
  const BleOff({Key key, this.state}) : super(key: key);
  final BluetoothState state;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlueAccent,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              Icons.bluetooth_disabled,
              size: 200.0,
              color: Colors.white54,
            ),
            Text(
              AppLocalizations.of(context).translate('Bluetooth adapter is') +
                  AppLocalizations.of(context)
                      .translate("${state.toString().substring(15)}"),
              style: Theme.of(context)
                  .primaryTextTheme
                  .subtitle1
                  .copyWith(color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}

class CryptoAES {
  static final dynamic key = enc.Key.fromUtf8(BleConstants.AnsKey);
  final enc.IV iv = enc.IV.fromUtf8(BleConstants.AesIv);
  enc.Encrypter encrypter = enc.Encrypter(enc.AES(key, mode: enc.AESMode.cbc));

  DebugPrint debugger = DebugPrint();

  List<int> encryptString(String input) {
    debugger.print('INPUT:' + input);
    final enc.Encrypted encrypted = encrypter.encrypt(input, iv: iv);
    debugger.print('SENT:' +
        String.fromCharCodes(encrypted.bytes.toList().sublist(0, 16)));
    /*TODO(Edvinas): add middleware logging of input and encripted input
    final decrypted = encrypter.decrypt(encrypted, iv: iv);
    */
    return encrypted.bytes.toList().sublist(0, 16);
  }

  List<int> decryptString(List<int> input) {
    final enc.Encrypted encryptedInput =
        enc.Encrypted(Uint8List.fromList(input));
    // TODO(Edvinas): Unnecessary conversion int > str > int. Find better.
    final String decryptedCommand =
        utf8.decode(encrypter.decryptBytes(encryptedInput));
    /*TODO(Edvinas): add middleware logging of input and decripted input
    final decrypted = encrypter.decrypt(encrypted, iv: iv);
    */
    return utf8.encode(decryptedCommand);
  }
}

class DebugPrint {
  void print(String input) {
    debugPrint('**************' + input);
  }
}

class ScanResultTile extends StatefulWidget {
  final Function() refreshBLEScreen;

  const ScanResultTile(
      {Key key,
      this.result,
      this.onTap,
      this.device,
      this.protocol,
      this.refreshBLEScreen,
      this.service})
      : super(key: key);

  final ScanResult result;
  final VoidCallback onTap;
  final BluetoothDevice device;
  final BluetoothService service;

  final BluetoothCommunication protocol;

  @override
  _ScanResultTileState createState() => _ScanResultTileState(refreshBLEScreen);
}

class _ScanResultTileState extends State<ScanResultTile> {
  final Function() refreshBLEScreen;

  _ScanResultTileState(
    this.refreshBLEScreen,
  );

  final BleCommands cmd = BleCommands();

  String btnText = 'Send data';

  final commandNames chosenCommand = commandNames.LedColorBlue;

  void setNotify(BluetoothCharacteristic ctx) async {
    if (!ctx.isNotifying) {
      await ctx.setNotifyValue(true);
    }
  }

  Widget _buildTitle(BuildContext context) {
    if (widget.result.device.name.length > 0) {
      return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AutoSizeText(
              widget.result.device.name,
            ),
          ]);
    } else {
      return Text(widget.result.device.id.toString());
    }
  }

  String getNiceHexArray(List<int> bytes) {
    return '[${bytes.map((i) => i.toRadixString(16).padLeft(2, '0')).join(', ')}]'
        .toUpperCase();
  }

  String getNiceManufacturerData(Map<int, List<int>> data) {
    if (data.isEmpty) {
      return null;
    }
    List<String> res = [];
    data.forEach((id, bytes) {
      res.add(
          '${id.toRadixString(16).toUpperCase()}: ${getNiceHexArray(bytes)}');
    });
    return res.join(', ');
  }

  String getNiceServiceData(Map<String, List<int>> data) {
    if (data.isEmpty) {
      return null;
    }
    List<String> res = [];
    data.forEach((id, bytes) {
      res.add('${id.toUpperCase()}: ${getNiceHexArray(bytes)}');
    });
    return res.join(', ');
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        color: Colors.transparent,
        shadowColor: Colors.transparent,
        shape: StadiumBorder(
          side: BorderSide(
            color: ColorSub1,
          ),
        ),
        child: ListTile(
          title: _buildTitle(context),
          trailing: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
            ElevatedButton(
              child: Text(AppLocalizations.of(context).translate('Add')),
              style: ElevatedButton.styleFrom(
                primary: ColorMain,
                onPrimary: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
              onPressed: () {
                if (savedDevices
                        .where((item) => item.name.contains(widget.device.name))
                        .length >
                    0) {
                  final snackBarContent = SnackBar(
                    content: Center(
                        heightFactor: 1,
                        child: Text(AppLocalizations.of(context)
                            .translate("Device already exists!"))),
                    action: SnackBarAction(
                        label: AppLocalizations.of(context).translate('Hide'),
                        onPressed: () {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        }),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBarContent);
                } else {
                  SavedBluetoothDevicesData newSavedBluetoothDevicesData =
                      SavedBluetoothDevicesData(
                          localid: widget.device.id,
                          name: widget.device.name,
                          type: widget.device.type);
                  savedDevices.add(newSavedBluetoothDevicesData);
                  String json =
                      jsonEncode(savedDevices.map((i) => i.toJson()).toList())
                          .toString();
                  DataFile(savedBluetoothDevicesFileName).clean();
                  DataFile(savedBluetoothDevicesFileName).appendString(json);
                  refreshBLEScreen();
                  final snackBarContent = SnackBar(
                    content: Center(
                        heightFactor: 1,
                        child: Text(AppLocalizations.of(context)
                                .translate("Device ") +
                            widget.device.name.toString() +
                            AppLocalizations.of(context).translate(" added!"))),
                    action: SnackBarAction(
                        label: AppLocalizations.of(context).translate('Hide'),
                        onPressed: () {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        }),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBarContent);
                }
                FlutterBlue.instance.stopScan();
                Navigator.of(context).pop();
              },
            ),
            Container(
              width: 5,
            ),
            ElevatedButton(
              child: Text(AppLocalizations.of(context).translate('Indicate')),
              style: ElevatedButton.styleFrom(
                primary: ColorSub2,
                onPrimary: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
              onPressed: () {
                FlutterBlue.instance.stopScan();
                BluetoothService tempService;
                widget.device
                    .connect(
                        timeout: const Duration(seconds: 5), autoConnect: false)
                    .then((a) {
                  widget.device.discoverServices().then((value) {
                    debugPrint('--- services discovered----------- ');
                    value.forEach((chosenService) {
                      if (chosenService.uuid.toString().toUpperCase() ==
                          BleConstants.TransUartUUID) {
                        tempService = chosenService;
                        protocol = BluetoothCommunication(tempService);
                        protocol.getTx().setNotifyValue(true).then((value) {
                          protocol.sendCommand(BleCommands.BlinkBlue);
                          Future.delayed(Duration(milliseconds: 200), () {
                            widget.device.disconnect();
                          });

                          final snackBarContent = SnackBar(
                            content: Center(
                                heightFactor: 1,
                                child: Text(AppLocalizations.of(context)
                                    .translate("Your device should blink!"))),
                            action: SnackBarAction(
                                label: AppLocalizations.of(context)
                                    .translate('Hide'),
                                onPressed: () {
                                  ScaffoldMessenger.of(context)
                                      .hideCurrentSnackBar();
                                }),
                          );
                          ScaffoldMessenger.of(context)
                              .showSnackBar(snackBarContent);
                        });
                      }
                    });
                  });
                });
              },
            ),
          ]),
        ));
  }
}

class ViOTScreen extends StatefulWidget {
  ViOTScreen({Key key, @required this.service}) : super(key: key);
  final BluetoothService service;

  @override
  _ViOTScreenState createState() =>
      _ViOTScreenState(BluetoothCommunication(service));
}

class _ViOTScreenState extends State<ViOTScreen> {
  _ViOTScreenState(this.protocol);

  BluetoothCommunication protocol;
  final BleCommands cmd = BleCommands();
  String btnText = 'Perform Update';
  commandNames chosenCommand = commandNames.LedColorBlue;

  void perform() {
    setState(() {
      if (protocol.tuService != null) {
        setNotify(protocol.getTx());
        protocol.sendCommand(cmd.getCommand(chosenCommand));
      }
    });
  }

  void performUpdate() {
    protocol.sendImageCrc();
    //protocol.sendImageLength();
    //protocol.sendImagePackages();
    // protocol.s
  }

  String intToString(int i, {int pad: 0}) {
    final String str = i.toString();
    final int paddingToAdd = pad - str.length;
    return (paddingToAdd > 0)
        ? "${List.filled(paddingToAdd, '0').join('')}$i"
        : str;
  }

  void setNotify(BluetoothCharacteristic ctx) async {
    if (!ctx.isNotifying) {
      await ctx.setNotifyValue(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      //  ViOTReceiver(protocol.getTx(), protocol),
      TextButton(
        style: TextButton.styleFrom(
          primary: Colors.deepPurple,
        ),
        child: const Text('Send data'),
        onPressed: () {
          performUpdate();
        },
      ),
      TextButton(
        style: TextButton.styleFrom(
          primary: Colors.deepPurple,
        ),
        child: const Text('Repeat last'),
        onPressed: () {
          protocol.repeat = 0;
          protocol.repeatSend('nope');
        },
      ),
      TextButton(
        style: TextButton.styleFrom(
          primary: Colors.deepPurple,
        ),
        child: const Text('Send command'),
        onPressed: () {
          perform();
        },
      ),
      Container(
          child: DropdownButton<commandNames>(
        value: chosenCommand,
        hint: Text(chosenCommand.toString().substring(13)),
        items: commandNames.values.map((commandNames name) {
          return DropdownMenuItem<commandNames>(
            value: name,
            child: Text(name.toString().substring(13)),
          );
        }).toList(),
        onChanged: (commandNames name) {
          setState(() {
            chosenCommand = name;
          });
        },
      )),
    ]);
  }
}

Widget backButton(BuildContext context) {
  return Container(
      alignment: Alignment.bottomLeft,
      child: NavigatorButtonWidget(
        text: AppLocalizations.of(context).translate('Back'),
        isPositionedOnLeft: true,
        onPressedCallback: () {
          Navigator.of(context).pop();
        },
      ));
}
