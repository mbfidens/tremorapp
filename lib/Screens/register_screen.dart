import 'package:Tremor_App/Backend/Logs/user_data_log.dart';
import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:Tremor_App/Widgets/fill_form_widget.dart';
import 'package:Tremor_App/Backend/User/local_user_profile.dart';
import 'package:flutter/services.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

class RegisterScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RegisterScreenState();
}

class RegisterScreenState extends State<RegisterScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _emailController2 = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordController2 = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _surnameController = TextEditingController();
  final TextEditingController _ageController = TextEditingController();
  final _controller = ScrollController();
  String firebaseErrorText = '';
  String inputErrorText = '';

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: Theme(
            data: ThemeData(
              highlightColor: ColorMain,
            ), //Does not work
            child: SafeArea(
              child: Scrollbar(
                  controller: _controller,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: SingleChildScrollView(
                        controller: _controller,
                        child: Column(children: <Widget>[
                          headlineContainer(),
                          formFields(),
                          getInputErrorMessage(),
                          getFirebaseErrorMessage(),
                          getRegisterButtonContainer(),
                          getBackButton()
                        ])),
                  )),
            )),
      ),
    );
  }

  Widget getBackButton() => Container(
        alignment: Alignment.center,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.grey,
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width * 0.5,
              height: MediaQuery.of(context).size.height * 0.06,
              child: AutoSizeText(
                AppLocalizations.of(context).translate("Back"),
                style: Theme.of(context).accentTextTheme.headline1,
              )),
        ),
      );

  Widget headlineContainer() => Container(
      padding: EdgeInsets.all(10),
      child: Text(
          AppLocalizations.of(context).translate("Tell us about yourself:"),
          textAlign: TextAlign.center,
          style: Theme.of(context).accentTextTheme.headline2));

  Widget getInputErrorMessage() => Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.03),
        alignment: Alignment.center,
        child: Text(
          inputErrorText ?? '',
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.red,
              fontSize: MediaQuery.of(context).size.width * 0.05),
        ),
      );

  Widget getFirebaseErrorMessage() => Container(
        alignment: Alignment.center,
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).size.width * 0.03),
        child: Text(
          firebaseErrorText ?? '',
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.red,
              fontSize: MediaQuery.of(context).size.width * 0.05),
        ),
      );

  Widget getRegisterButtonContainer() => Container(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.02),
        alignment: Alignment.center,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: ColorMain,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
          ),
          onPressed: () async {
            setState(() {
              if (checkInputData()) {
                setState(() {
                  firebaseErrorText = '';
                  inputErrorText = '';
                  register();
                });
              }
            });
          },
          child: Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width * 0.5,
              height: MediaQuery.of(context).size.width * 0.15,
              child: AutoSizeText(
                AppLocalizations.of(context).translate("Register"),
                style: Theme.of(context).accentTextTheme.headline1,
              )),
        ),
      );

  Widget formFields() => Column(children: <Widget>[
        FillFormWidget(
            Icon(
              Icons.account_circle,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            "Name",
            _nameController,
            false,
            context),
        FillFormWidget(
            Icon(
              Icons.account_circle,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            "Surname",
            _surnameController,
            false,
            context),
        FillFormWidget(
            Icon(
              Icons.calendar_today,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            "Age",
            _ageController,
            false,
            context),
        FillFormWidget(
            Icon(
              Icons.alternate_email,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            "Email",
            _emailController,
            false,
            context),
        FillFormWidget(
            Icon(
              Icons.alternate_email,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            "Repeat Email",
            _emailController2,
            false,
            context),
        FillFormWidget(
            Icon(
              Icons.lock,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            "Password",
            _passwordController,
            true,
            context),
        FillFormWidget(
            Icon(
              Icons.lock,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            "Repeat password",
            _passwordController2,
            true,
            context),
      ]);

  bool checkPasswordMatch() =>
      _passwordController.text == _passwordController2.text;

  bool checkEmailMatch() => _emailController.text == _emailController2.text;

  bool checkInputData() {
    inputErrorText = ' ';
    debugPrint('Checking input data on registration: ' +
        formKey.currentState.validate().toString() +
        ' ' +
        checkPasswordMatch().toString() +
        ' ' +
        checkEmailMatch().toString());
    if (!checkPasswordMatch()) {
      inputErrorText =
          AppLocalizations.of(context).translate("password don't mach");
      return false;
    } else if (!checkEmailMatch()) {
      inputErrorText =
          AppLocalizations.of(context).translate("email don't mach");
      return false;
    } else {
      if (formKey.currentState.validate()) {
        return true;
      } else {
        return false;
      }
    }
  }

  void register() async {
    debugPrint('Registration in progress');
    try {
      User user = (await _auth.createUserWithEmailAndPassword(
        email: _emailController.text.trim(),
        password: _passwordController.text,
      ))
          .user;
      if (user != null && firebaseErrorText == '') {
        setState(() {
          updateFirebaseProfile(user);
          registrationSuccessDialog();
          saveUserData(user);
        });
      }
    } on PlatformException catch (e) {
      debugPrint('Registration error - Platform exception: ' + e.message);
      setState(() {
        // firebaseErrorText = AppLocalizations.of(context).translate(e.message);
      });
    } catch (e) {
      debugPrint('Registration error: ' + e.message);
      setState(() {
        //firebaseErrorText = AppLocalizations.of(context).translate(e.message);
      });
    }
  }

  Future<void> updateFirebaseProfile(User user) async {
    //UserUpdateInfo newUserProfile = UserUpdateInfo();
    // newUserProfile.displayName = _nameController.text + ' ' + _surnameController.text;
    try {
      // await user.updateProfile(newUserProfile);
      await FirebaseAuth.instance.currentUser
          .updateProfile(displayName: user.displayName);
    } catch (e) {
      debugPrint('Error on firebase profile update: ' + e.toString());
    }
  }

  void saveUserData(User user) async {
    final LocalUserProfile profile = LocalUserProfile(
        name: _nameController.text,
        surname: _surnameController.text,
        age: int.parse(_ageController.text),
        email: _emailController.text,
        password: _passwordController.text,
        registrationDate: DateTime.now().toIso8601String(),
        id: user.uid);

    UserDataLog logger = UserDataLog(profile);
    logger.saveUserData();
  }

  void registrationSuccessDialog() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => AlertDialog(
                title: Container(
                    alignment: Alignment.center,
                    child: Text(
                      AppLocalizations.of(context)
                          .translate("Successfully registered!"),
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w300),
                      textAlign: TextAlign.center,
                    )),
                content: SingleChildScrollView(
                    child: Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(20),
                        alignment: Alignment.center,
                        child:
                            //TODO(Edvinas): Make button widget because they are the same
                            Container(
                          padding: const EdgeInsets.only(top: 20.0),
                          alignment: Alignment.center,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: ColorMain,
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width * 0.5,
                                height:
                                    MediaQuery.of(context).size.width * 0.12,
                                child: AutoSizeText(
                                  AppLocalizations.of(context)
                                      .translate("Continue"),
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .headline1,
                                )),
                          ),
                        ),
                      )
                    ],
                  ),
                )),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              )).whenComplete(() {
        Navigator.pop(context);
      });
    });
  }
}
