import 'package:Tremor_App/Backend/User/user_authentication.dart';
import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/User/local_user_profile.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:Tremor_App/Backend/Logs/user_data_log.dart';
import 'package:flutter/services.dart';
import 'package:Tremor_App/Backend/validator.dart';
import 'package:Tremor_App/Backend/User/global_user_profile.dart';

enum ProfileFields { NAME, SURNAME, AGE, WEIGHT, HEIGHT }

class ProfileScreen extends StatefulWidget {
  final UserAuthentication authentication;
  final Function() notifyParent;

  ProfileScreen(this.authentication, this.notifyParent);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final formKey = GlobalKey<FormState>();
  LocalUserProfile userProfile = LocalUserProfile();

  TextEditingController editProfileValueController =
      new TextEditingController();
  Validator validator = Validator();

  @override
  void initState() {
    super.initState();
    UserDataLog userDataLog = UserDataLog(userProfile);
    userDataLog.dataInit(userDataLog.profile.id).whenComplete(() {
      // paziuret ar ne null buna
      setState(() {
        userProfile = userDataLog.profile;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.05,
              left: MediaQuery.of(context).size.height * 0.03,
              right: MediaQuery.of(context).size.height * 0.03),
          child: Column(
            children: [
              getPhoto(),
              Divider(),
              getProfileDetails(),
              Divider(),
              getButtons(),
              getLabel(),
              getId(),
              getBottomSpace()
            ],
          ),
        ),
      ),
    );
  }

  Widget getPhoto() => Container(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.05),
      height: MediaQuery.of(context).size.width * 0.30,
      width: MediaQuery.of(context).size.width * 0.30,
      child:
          GlobalUserProfile(profile: widget.authentication.userProfile).avatar ??
              Icon(Icons.account_circle,
                  color: ColorMain,
                  size: MediaQuery.of(context).size.width * 0.25));

  getProfileDetails() => Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            getProfileItem(ProfileFields.NAME),
            getProfileItem(ProfileFields.SURNAME),
            getProfileItem(ProfileFields.AGE),
            getProfileItem(ProfileFields.WEIGHT),
            getProfileItem(ProfileFields.HEIGHT),
          ],
        ),
      );

  getProfileItem(ProfileFields currentProfileField) => ListTile(
        trailing: IconButton(
          iconSize: MediaQuery.of(context).size.width * 0.1,
          icon: Icon(Icons.edit),
          //icon: Icons.edit,
          color: ColorMain,
          onPressed: () {
            showProfileEditDialog(currentProfileField);
          },
        ),
        title: Text(
          getValueText(currentProfileField),
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w500,
          ),
        ),
        subtitle: Text(
          getTitleText(currentProfileField),
          style: TextStyle(color: ColorMain, fontWeight: FontWeight.w300),
        ),
      );

  String getTitleText(ProfileFields currentProfileField) {
    switch (currentProfileField) {
      case ProfileFields.NAME:
        return AppLocalizations.of(context).translate("Name");
      case ProfileFields.SURNAME:
        return AppLocalizations.of(context).translate("Surname");
      case ProfileFields.AGE:
        return AppLocalizations.of(context).translate("Age");
      case ProfileFields.WEIGHT:
        return AppLocalizations.of(context).translate("Weight");
      case ProfileFields.HEIGHT:
        return AppLocalizations.of(context).translate("Height");
      default:
        debugPrint(
            "ProfileScreen.getTitleText() Unrecognized ProfileFields value");
        return '';
    }
  }

  String getValueText(ProfileFields currentProfileField) {
    switch (currentProfileField) {
      case ProfileFields.NAME:
        return (userProfile.name != null
            ? userProfile.name
            : AppLocalizations.of(context).translate("unknown"));
      case ProfileFields.SURNAME:
        return (userProfile.surname != null
            ? userProfile.surname
            : AppLocalizations.of(context).translate("unknown"));
      case ProfileFields.AGE:
        return (userProfile.age != null
            ? userProfile.age.toString()
            : AppLocalizations.of(context).translate("unknown"));
      case ProfileFields.WEIGHT:
        return (userProfile.weight != null
            ? userProfile.weight.toString()
            : AppLocalizations.of(context).translate("unknown"));
      case ProfileFields.HEIGHT:
        return (userProfile.height != null
            ? userProfile.height.toString()
            : AppLocalizations.of(context).translate("unknown"));
      default:
        debugPrint(
            "ProfileScreen.getValueText() Unrecognized ProfileFields value");
        return '';
    }
  }

  getButtons() => Container(
        alignment: Alignment.bottomRight,
        padding: EdgeInsets.symmetric(
            vertical: MediaQuery.of(context).size.height * 0.04),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [getCancelButton(), getSaveButton()],
        ),
      );

  Widget getCancelButton() => Padding(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.02),
        child: RawMaterialButton(
          child: Container(
            width: MediaQuery.of(context).size.width * 0.3,
            height: MediaQuery.of(context).size.width * 0.12,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.all(Radius.circular(30)),
                border: Border.all(color: Colors.white)),
            child: Text(
              AppLocalizations.of(context).translate("Cancel"),
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: MediaQuery.of(context).size.width * 0.05),
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      );

  Widget getSaveButton() => Padding(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.02),
        child: RawMaterialButton(
          child: Container(
            width: MediaQuery.of(context).size.width * 0.3,
            height: MediaQuery.of(context).size.width * 0.12,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: ColorMain,
                borderRadius: BorderRadius.all(Radius.circular(30)),
                border: Border.all(color: Colors.white)),
            child: AutoSizeText(
              AppLocalizations.of(context).translate("Save"),
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: MediaQuery.of(context).size.width * 0.05),
            ),
          ),
          onPressed: () {
            saveData();
            widget.notifyParent();
            setNewData();
            Navigator.of(context).pop();
          },
        ),
      );

  Future<void> saveData() async {
    // UserUpdateInfo newUserProfile = UserUpdateInfo();
    //newUserProfile.displayName = userProfile.name + ' ' + userProfile.surname;
    try {
      // await widget.authentication.firebaseUser.updateProfile(newUserProfile);
      await FirebaseAuth.instance.currentUser.updateProfile(
          displayName: userProfile.name + ' ' + userProfile.surname);
    } catch (e) {
      debugPrint('Error on firebase profile update: ' + e.toString());
    }
    UserDataLog logger = UserDataLog(userProfile);
    logger.profile = userProfile;
    logger.saveUserData();
  }

  void setNewData() {
    widget.authentication.userProfile.name = userProfile.name;
    widget.authentication.userProfile.surname = userProfile.surname;
    widget.authentication.userProfile.age = userProfile.age;
    widget.authentication.userProfile.weight = userProfile.weight;
    widget.authentication.userProfile.height = userProfile.height;
  }

  void showProfileEditDialog(ProfileFields profilefield) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            title: Text(profilefield == ProfileFields.NAME
                ? AppLocalizations.of(context).translate("Edit name") + ': '
                : profilefield == ProfileFields.SURNAME
                    ? AppLocalizations.of(context).translate("Edit surname") +
                        ': '
                    : profilefield == ProfileFields.AGE
                        ? AppLocalizations.of(context).translate("Edit age") +
                            ': '
                        : profilefield == ProfileFields.WEIGHT
                            ? AppLocalizations.of(context)
                                    .translate("Edit weight") +
                                ': '
                            : profilefield == ProfileFields.HEIGHT
                                ? AppLocalizations.of(context)
                                        .translate("Edit height") +
                                    ': '
                                : ""),
            content: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.25,
              child: Wrap(children: [
                TextField(
                    keyboardType: profilefield == ProfileFields.NAME
                        ? TextInputType.text
                        : profilefield == ProfileFields.SURNAME
                            ? TextInputType.text
                            : profilefield == ProfileFields.AGE
                                ? TextInputType.number
                                : profilefield == ProfileFields.WEIGHT
                                    ? TextInputType.number
                                    : profilefield == ProfileFields.HEIGHT
                                        ? TextInputType.number
                                        : TextInputType.text,
                    controller: editProfileValueController,
                    decoration: InputDecoration(
                        labelText: profilefield == ProfileFields.NAME
                            ? (userProfile.name != null
                                ? userProfile.name
                                : AppLocalizations.of(context)
                                    .translate("unknown"))
                            : profilefield == ProfileFields.SURNAME
                                ? (userProfile.surname != null
                                    ? userProfile.surname
                                    : AppLocalizations.of(context)
                                        .translate("unknown"))
                                : profilefield == ProfileFields.AGE
                                    ? (userProfile.age != null
                                        ? userProfile.age.toString()
                                        : AppLocalizations.of(context)
                                            .translate("unknown"))
                                    : profilefield == ProfileFields.WEIGHT
                                        ? (userProfile.weight != null
                                            ? userProfile.weight.toString()
                                            : AppLocalizations.of(context)
                                                .translate("unknown"))
                                        : profilefield == ProfileFields.HEIGHT
                                            ? (userProfile.height != null
                                                ? userProfile.height.toString()
                                                : AppLocalizations.of(context)
                                                    .translate("unknown"))
                                            : "")),
                //TODO add error message when input is not correct
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        getDialogCancelButton(),
                        getDialogSaveButton(profilefield),
                      ]),
                ),
              ]),
            ),
          );
        });
  }

  String getValidatorMessage(ProfileFields profilefield) {
    bool isCorrect = profilefield == ProfileFields.NAME
        ? validator.checkIfNameIsCorrect(userProfile.name)
        : profilefield == ProfileFields.SURNAME
            ? validator.checkIfNameIsCorrect(userProfile.surname)
            : profilefield == ProfileFields.AGE
                ? validator.checkIfAgeIsCorrect(userProfile.age.toString())
                : profilefield == ProfileFields.WEIGHT
                    ? validator
                        .checkIfWeightIsCorrect(userProfile.weight.toString())
                    : profilefield == ProfileFields.HEIGHT
                        ? validator.checkIfHeightIsCorrect(
                            userProfile.height.toString())
                        : "";
    return isCorrect ? '' : 'Incorrect';
  }

  Widget getDialogCancelButton() => Flexible(
        child: Container(
          decoration: BoxDecoration(
            color: ColorSub7,
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(30),
                topRight: Radius.circular(30),
                bottomLeft: Radius.circular(30),
                topLeft: Radius.circular(30)),
          ),
          child: TextButton(
            onPressed: () {
              editProfileValueController.text = "";
              Navigator.pop(context);
            },
            child: AutoSizeText(
              AppLocalizations.of(context).translate("Cancel"),
              maxLines: 1,
              softWrap: false,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
      );

  Widget getDialogSaveButton(ProfileFields profilefield) => Flexible(
        child: Container(
          decoration: BoxDecoration(
            color: ColorMain,
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(30),
                topRight: Radius.circular(30),
                bottomLeft: Radius.circular(30),
                topLeft: Radius.circular(30)),
          ),
          child: TextButton(
            onPressed: () {
              if (editProfileValueController.text != "") {
                if (profilefield == ProfileFields.NAME &&
                    validator.checkIfNameIsCorrect(
                        editProfileValueController.text.toString())) {
                  updateName();
                } else if (profilefield == ProfileFields.SURNAME &&
                    validator.checkIfSurnameIsCorrect(
                        editProfileValueController.text.toString())) {
                  updateSurname();
                } else if (profilefield == ProfileFields.AGE &&
                    validator.checkIfAgeIsCorrect(
                        editProfileValueController.text.toString())) {
                  updateAge();
                } else if (profilefield == ProfileFields.WEIGHT &&
                    validator.checkIfWeightIsCorrect(
                        editProfileValueController.text.toString())) {
                  updateWeigth();
                } else if (profilefield == ProfileFields.HEIGHT &&
                    validator.checkIfHeightIsCorrect(
                        editProfileValueController.text.toString())) {
                  updateHeigth();
                }
              }
            },
            child: AutoSizeText(
              AppLocalizations.of(context).translate("Save"),
              maxLines: 1,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
      );

  void updateAge() {
    userProfile.age = int.parse(editProfileValueController.text);
    editProfileValueController.text = "";
    Navigator.pop(context);
  }

  void updateName() {
    userProfile.name = editProfileValueController.text;
    editProfileValueController.text = "";
    Navigator.pop(context);
  }

  void updateSurname() {
    userProfile.surname = editProfileValueController.text;
    editProfileValueController.text = "";
    Navigator.pop(context);
  }

  void updateWeigth() {
    userProfile.weight = int.parse(editProfileValueController.text);
    editProfileValueController.text = "";
    Navigator.pop(context);
  }

  void updateHeigth() {
    userProfile.height = int.parse(editProfileValueController.text);
    editProfileValueController.text = "";
    Navigator.pop(context);
  }

  Widget getLabel() => Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.1),
        child: Image.asset('lib/Assets/Images/ce_label.png'),
      );

  getId() => Container(
        height: MediaQuery.of(context).size.height * 0.1,
        alignment: Alignment.topCenter,
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.02),
        child: Text(
          (userProfile.id != null ? userProfile.id.toString() + '\n' : '\n'),
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
        ),
      );

  getBottomSpace() => Container(
        height: MediaQuery.of(context).size.width * 0.05,
      );
}
