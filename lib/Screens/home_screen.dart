import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Containers/menu_container.dart';
import 'package:Tremor_App/Widgets/date_picker_fab_widget.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:Tremor_App/Widgets/circular_progress_indicator.dart';
import 'package:Tremor_App/Widgets/notes_floating_button.dart';

class HomeScreen extends StatefulWidget {
  final String uid;

  HomeScreen({Key key, @required this.uid}) : super(key: key);
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<HomeScreen> {
  ScrollController controller = ScrollController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Theme(
            data: ThemeData(
              highlightColor: Colors.grey,
            ), //Does not work
            child: Scrollbar(
                controller: controller,
                isAlwaysShown: true,
                child: SingleChildScrollView(
                  controller: controller,
                  child: getScrollableView(),
                ))));
  }

  Widget getScrollableView() => Container(
        child: Stack(
          children: <Widget>[getBackground(), getMainContent(), getToolbar()],
        ),
      );

  Widget getBackground() => Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(color: Colors.white, height: MediaQuery.of(context).size.height * 0.28),
          getRoundedContainer()
        ],
      );

  Widget getToolbar() => Container(
        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.06, top: MediaQuery.of(context).size.height * 0),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.36,
        alignment: Alignment.centerRight,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            NotesFloatingButton(),
            ConstrainedBox(
              constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height * 0.01),
            ),
            DatePickerFabWidget(uid: widget.uid),
            getDate()
          ],
        ),
      );

  Widget getDate() {
    String selectedDate = MyStatefulWidget.of(context).getCurrentlySelectedDate().toString().substring(5, 10);
    bool isToday = MyStatefulWidget.of(context).getCurrentlySelectedDate().difference(DateTime.now()).inDays == 0 ? true : false;
    return Container(
        width: 56,
        height: MediaQuery.of(context).size.width * 0.08,
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.015),
        child: AutoSizeText(
          selectedDate,
          maxLines: 1,
          style: TextStyle(
              fontSize: MediaQuery.of(context).size.width * 0.035,
              color: Colors.grey.shade700,
              fontWeight: FontWeight.w500),
        ));
  }

  Widget getMainContent() => Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            getTopSpacing(),
            getResultContainer(),
            getMenuContainer(),
          ]);

  Widget getRoundedContainer() => Stack(children: <Widget>[
        Container(
          color: Colors.white,
          height: MediaQuery.of(context).size.height * 0.85,
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.9,
          decoration: BoxDecoration(
            color: ColorMain.withOpacity(0.8),
            borderRadius: BorderRadius.only(
                topLeft: Radius.elliptical(261, 51),
                topRight: Radius.elliptical(261, 51),
                bottomRight: Radius.elliptical(261, 51),
                bottomLeft: Radius.elliptical(261, 51)),
          ),
        ),
      ]);

  Widget getTopSpacing() => Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width,
        height: getScreenHeight() * 0.06,
      );

  Widget getResultContainer() => Container(
      padding: EdgeInsets.only(left: 10, right: 10, bottom: 30),
      width: MediaQuery.of(context).size.width,
      height: getScreenHeight() * 0.3,
      child: CustomCircularProgressIndicator());

  Widget getMenuContainer() => Container(
        width: MediaQuery.of(context).size.width,
        child: MenuContainer(),
      );

  double getScreenHeight() => MediaQuery.of(context).size.height - kBottomNavigationBarHeight;
}
