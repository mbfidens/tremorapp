import 'package:flutter/material.dart';
import 'package:Tremor_App/Screens/edit_note_screen.dart';
import 'package:Tremor_App/Widgets/Notes/add_note_widget.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:Tremor_App/Backend/Models/notes_model.dart';
import 'package:Tremor_App/Widgets/filled_note_widget.dart';
import 'package:Tremor_App/Backend/Logs/note_logs.dart';

class EvaluationScreen extends StatefulWidget {
  EvaluationScreen({Key key, @required this.uid}) : super(key: key);
  final String uid;

  @override
  _EvaluationScreenState createState() => _EvaluationScreenState();
}

class _EvaluationScreenState extends State<EvaluationScreen> {
  bool isEditingNoteActive = false;
  NotesModel note = NotesModel.empty();
  NoteLogs logs = NoteLogs();

  @override
  void initState() {
    super.initState();
    logs.dataInit(context, widget.uid);
  }

  @override
  Widget build(BuildContext context) {
    return isEditingNoteActive
        ? EditNoteScreen(
            notifyParent: onNewNoteEditInProgress,
            uid: widget.uid,
            note: note,
          )
        : getDefaultScreen(context);
  }

  void onNewNoteEditInProgress({NotesModel updatedNote}) {
    setState(() {
      isEditingNoteActive = isEditingNoteActive ? false : true;
      if (updatedNote != null) {
        note = updatedNote;
      }
    });
  }

  Widget getDefaultScreen(BuildContext context) => Column(children: <Widget>[
        getNotesWidget(),
        Container(
          padding:
              EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
          width: MediaQuery.of(context).size.width * 0.6,
          child: getRowOfButtons(),
        )
      ]);

  void onRatingChanged(double newRating) {
    setState(() {});
  }

  Widget getNotesWidget() {
    return note == null || note.isEmpty
        ? AddNoteWidget(onNewNoteEditInProgress)
        : FilledNoteWidget(
            note: note,
            notifyParent: onNewNoteEditInProgress,
          );
  }

  Widget getRowOfButtons() => Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[getCompletedButton(context)],
      );

  Widget getCompletedButton(BuildContext context) => FloatingActionButton(
        child: Icon(
          Icons.done,
          color: Colors.white,
        ),
        backgroundColor: ColorMain,
        onPressed: () {
          saveOrDiscardNewNote();
          Navigator.of(context).pop();
        },
      );

  void saveOrDiscardNewNote() {
    if (!note.isEmpty) {
      logs.addLog(note, widget.uid);
      logs.logFirebase(logs.savedData, widget.uid);
    }
  }
}
