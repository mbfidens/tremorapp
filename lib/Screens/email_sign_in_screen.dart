import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:Tremor_App/Screens/register_screen.dart';
import 'package:Tremor_App/Widgets/fill_form_widget.dart';
import 'package:Tremor_App/Backend/User/user_authentication.dart';
import 'package:Tremor_App/Resources/Exceptions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:Tremor_App/Dialogs/password_reset_dialog.dart';
import 'package:Tremor_App/Widgets/restart_widget.dart';
import 'package:Tremor_App/Widgets/warning_widget.dart';

class EmailSignInScreen extends StatefulWidget {
  final VoidCallback backFunction;
  final UserAuthentication authentication;

  EmailSignInScreen(this.backFunction, this.authentication);

  @override
  State<StatefulWidget> createState() => _EmailSignInScreenState();
}

class _EmailSignInScreenState extends State<EmailSignInScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  static String errorText = '';
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  final GlobalKey<FormState> resetEmailFormKey = GlobalKey<FormState>();

  double getSpacing(BuildContext context) => MediaQuery.of(context).size.height * 0.01;

  @override
  void dispose() {
    errorText = '';
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        widget.backFunction();
        return false;
      },
      child: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Wrap(
            alignment: WrapAlignment.center,
            children: <Widget>[
              getEmailField(),
              getPasswordField(),
              getPasswordResetButton(),
              WarningWidget(errorText),
              getSignInButton(),
              getRegisterButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget getEmailField() => FillFormWidget(
      Icon(
        Icons.alternate_email,
        size: getSpacing(context) * 3,
      ),
      AppLocalizations.of(context).translate("Email"),
      emailController,
      false,
      context);

  Widget getPasswordField() => FillFormWidget(
      Icon(
        Icons.lock,
        size: getSpacing(context) * 3,
      ),
      AppLocalizations.of(context).translate("Password"),
      passwordController,
      true,
      context);

  Widget getSignInButton() => Container(
        padding: EdgeInsets.only(top: getSpacing(context)),
        alignment: Alignment.center,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: ColorMain,
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
          ),
          onPressed: () async {
            if (formKey.currentState.validate()) {
              signInWithEmailAndPassword();
            }
          },
          child: Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width * 0.5,
              height: MediaQuery.of(context).size.height * 0.06,
              child: Text(
                AppLocalizations.of(context).translate("Sign in"),
                style: getButtonTextStyle(context),
              )),
        ),
      );

  Widget getToolbar() => Container(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [getPasswordResetButton()],
        ),
      );

  Widget getBackButton() => Container(
        alignment: Alignment.center,
        child: IconButton(
          icon: Icon(Icons.arrow_back_sharp),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      );

  Widget getPasswordResetButton() => TextButton(
        child: Text(
          AppLocalizations.of(context).translate("Reset password"),
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal),
        ),
        onPressed: () {
          return showDialog(
              context: context,
              builder: (context) {
                PasswordResetDialog dialog = PasswordResetDialog(() {
                  setState(() {});
                }, firebaseAuth, resetEmailFormKey);
                return dialog.getDialog(context);
              });
        },
      );

  Widget getRegisterButton() => Container(
        padding: EdgeInsets.only(top: getSpacing(context) * 2, bottom: getSpacing(context)),
        alignment: Alignment.center,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.blue,
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
          ),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterScreen()));
          },
          child: Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width * 0.5,
              height: MediaQuery.of(context).size.height * 0.06,
              child: AutoSizeText(
                AppLocalizations.of(context).translate("Register"),
                style: getButtonTextStyle(context),
              )),
        ),
      );

  void signInWithEmailAndPassword() async {
    errorText = '';
    try {
      await widget.authentication
          .signInWithEmailAndPassword(context, email: emailController.text, password: passwordController.text);
      if (widget.authentication != null && errorText == '') {
        setState(() {
          RestartWidget.restartApp(context);
          Navigator.pop(context);
        });
      }
    } on LoginException catch (e) {
      debugPrint('EmailSignInScreen.signInWithEmailAndPassword() + ' + e.toString());
      setState(() {
        errorText = emailController.text.isNotEmpty
            ? AppLocalizations.of(context).translate(e.decodedMessage)
            : AppLocalizations.of(context).translate('Enter all data');
      });
    } catch (e) {
      debugPrint('EmailSignInScreen.signInWithEmailAndPassword() + ' + e.toString());
    }
  }

  TextStyle getButtonTextStyle(BuildContext context) =>
      TextStyle(fontSize: MediaQuery.of(context).size.width * 0.05, color: Colors.white);
}
