import 'package:flutter/material.dart';
import 'package:Tremor_App/Containers/menu_container.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:Tremor_App/Containers/circular_slider_container.dart';

class ProgressScreen extends StatelessWidget {
 final String data = '30';

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Container(
            padding: const EdgeInsets.all(0.0),
            alignment: Alignment.center,
            width: 1.7976931348623157e+308,
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          new Container(
              padding: const EdgeInsets.fromLTRB(2, 0, 2, 30),
              width: 1.7976931348623157e+308,
              height: MediaQuery.of(context).size.height * 0.31,
              child: Stack(children: <Widget>[
                CircularSliderContainer(),
                Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child:RawMaterialButton(
                        onPressed: () {},
                        elevation: 2.0,
                        child: Container(
                            child: new Center(
                                child: new Text(data,
                                    style: new TextStyle(
                                      fontSize: MediaQuery.of(context).size.height * 0.10,
                                      color: const Color(0xFFFFFFFF),
                                      fontWeight: FontWeight.w600,
                                    ))),
                            width: MediaQuery.of(context).size.height * 0.2,
                            height: MediaQuery.of(context).size.height * 0.2,
                            decoration: new BoxDecoration(
                              color: ColorMain,
                              shape: BoxShape.circle,
                            )),
                        shape: CircleBorder())),
              ])),
          new Container(
            child: MenuContainer(),
            width: 1.7976931348623157e+308,
            height: MediaQuery.of(context).size.height * 0.44,
          )
        ]);
  }
}
