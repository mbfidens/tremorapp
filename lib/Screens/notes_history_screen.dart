import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:Tremor_App/Widgets/Notes/saved_notes_list_widget.dart';

class NotesHistoryScreen extends StatefulWidget {
  final String uid;

  NotesHistoryScreen({Key key, @required this.uid}) : super(key: key);
  @override
  _NotesHistoryScreenState createState() => _NotesHistoryScreenState();
}

class _NotesHistoryScreenState extends State<NotesHistoryScreen> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return getMainContainer();
  }

  Widget getMainContainer() => Container(
    child: SingleChildScrollView(
      child: Column(
        children: [SavedNotesListWidget(uid: widget.uid,)],
      ),
    ),
  );
}