import 'dart:convert';

import 'package:Tremor_App/Backend/Logs/note_logs.dart';
import 'package:Tremor_App/Backend/Models/notes_model_list.dart';
import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/Models/notes_model.dart';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

class EditNoteScreen extends StatefulWidget {
  final String uid;
  final Function({NotesModel updatedNote}) notifyParent;
  final NotesModel note;
  final bool resizeOnKeyboard;

  EditNoteScreen(
      {@required this.notifyParent,
      @required this.note,
      @required this.uid,
      this.resizeOnKeyboard = true});

  @override
  _EditNoteScreenState createState() => _EditNoteScreenState();
}

class _EditNoteScreenState extends State<EditNoteScreen> {
  bool isDirty = false;
  bool isNoteNew = true;
  FocusNode titleFocus = FocusNode();
  FocusNode contentFocus = FocusNode();
  bool importantStatusChanged = false;
  ScrollController controller = ScrollController();

  TextEditingController titleController = TextEditingController();
  TextEditingController contentController = TextEditingController();

  NotesModel currentNote;
  NoteLogs logs = NoteLogs();

  @override
  void initState() {
    super.initState();
    if (widget.note == null || widget.note.isEmpty) {
      currentNote = NotesModel.empty();
      isNoteNew = true;
    } else {
      currentNote = widget.note;
    }
    titleController.text = widget.note.title;
    contentController.text = widget.note.content;
    logs.dataInit(context, widget.uid);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: onBackPress,
        child: KeyboardVisibilityBuilder(builder: (context, isKeyboardVisible) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Flexible(
                child: Container(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * 0.06),
                  width: MediaQuery.of(context).size.width,
                  child: Scrollbar(
                    controller: controller,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      controller: controller,
                      child: Column(
                        children: <Widget>[
                          getNoteTitleWidget(context),
                          Container(
                              height:
                                  MediaQuery.of(context).size.height * 0.02),
                          getMainTextWidget(),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            height: MediaQuery.of(context).size.height * 0.1,
                            color:
                                Theme.of(context).canvasColor.withOpacity(0.3),
                            child: getToolbar(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        }));
  }

  double getHeight(bool isKeyboardVisible) {
    double height = MediaQuery.of(context).size.height * 0.45;
    if (widget.resizeOnKeyboard && isKeyboardVisible) {
      height = MediaQuery.of(context).size.height * 0.25;
    }
    return height;
  }

  Widget getNoteTitleWidget(BuildContext context) => Container(
        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
        decoration: BoxDecoration(
            border: Border.all(color: ColorMain),
            borderRadius: BorderRadius.all(
              Radius.circular(40.0),
            )),
        child: TextField(
          inputFormatters: [
            LengthLimitingTextInputFormatter(15),
          ],
          focusNode: titleFocus,
          autofocus: false,
          controller: titleController,
          keyboardType: TextInputType.multiline,
          textCapitalization: TextCapitalization.sentences,
          maxLines: 1,
          onSubmitted: (text) {
            titleFocus.unfocus();
            FocusScope.of(context).requestFocus(contentFocus);
          },
          onChanged: (value) {
            markTitleAsDirty(value);
          },
          textInputAction: TextInputAction.next,
          style: TextStyle(
              fontFamily: 'ZillaSlab',
              fontSize: MediaQuery.of(context).size.width * 0.04,
              fontWeight: FontWeight.w700),
          decoration: InputDecoration.collapsed(
            hintText: AppLocalizations.of(context).translate('Enter a title'),
            hintStyle: TextStyle(
                color: Colors.grey.shade400,
                fontSize: MediaQuery.of(context).size.width * 0.04,
                fontFamily: 'ZillaSlab',
                fontWeight: FontWeight.w700),
            border: InputBorder.none,
          ),
        ),
      );

  Widget getMainTextWidget() => Container(
        height: MediaQuery.of(context).size.height * 0.2,
        decoration: BoxDecoration(
            border: Border.all(color: ColorMain),
            borderRadius: new BorderRadius.all(
              Radius.circular(40.0),
            )),
        padding: EdgeInsets.all(16.0),
        child: TextField(
          focusNode: contentFocus,
          controller: contentController,
          keyboardType: TextInputType.multiline,
          textCapitalization: TextCapitalization.sentences,
          maxLines: null,
          onChanged: (value) {
            markContentAsDirty(value);
          },
          style: TextStyle(
              fontSize: MediaQuery.of(context).size.width * 0.04,
              fontWeight: FontWeight.w500),
          decoration: InputDecoration.collapsed(
            hintText: AppLocalizations.of(context).translate('Enter a note...'),
            hintStyle: TextStyle(
                color: Colors.grey.shade400,
                fontSize: MediaQuery.of(context).size.width * 0.04,
                fontWeight: FontWeight.w500),
            border: InputBorder.none,
          ),
        ),
      );

  Widget getToolbar() => Row(
        children: <Widget>[
          Spacer(),
          getImportantButton(),
          getDeleteButton(),
          getSaveButton()
        ],
      );

  Widget getImportantButton() => IconButton(
      tooltip: AppLocalizations.of(context).translate('Mark note as important'),
      icon: Icon(
        currentNote.isImportant ? Icons.flag : Icons.outlined_flag,
      ),
      onPressed: handleImportant);

  Widget getDeleteButton() => IconButton(
        icon: Icon(
          Icons.delete_outline,
        ),
        onPressed: () {
          handleDelete();
        },
      );

  Widget getSaveButton() => AnimatedContainer(
        duration: Duration(milliseconds: 500),
        curve: Curves.decelerate,
        child: Container(
          decoration: BoxDecoration(
              color: ColorMain,
              border: Border.all(color: ColorMain),
              borderRadius: BorderRadius.all(
                Radius.circular(30.0),
              )),
          child: IconButton(
            color: Colors.white,
            icon: Icon(
              Icons.done,
            ),
            onPressed: onSavePressed,
          ),
        ),
      );

  void onSavePressed() async {
    currentNote.title = titleController.text;
    currentNote.content = contentController.text;
    titleFocus.unfocus();
    contentFocus.unfocus();
    widget.notifyParent(updatedNote: currentNote);
    logs.savedData.add(currentNote);
    logs.logFirebase(logs.savedData, widget.uid);
  }

  Future<bool> onBackPress() {
    handleBack();
    return Future.value(false);
  }

  void markTitleAsDirty(String title) {
    setState(() {
      isDirty = true;
    });
  }

  void markContentAsDirty(String content) {
    setState(() {
      isDirty = true;
    });
  }

  void markImportantAsDirty() {
    setState(() {
      currentNote.isImportant = !currentNote.isImportant;
      importantStatusChanged = !importantStatusChanged;
      isDirty = true;
    });
  }

  void handleDelete() async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30.0))),
            title: Text(
              AppLocalizations.of(context).translate('Are you sure?'),
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              TextButton(
                child: Text(AppLocalizations.of(context).translate("No"),
                    style: TextStyle(
                        color: ColorMain,
                        fontSize: MediaQuery.of(context).size.width * 0.05)),
                onPressed: () {
                  widget.notifyParent();
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text(AppLocalizations.of(context).translate("Yes"),
                    style: TextStyle(
                        color: ColorMain,
                        fontSize: MediaQuery.of(context).size.width * 0.05)),
                onPressed: () {
                  widget.notifyParent(updatedNote: NotesModel.empty());

                  logs.removeDeletedNoteFromCloud(logs.savedData, currentNote);
                  logs.saveWithNewData(widget.uid);

                  logs.logFirebase(logs.savedData, widget.uid);
                  Navigator.of(context).pop();
                  /* NoteLogs()
                      .readRemotelySavedLogs(context, widget.uid)
                      .then((downlaodedData) => NoteLogs()
                          .removeDeletedNoteFromCloud(
                              downlaodedData, currentNote))
                      .then((value) => NoteLogs().logFirebase(
                      value,
                          widget.uid)

                  );*/
                },
              ),
            ],
          );
        });
  }

  void handleBack() {
    if (importantStatusChanged) {
      markImportantAsDirty();
    }
    if (titleController.text.isEmpty && contentController.text.isEmpty) {
      if (widget.note.isEmpty) {
        widget.notifyParent(updatedNote: NotesModel.empty());
      } else {
        widget.notifyParent(updatedNote: widget.note);
      }
    } else {
      if (widget.note.isEmpty) {
        widget.notifyParent(updatedNote: NotesModel.empty());
      } else {
        widget.notifyParent(updatedNote: widget.note);
      }
    }
  }

  void handleImportant() {
    if (titleController.text.trim().isNotEmpty &&
        contentController.text.trim().isNotEmpty) {
      markImportantAsDirty();
    }
  }
}
