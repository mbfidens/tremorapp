import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:Tremor_App/Widgets/intro_text_widget.dart';
import 'package:Tremor_App/Widgets/meditation_guide_widget.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'dart:async';
import 'package:wakelock/wakelock.dart';

class RelaxationScreen extends StatefulWidget {
  @override
  _RelaxationScreenState createState() => _RelaxationScreenState();
}

class _RelaxationScreenState extends State<RelaxationScreen> {
  static const int oneBreathDurationSeconds = 4;
  static const int breathingSessionMinutes = 2;

  bool isSessionStarted = false;
  bool isIntroStarted = false;
  int breathCount = 1;
  double buttonTextOpacity = 1;
  double finishTextOpacity = 0;
  double breathingSessionTime;

  @override
  void initState() {
    super.initState();
    breathingSessionTime = breathingSessionMinutes * 60 / oneBreathDurationSeconds;
    Timer.periodic(Duration(seconds: oneBreathDurationSeconds), (Timer t) {
      if (isSessionStarted) {
        setState(() {
          breathCount++;
          showEndingWords();
          resetSessionIfFinished();
        });
      }
    });
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void dispose() {
    Wakelock.disable();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          getFinishText(),
          getButton(),
          isSessionStarted || isIntroStarted ? getFadingText() : Container(),
        ],
      ),
    );
  }

  Widget getButton() => TextButton(
    style: ButtonStyle(
      overlayColor: MaterialStateProperty.all(Colors.transparent),
    ),
        onPressed: () {
          setState(() {
            if (!isSessionStarted && !isIntroStarted) {
              buttonTextOpacity = 0;
              isIntroStarted = true;
              Wakelock.enable();
            }
          });
        },
        child: getIntroText(),
      );

  Widget getIntroText() => Stack(
        children: <Widget>[
          isIntroStarted
              ? Container(
                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.width * 0.2),
                  alignment: Alignment.bottomCenter,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: IntroTextWidget(onIntroFinished))
              : Container(),
          Container(
            alignment: Alignment.bottomCenter,
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.width * 0.2),
            color: Colors.transparent,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: AnimatedOpacity(
              opacity: buttonTextOpacity,
              duration: Duration(milliseconds: 500),
              child: Text(AppLocalizations.of(context).translate("Tap to start"),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: ColorMain, fontSize: MediaQuery.of(context).size.width * 0.10, fontWeight: FontWeight.w300)),
            ),
          )
        ],
      );

  Widget getFadingText() =>
      MeditationGuideWidget(oneBreathDurationSeconds * 1000, AppLocalizations.of(context).translate("breath in"), AppLocalizations.of(context).translate("breath out"), resetSessionIfFinished);

  void onIntroFinished() {
    isSessionStarted = true;
  }

  void resetSessionIfFinished() {
    if (breathCount >= breathingSessionTime) {
      isSessionStarted = false;
      isIntroStarted = false;
      buttonTextOpacity = 1;
      breathCount = 1;
      finishTextOpacity = 0;
    }
  }

  void showEndingWords(){
    if (breathCount >= breathingSessionTime-1) {
      finishTextOpacity = 1;
    }
  }

  Widget getFinishText() => Container(
        alignment: Alignment.topCenter,
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.2),
        color: Colors.transparent,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: AnimatedOpacity(
          opacity: finishTextOpacity,
          duration: Duration(milliseconds: 500),
          child: Text(AppLocalizations.of(context).translate("Well done!"),
              textAlign: TextAlign.center,
              style:
                  TextStyle(color: ColorMain, fontSize: MediaQuery.of(context).size.width * 0.10, fontWeight: FontWeight.w300)),
        ),
      );
}
