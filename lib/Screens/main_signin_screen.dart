import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:Tremor_App/Backend/User/user_authentication.dart';
import 'package:Tremor_App/Screens/email_sign_in_screen.dart';
import 'package:Tremor_App/Backend/User/global_user_profile.dart';
import 'package:Tremor_App/Widgets/restart_widget.dart';
import 'dart:async';
import 'dart:io';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:Tremor_App/Widgets/warning_widget.dart';

class MainSignInScreen extends StatefulWidget {
  final UserAuthentication authentication;

  MainSignInScreen(this.authentication);

  @override
  State<StatefulWidget> createState() => MainSignInScreenState();
}

class MainSignInScreenState extends State<MainSignInScreen> {
  Widget overlayWidget;
  String notificationString = '';

  @override
  void initState() {
    Timer.periodic(Duration(milliseconds: 200), (timer) {
      if (GlobalUserProfile(profile: widget.authentication.userProfile).isSigned) {
        timer.cancel();
        Navigator.pop(context);
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return overlayWidget ??
        WillPopScope(
            onWillPop: () async => false,
            child: Wrap(
              alignment: WrapAlignment.center,
              children: <Widget>[
                Platform.isAndroid ? getGoogleButton() : Container(),
                Platform.isAndroid ? getSpacer() : Container(),
                getEmailButton(),
                getSpacer(),
                Platform.isIOS ? getAppleButton() : Container(),
                WarningWidget(notificationString)
              ],
            ));
  }

  void deactivateWidget() {
    setState(() {
      overlayWidget = null;
    });
  }

  Widget getSpacer() => Container(
        height: MediaQuery.of(context).size.height * 0.005,
      );

  Widget getGoogleButton() => SignInButton(
        Buttons.GoogleDark,
        text: AppLocalizations.of(context).translate("Google"),
        onPressed: () {
          showNoInternetIfNeeded();
          widget.authentication.signInWithGoogle(context).whenComplete(() {
            if (GlobalUserProfile(profile: widget.authentication.userProfile).isSigned) {
              RestartWidget.restartApp(context);
              Navigator.pop(context);
            }
          }).catchError((e) {
            debugPrint('SignInScreen.getGoogleButton() ' + e.toString());
            Navigator.pop(context);
          });
        },
      );

  Widget getEmailButton() => SignInButton(
        Buttons.Email,
        text: AppLocalizations.of(context).translate("Email"),
        onPressed: () {
          checkIfInternetIsAvailable();
          setState(() {
            overlayWidget = EmailSignInScreen(deactivateWidget, widget.authentication);
          });
        },
      );

  Widget getAppleButton() => FutureBuilder(
      future: getVersion(),
      builder: (context, snapshot) {
        if (snapshot.data && snapshot.hasData) {
          return SignInButton(
            Buttons.AppleDark,
            text: AppLocalizations.of(context).translate("Sign in with Apple"),
            onPressed: () async {
              checkIfInternetIsAvailable();
              widget.authentication.signInWithApple(context).whenComplete(() {
                if (GlobalUserProfile(profile: widget.authentication.userProfile).isSigned) {
                  RestartWidget.restartApp(context);
                  Navigator.pop(context);
                }
              }).catchError((e) {
                debugPrint('SignInScreen.getAppleButton() ' + e.toString());
                Navigator.pop(context);
              });
            },
          );
        } else
          return Container();
      });

  void showNoInternetIfNeeded() async {
    notificationString = (await checkIfInternetIsAvailable()) ? '' : 'No internet access';
    setState(() {});
  }

  Future<bool> getVersion() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      if (iosInfo.systemVersion.contains('.')) {
        if (double.parse(iosInfo.systemVersion.substring(0, iosInfo.systemVersion.indexOf('.'))) >= 13) {
          return true;
        }
        return false;
      }

      if (double.parse(iosInfo.systemVersion) >= 13) {
        return true;
      }
      return false;
    } else {
      return false;
    }
  }

  Future<bool> checkIfInternetIsAvailable() async {
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }
}
