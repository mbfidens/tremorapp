import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  int textIndex = 0;
  int firstTimeShowing = 0;

  static const List<String> mainTextScript = [
    "Thank you for\nusing ViliMap",
    "Our goal is to assist you on tremor management",
    "And increase\nquality of life",
    "There are 6 main ingredients to achieve this goal",
    "Firstly - exercising. 30 minutes a day can make a difference!",
    "Secondly - relaxation. Mental health is equally important.",
    "We recommend at least 2 relaxations per day. ",
    "You can use ViliMap relaxation timer",
    "Sleeping 7 to 8 hours a day is mandatory too",
    "Less or too much sleep may have negative effects",
    "There are some smart devices that may improve your daily life",
    "Vilim Ball is one of them",
    "It is known that maintaining healthy weight improves tremor",
    "Therefore, eating at least 2 healthy meals per day is a must!",
    "And the easy part - stay away from caffeine",
    "You can monitor your lifestyle using history window",
    "But the main information is shown here",
    "Your goal is to reach 100% every single day!",
    "This is it, we hope you will enjoy ViliMap!"
  ];

  @override
  void initState() {
    super.initState();
    Future<SharedPreferences> sharedPrefs = SharedPreferences.getInstance();
    sharedPrefs.then((SharedPreferences prefs) {
      if (prefs.getInt("isWelcomedBefore") == 1) {
        firstTimeShowing = 1;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          getBackground(),
          getCenterText(),
          getButton(),
          getBottomLineWidgets(),
        ],
      ),
    );
  }

  Widget getBackground() {
    if (textIndex < 3) {
      return Container(
          padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: ColorMain);
    } else {
      return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: FittedBox(
          fit: BoxFit.fill,
          child: Image(
            image: AssetImage('lib/Assets/Images/' + getImageName() + '.png'),
          ),
        ),
      );
    }
  }

  String getImageName() {
    switch (textIndex) {
      case 4:
        return 'init1';
      case 5:
        return 'init2';
      case 6:
        return 'init2';
      case 7:
        return 'init3';
      case 8:
        return 'init4';
      case 9:
        return 'init4';
      case 10:
        return 'init5';
      case 11:
        return 'init5';
      case 12:
        return 'init6';
      case 13:
        return 'init6';
      case 14:
        return 'init7';
      case 15:
        return 'init8';
      case 16:
        return 'init9';
      case 17:
        return 'init9';
      case 18:
        return 'init0';
      default:
        return 'init0';
    }
  }

  Widget getCenterText() => textIndex < 3
      ? Container(
          padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.transparent,
          alignment: Alignment.centerRight,
          child: Text(
              AppLocalizations.of(context).translate(mainTextScript[textIndex]),
              textAlign: TextAlign.end,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: MediaQuery.of(context).size.width * 0.08)))
      : getComments();

  Widget getComments() => Container(
      padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.1,
          left: MediaQuery.of(context).size.width * 0.05,
          right: MediaQuery.of(context).size.width * 0.05),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Colors.transparent,
      alignment: Alignment.topCenter,
      child: Text(
          AppLocalizations.of(context).translate(mainTextScript[textIndex]),
          textAlign: TextAlign.right,
          style: TextStyle(
              color: ColorMain,
              fontSize: MediaQuery.of(context).size.width * 0.08,
              fontWeight: FontWeight.bold)));

  Widget getBottomLineWidgets() => SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  children: [
                    textIndex > 0
                        ? IconButton(
                            icon: const Icon(Icons.arrow_back_rounded),
                            color: Colors.white,
                            onPressed: () {
                              setState(() {
                                if (textIndex > 0) {
                                  textIndex--;
                                }
                              });
                            },
                          )
                        : Container(),
                    firstTimeShowing == 1
                        ? IconButton(
                            icon: const Icon(Icons.cancel_outlined),
                            color: Colors.white,
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          )
                        : Container(),
                    TextButton(
                      child: AutoSizeText(
                        AppLocalizations.of(context)
                            .translate("Tap to continue..."),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () {
                        setState(() {
                          makeTransition();
                        });
                      },
                    )
                  ],
                ),
              ],
            ),
          ],
        ),
      );

  Widget getButton() => TextButton(
        style: ButtonStyle(
          overlayColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
            return null; // Defer to the widget's default.
          }),
        ),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.transparent,
        ),
        onPressed: () {
          setState(() {
            makeTransition();
          });
        },
      );

  void makeTransition() {
    if (textIndex < mainTextScript.length - 1) {
      textIndex++;
    } else {
      Future<SharedPreferences> sharedPrefs = SharedPreferences.getInstance();
      sharedPrefs.then((SharedPreferences prefs) {
        prefs.setInt("isWelcomedBefore", 1);
      });
      Navigator.of(context).pop();
    }
  }
}
