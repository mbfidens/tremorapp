import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:Tremor_App/Containers/history_graph_container.dart';
import 'package:Tremor_App/Containers/options_selector_container.dart';
import 'package:Tremor_App/Widgets/multiple_choice_widget.dart';
import 'package:Tremor_App/Screens/notes_history_screen.dart';
import 'package:fab_circular_menu/fab_circular_menu.dart';
import 'package:Tremor_App/Widgets/navigator_button_widget.dart';

class HistoryScreen extends StatefulWidget {
  final String uid;
  HistoryScreen({Key key, @required this.uid}) : super(key: key);

  @override
  _MyHistoryScreenState createState() => new _MyHistoryScreenState();
}

class _MyHistoryScreenState extends State<HistoryScreen> {
  final PageController pageController = PageController(initialPage: 0);
  final GlobalKey<FabCircularMenuState> fabKey = GlobalKey();
  int currentPage = 0;

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: pageController,
      onPageChanged: pageChangeCallback,
      children: <Widget>[getGraphPage(), getNotesPage()],
    );
  }

  void pageChangeCallback(int newPage) {
    setState(() {
      currentPage = newPage;
    });
  }

  Widget getGraphPage() {
    return Stack(
      children: <Widget>[
        Container(
            child: ListView(children: <Widget>[
          getDaysSelector(),
          Divider(),
          getHistoryChart(),
          Divider(),
          OptionsSelectorContainer(),
        ])),
        getNavigatorButtons()
      ],
    );
  }

  Widget getNotesPage() => SafeArea(
        child: Stack(
          children: <Widget>[NotesHistoryScreen(uid: widget.uid), getNavigatorButtons()],
        ),
      );

  Widget getNavigatorButtons() {
    return currentPage == 0 ? getFirstPageNavigator() : getSecondPageNavigator();
  }

  Widget getFirstPageNavigator() => Container(
      alignment: Alignment.bottomRight,
      child: NavigatorButtonWidget(
        text: AppLocalizations.of(context).translate("Notes"),
        onPressedCallback: () {
          pageController.animateToPage(1, duration: Duration(milliseconds: 500), curve: Curves.ease);
        },
      ));

  Widget getSecondPageNavigator() => Container(
        child: Container(
            alignment: Alignment.bottomLeft,
            child: NavigatorButtonWidget(
              text: AppLocalizations.of(context).translate('Analysis'),
              isPositionedOnLeft: true,
              onPressedCallback: () {
                pageController.animateToPage(0, duration: Duration(milliseconds: 500), curve: Curves.ease);
              },
            )),
      );

  Widget getDaysSelector() => Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.06,
        child: MultipleChoiceWidget(<String>[
          AppLocalizations.of(context).translate("Week"),
          '10 ' + AppLocalizations.of(context).translate("days"),
          '2 ' + AppLocalizations.of(context).translate("weeks")
        ]),
      );

  Widget getHistoryChart() => Container(
        alignment: Alignment.center,
        width: 1.7976931348623157e+308,
        height: MediaQuery.of(context).size.height * 0.28,
        child: HistoryGraphContainer(),
      );
}
