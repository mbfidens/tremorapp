class UserDataLocallyDoesNotExist implements Exception {
    String toString() {
        return 'Cant find matching user data locally';
    }
}

class CantReadLocalUserData implements Exception {
    String message;

    CantReadLocalUserData([String message = 'Cant read local user data correctly']) {
        this.message = message;
    }

    @override
    String toString() {
        return message;
    }
}

class RemoteLogDoesNotExist implements Exception {
    String message;

    RemoteLogDoesNotExist([String message = 'Remote log does not exist']) {
        this.message = message;
    }

    @override
    String toString() {
        return message;
    }
}

class RemoteLogCantBeAccessed implements Exception {
    String message;

    RemoteLogCantBeAccessed([String message = 'Remote log can not be accessed']) {
        this.message = message;
    }

    @override
    String toString() {
        return message;
    }
}

class LocalLogDoesNotExist implements Exception {
    String message;

    LocalLogDoesNotExist([String message = 'Local log does not exist']) {
        this.message = message;
    }

    @override
    String toString() {
        return message;
    }
}

class LocalLogIsNull implements Exception {
    String message;

    LocalLogIsNull([String message = 'Local log is null']) {
        this.message = message;
    }

    @override
    String toString() {
        return message;
    }
}

class UidIsNull implements Exception {
    String message;

    UidIsNull([String message = 'Provided Uid is null']) {
        this.message = message;
    }

    @override
    String toString() {
        return message;
    }
}

class LoginException implements Exception {
    final String message;

    const LoginException([this.message = ""]);

    String get decodedMessage {
        switch (message.substring(
            message.indexOf(" ") + 1, message.indexOf(" ") + 10) ??
            '') {
            case "ERROR_INVALID_EMAIL":
                return "Your email address appears to be malformed";
            case "ERROR_WRONG_PASSWORD":
                return "You entered wrong password";
            case "ERROR_USER_NOT_FOUND":
                return "User with this email doesn't exist";
            case "ERROR_USER_DISABLED":
                return "User with this email has been disabled";
            case "ERROR_TOO_MANY_REQUESTS":
                return "Too many requests. Try again later";
            case "ERROR_OPERATION_NOT_ALLOWED":
                return "Signing in with Email and Password is not enabled";
            case "ERROR_OPERATION_NOT_ALLOWED":
                return "Anonymous accounts are not enabled";
            case "ERROR_WEAK_PASSWORD":
                return "Your password is too weak";
            case "ERROR_INVALID_EMAIL":
                return "Your email is invalid";
            case "ERROR_EMAIL_ALREADY_IN_USE":
                return "Email is already in use on different account";
            case "ERROR_INVALID_CREDENTIAL":
                return "Your email is invalid";
            case "There is ":
                return "There is no user record corresponding to this identifier";
            case "The email":
                return "The email address is badly formatted";
            case "Given Str":
                return "Given String is empty or null";
            case "The passw":
                return "The password is invalid or the user does not have a password";
            case "A network":
                return "No internet access";
            default:
                return "An undefined Error happened";
        }
    }

    String toString() =>
        message.substring(message.indexOf(" "), message.indexOf(" ") + 10);
}