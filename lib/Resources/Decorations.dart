import 'package:flutter/material.dart';

class RoundedRectangleDecorator {
  final Color color;
  double bottomLeft;
  double bottomRight;
  double topLeft;
  double topRight;
  RoundedRectangleDecorator(
      {@required this.color, this.bottomLeft = 30, this.bottomRight = 30, this.topLeft = 30, this.topRight = 30});

   getRoundedEdges() {
    return BoxDecoration(
        color: color,
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(bottomLeft),
            bottomRight: Radius.circular(bottomRight),
            topLeft: Radius.circular(topLeft),
            topRight: Radius.circular(topRight)));
  }
}
