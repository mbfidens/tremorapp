class CurrentDate{
  DateTime currentlySelectedDate = DateTime.now();

  void setNew(DateTime newDate){
    currentlySelectedDate = newDate;
  }

  void setToday(){
    currentlySelectedDate = DateTime.now();
  }

  DateTime getDate(){
    return DateTime(currentlySelectedDate.year, currentlySelectedDate.month, currentlySelectedDate.day);
  }
}