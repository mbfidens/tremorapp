import 'package:Tremor_App/Backend/Options/options_manager.dart';
import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/Models/options_model.dart';

class MyInheritedWidget extends InheritedWidget {
  final MyStatefulWidgetState data;

  MyInheritedWidget({
    Key key,
    @required Widget child,
    @required this.data,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}

class MyStatefulWidget extends StatefulWidget {
  final Widget child;

  const MyStatefulWidget({Key key, @required this.child}) : super(key: key);

  static MyStatefulWidgetState of(BuildContext context) {
    if (context.dependOnInheritedWidgetOfExactType<MyInheritedWidget>() !=
        null) {
      return context
          .dependOnInheritedWidgetOfExactType<MyInheritedWidget>()
          .data;
    } else {
      return null;
    }
  }

  @override
  State<StatefulWidget> createState() {
    return MyStatefulWidgetState();
  }
}

class MyStatefulWidgetState extends State<MyStatefulWidget> {
  OptionsManager options;
  String uid;
  int chosenHistoryMenuItem;
  int historyInterval = 7;
  bool newDataAvailable = true;
  bool firstTime100Points = true;
  int optionArrowsUsed = 0;
  bool slideInstructionsVisible = true;

  int getOptionValue(int index) => options.getValue(index);

  int getOptionResult() => options.getResult();

  int getMax() => options.getMax();

  bool isMax(int index) => options.isMax(index);

  bool isMin(int index) => options.isMin(index);

  String getInfoBar(int index) => options.getOptionInfoBar(index);

  String getOptionInfo(int index) => options.getInfo(index);

  int getOptionItemLength(int index) => options.getLengthOneOption(index);

  int getOptionPoints(int index) => options.getPoints(index);

  String getOptionKeyword(int index) => options.getName(index);

  String getOptionIcon(int index) => options.getIcon(index);

  int getOptionLength() => optionsLength;

  int getOptionType(int index) => options.getType(index);

  int getChosenHistoryMenuItem() => chosenHistoryMenuItem;

  int getsHistoryInterval() => historyInterval;

  DateTime getCurrentlySelectedDate() => options.currentDate.getDate();

  void setCurrentlySelectedDate(DateTime dateToSet) {
    saveData();
    OptionsModel newOptionsModel = getSaved.firstWhere(
        (element) => ((element.date.difference(dateToSet).inHours < 24) &&
            (element.date.day - dateToSet.day) == 0),
        orElse: () => OptionsModel(dateToSet, 0, [0, 0, 5, 0, 0, 0, 0]));
    for (int i = 0; i < newOptionsModel.options.length; i++) {
      setOptionValue(newOptionsModel.options[i], i);
    }
    setState(() {
      options.currentDate.setNew(dateToSet);
    });
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  @override
  void initState() {
    options = OptionsManager.empty(refreshOptionsList);
    super.initState();
  }

  void initOptionLogsAfterSignIn() {
    initOptionsAndRefresh(context);
  }

  void refreshOptionsList({bool newDataDownloaded}) {
    setState(() {
      if (newDataDownloaded != null && newDataDownloaded) {
        newDataAvailable = false;
      }
      options.takeDataFromOptionsList();
    });
  }

  void initOptionsAndRefresh(BuildContext context) async {
    options.optionLogs.init(uid).whenComplete(() {
      refreshOptionsList();
    }).catchError((e) {
      debugPrint('MyInheritedWidget.initOptionsAndRefresh() ' + e.toString());
    });
  }

  void setHistoryButtonIndex(int index) {
    setState(() {
      chosenHistoryMenuItem = index;
    });
  }

  void setHistoryInterval(int interval) {
    setState(() {
      historyInterval = interval;
    });
  }

  void incOptionValue(int index) {
    newDataAvailable = true;
    setState(() {
      options.addPoints(index);
    });
  }

  void setOptionValue(int value, index) {
    newDataAvailable = true;
    setState(() {
      options.setValue(value, index);
    });
  }

  void decOptionValue(index) {
    newDataAvailable = true;
    setState(() {
      options.minusPoints(index);
    });
  }

  Future<void> saveData() async {
    if (newDataAvailable) {
      options.updateTodayOptions();
      await options.saveData(uid);
      setState(() {
        newDataAvailable = false;
      });
    }
  }

  void setIdForCloud(String newId) {
    debugPrint('MyInheritedWidget.setIdForCloud() ' + newId);
    options.optionLogs.userId = newId;
    uid = newId;
  }

  Future<void> cleanOptionsListLocally() async {
    setState(() {
      options = OptionsManager.empty(refreshOptionsList);
    });
    try {
      await options.optionLogs.logFile.clean();
      initOptionsAndRefresh(context);
    } catch (e) {
      debugPrint('MyInheritedWidget.cleanOptionsListLocally() ' + e.toString());
    }
  }

  List<OptionsModel> get getSaved {
    List<OptionsModel> savedList = options.optionLogs.getList;

    if (savedList != null && savedList.length != 0) {
      if (options.currentDate.getDate().difference(DateTime.now()).inDays ==
          0) {
        if (savedList.last != null &&
            savedList.last.date.difference(DateTime.now()).inDays == 0) {
          savedList.last = options.todayOptions;
        } else {
          savedList.add(options.todayOptions);
        }
      }
      /*
    Setting time to middle of the day for consistent chart visuals
     */
      savedList = savedList
          .map((element) => OptionsModel(
              DateTime(
                  element.date.year, element.date.month, element.date.day, 12),
              element.result,
              element.options))
          .toList();
    } else {
      savedList = <OptionsModel>[options.todayOptions];
    }
    return savedList;
  }

  @override
  Widget build(BuildContext context) {
    for (int i = 0; i < optionsLength; i++) {
      options.setName(
          AppLocalizations.of(context).translate(options.getOptionKeyword(i)),
          i);
      options.setInfo(
          AppLocalizations.of(context).translate(options.getOptionInfo(i)), i);
    }
    return MyInheritedWidget(
      child: widget.child,
      data: this,
    );
  }
}
