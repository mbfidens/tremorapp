import 'package:flutter/material.dart';

const Color ColorMain = const Color(0xFF2ecc71);
const Color ColorSub1 = const Color(0xFF1abc9c);
const Color ColorSub2 = const Color(0xFF3498db);
const Color ColorSub3 = const Color(0xFFf39c12);
const Color ColorSub4 = const Color(0xFFf1c40f);
const Color ColorSub5 = const Color(0xFFe74c3c);
const Color ColorSub6 = const Color(0xFF9b59b6);
const Color ColorSub7 = const Color(0xFF2980b9);
const Color ColorSub8 = const Color(0xFF16a085);
const Color ColorSub9 = const Color(0xFF34495e);
const Color ColorSub10 = const Color(0xFFecf0f1);