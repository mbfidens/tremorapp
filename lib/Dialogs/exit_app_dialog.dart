import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';

class ExitAppDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01),
              child: Icon(
                Icons.exit_to_app,
                size: MediaQuery.of(context).size.width * 0.09,
                color: ColorMain,
              ),
            ),
            Expanded(
              child: AutoSizeText(
                AppLocalizations.of(context).translate("Exit app"),
                style: TextStyle(
                    color: Colors.black,
                   // fontSize: MediaQuery.of(context).size.width * 0.05,
                    fontWeight: FontWeight.bold),
              ),
            )
          ]),
      content: Wrap(
        children: <Widget>[Container(
          padding: EdgeInsets.only(bottom: 10.0),
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.width * 0.04),
                child: AutoSizeText(
                  AppLocalizations.of(context)
                      .translate("Are you sure to exit app?"),
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: MediaQuery.of(context).size.width * 0.04,
                  ),
                ),
              ),
              Row(
                  children: <Widget>[
                Expanded(
                  child: TextButton(
                    onPressed: () {
                      Navigator.pop(context, 0);
                    },
                    child: Row(

                      children: <Widget>[
                        Container(
                          child: Icon(
                            Icons.cancel,
                            color: ColorMain,
                          ),
                          margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01),
                        ),
                        AutoSizeText(
                          AppLocalizations.of(context).translate("NO"),
                          style: TextStyle(
                              color: Colors.black,
                             // fontSize: MediaQuery.of(context).size.width * 0.04,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: TextButton(
                    onPressed: () {
                      Navigator.pop(context, 1);
                    },
                    child: Row(
                      children: <Widget>[
                        Container(
                          child: Icon(
                            Icons.check_circle,
                            color: ColorMain,
                          ),
                          margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01),
                        ),
                        AutoSizeText(
                          AppLocalizations.of(context).translate("YES"),
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                ),
              ]),
            ],
          ),
        )],
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
    );
  }
}
