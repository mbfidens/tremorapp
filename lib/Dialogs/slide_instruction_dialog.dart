import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:Tremor_App/Widgets/checkbox_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SlideInstructionDialog {
  bool hide = false;

  void show(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => AlertDialog(
                title: getTitle(context),
                content: getContent(context),
                actions: <Widget>[getActions(context)],
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
              ));
    });
  }

  Widget getTitle(BuildContext context) => Container(
      alignment: Alignment.center,
      child: Text(
        'Recommendation',
        style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.05, fontWeight: FontWeight.w500, color: ColorMain),
        textAlign: TextAlign.center,
      ));

  Widget getContent(BuildContext context) => Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            'Instead of pressing arrows, drag the number!',
            textAlign: TextAlign.center,
          ),
          Padding(
            padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.05),
            child: Image.asset('lib/Assets/Images/slide_info.gif'),
          )
        ],
      );

  Widget getActions(BuildContext context) => Container(
      alignment: Alignment.center,
    child: Row(
        children: [
            getCheckbox(context),  Spacer(), getOkButton(context)
        ],
    ),
  );

  Widget getCheckbox(BuildContext context) => Row(
      mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          CheckboxWidget((bool newValue){
              hide = newValue;
          }),
          Text(
            'Don\'t show\nnext session',
            maxLines: 2,
            style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.03, fontWeight: FontWeight.normal),
          )
        ],
      );

  Widget getOkButton(BuildContext context) => TextButton(
        child: Container(
            alignment: Alignment.center,
            height: MediaQuery.of(context).size.width * 0.11,
            width: MediaQuery.of(context).size.width * 0.26,
            decoration: BoxDecoration(color: ColorMain, borderRadius: BorderRadius.all(Radius.circular(30))),
            child: Text(
              AppLocalizations.of(context).translate("OK"),
              style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.width * 0.04),
            )),
        onPressed: () async {
            onOkPress(context);
        },
      );

  Future<void> onOkPress (BuildContext context) async {
      try {
          SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
          if(hide){
              await sharedPrefs.setInt("hideOptionsSliderInstructions", 1);
          }
      } catch(e){
          debugPrint('SlideInstructionsDialog.onOkPress() => Error: ' + e.toString());
      } finally {
          Navigator.pop(context);
      }
  }
}
