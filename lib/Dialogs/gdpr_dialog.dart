import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:Tremor_App/Widgets/flags_row.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GdprDialog extends StatelessWidget {
  final ScrollController controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: getTitle(context),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Wrap(
                runSpacing: 5, // to apply margin in the cross axis of the wrap
                children: <Widget>[
                  getText(context),
                  FlagsRow(),
                  getButtonBar(context),
                ],
              ),
            ),
          ),
        ],
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
    );
  }

  Widget getTitle(BuildContext context) => Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          AppLocalizations.of(context).translate("GDPR aggreement"),
          style: TextStyle(
            color: Colors.black,
            fontSize: MediaQuery.of(context).size.width * 0.05,
            //fontWeight: FontWeight.bold
          ),
        )
      ]);

  Widget getText(BuildContext context) => Container(
    padding: EdgeInsets.only(bottom: 10.0),
    child: Column(
      children: <Widget>[
        Scrollbar(
          isAlwaysShown: false,
          controller: controller,
          child: ConstrainedBox(
            constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height * 0.5),
            child: SingleChildScrollView(
              controller: controller,
              scrollDirection: Axis.vertical, //.horizontal
              child: new Text(
                AppLocalizations.of(context).translate("GDPR text"),
                style: new TextStyle(
                  fontSize: MediaQuery.of(context).size.width * 0.035,
                  color: Colors.black,
                ),
                textAlign: TextAlign.justify,
              ),
            ),
          ),
        ),
      ],
    ),
  );

  Widget getButtonBar(BuildContext context) => Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Flexible(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(30),
                  topRight: Radius.circular(30),
                  bottomLeft: Radius.circular(30),
                  topLeft: Radius.circular(30)),
            ),
            child: TextButton(
              onPressed: () {
                SystemNavigator.pop();
              },
              child: AutoSizeText(
                AppLocalizations.of(context).translate("Disagree"),
                maxLines: 1,
                softWrap: false,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
        Flexible(
          child: Container(
            decoration: BoxDecoration(
              color: ColorMain,
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(30),
                  topRight: Radius.circular(30),
                  bottomLeft: Radius.circular(30),
                  topLeft: Radius.circular(30)),
            ),
            child: TextButton(
              onPressed: () {
                Future<SharedPreferences> sharedPrefs =
                SharedPreferences.getInstance();
                sharedPrefs.then((SharedPreferences prefs) {
                  prefs.setInt("isGdprAgreed", 1);
                });
                Navigator.pop(context, 1);
              },
              child: AutoSizeText(
                AppLocalizations.of(context).translate("Agree"),
                maxLines: 1,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ]);
}
