import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'dart:async';

class LoadingDataDialog extends StatefulWidget {
  final LoadingState state;
  LoadingDataDialog(this.state);

  @override
  _LoadingDataDialogState createState() => _LoadingDataDialogState();
}

class _LoadingDataDialogState extends State<LoadingDataDialog> {
    int passedPeriods = 0;
    static const int periodInMs = 100;
    static const int maxPeriods = 30 * (1000 ~/periodInMs);

    Timer loadingTimer;

  @override
  void initState() {
      super.initState();
     loadingTimer = loadingPeriodTimer(context);
  }

  @override
  void dispose() {
    super.dispose();
    loadingTimer.cancel();
    passedPeriods = 0;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: getHeadline(context),
      content: Wrap(children: <Widget>[
        Container(
            alignment: Alignment.center,
            child: CircularProgressIndicator(
              value: null,
              backgroundColor: ColorMain,
              strokeWidth: 8,
            ))
      ]),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
    );
  }

  Timer loadingPeriodTimer(BuildContext context) =>
    Timer.periodic(Duration(milliseconds: periodInMs), (timer) {
        passedPeriods++;
      if (!widget.state.isLoading || passedPeriods >= maxPeriods) {
        timer.cancel();
        Navigator.pop(context);
      }
    });


  Widget getHeadline(BuildContext context) => Text(
        AppLocalizations.of(context).translate("Please wait..."),
        style: TextStyle(color: ColorMain, fontSize: MediaQuery.of(context).size.width * 0.06, fontWeight: FontWeight.normal),
        textAlign: TextAlign.center,
      );
}

class LoadingState {
  bool isLoading;
  LoadingState(this.isLoading);
}
