import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:Tremor_App/Widgets/fill_form_widget.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:Tremor_App/Resources/AppColors.dart';

class PasswordResetDialog {
  final Function() notifyParent;
  final GlobalKey<FormState> resetEmailFormKey;
  FirebaseAuth firebaseAuth;
  TextEditingController textFieldController = TextEditingController();

  PasswordResetDialog(this.notifyParent, this.firebaseAuth, this.resetEmailFormKey);

  Widget getDialog(BuildContext context) => AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        title: Text(AppLocalizations.of(context).translate("Enter your email"), textAlign: TextAlign.center),
        content: getEmailTextField(context),
        actions: <Widget>[
          getCancelButton(context),
          getConfirmationButton(context),
        ],
      );

  Widget getEmailTextField(BuildContext context) => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Form(
            child: FillFormWidget(
                Icon(
                  Icons.alternate_email,
                  size: MediaQuery.of(context).size.width * 0.09,
                ),
                AppLocalizations.of(context).translate("Email"),
                textFieldController,
                false,
                context),
            key: resetEmailFormKey,
          ),
        ],
      );

  Widget getCancelButton(BuildContext context) => TextButton(
        child: Container(
            alignment: Alignment.center,
            height: MediaQuery.of(context).size.width * 0.11,
            width: MediaQuery.of(context).size.width * 0.26,
            decoration: BoxDecoration(color: Colors.red, borderRadius: BorderRadius.all(Radius.circular(30))),
            child: Text(
              AppLocalizations.of(context).translate("Cancel"),
              style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.width * 0.04),
            )),
        onPressed: () {
          textFieldController.text = '';
          Navigator.pop(context);
          notifyParent();
        },
      );

  Widget getConfirmationButton(BuildContext context) => TextButton(
        child: Container(
            alignment: Alignment.center,
            height: MediaQuery.of(context).size.width * 0.11,
            width: MediaQuery.of(context).size.width * 0.26,
            decoration: BoxDecoration(color: ColorMain, borderRadius: BorderRadius.all(Radius.circular(30))),
            child: Text(
              AppLocalizations.of(context).translate("OK"),
              style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.width * 0.04),
            )),
        onPressed: () async {
          notifyParent();
          final bool isValid = EmailValidator.validate(textFieldController.text);
          if (isValid) {
            notifyParent();
            debugPrint("email good");
            resetPassword(textFieldController.text);
            Navigator.pop(context);
            showCompleted(context);
          } else {
            debugPrint("email bad");
            resetEmailFormKey.currentState.validate();
            notifyParent();
          }
        },
      );

  Future<void> showCompleted (BuildContext context) => showDialog(
      context: context,
      builder: (context) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
          ),
          content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                  Text(AppLocalizations.of(context).translate("Check"), textAlign: TextAlign.center),
                  RichText(
                      text: TextSpan(
                          text: textFieldController.text,
                          style: TextStyle(
                              fontSize: 18,
                              fontStyle: FontStyle.italic,
                              color: Colors.black,
                          ),
                      )),
                  Text(AppLocalizations.of(context).translate("for further instructions"), textAlign: TextAlign.center),
                  Container(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.05),
                    child: TextButton(
                        child: Container(
                            alignment: Alignment.center,
                            height: MediaQuery.of(context).size.width * 0.11,
                            width: MediaQuery.of(context).size.width * 0.26,
                            decoration: BoxDecoration(color: ColorMain, borderRadius: BorderRadius.all(Radius.circular(30))),
                            child: Text(
                                AppLocalizations.of(context).translate("OK"),
                                style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.width * 0.04),
                            )),
                        onPressed: () {
                            textFieldController.text = '';
                            notifyParent();
                            Navigator.pop(context);
                        },
                    ),
                  ),
              ],
          ),
      ),
  );

  Future<void> resetPassword(String email) async {
    await firebaseAuth.sendPasswordResetEmail(email: email);
  }
}
