import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Screens/evaluation_screen.dart';
import 'package:Tremor_App/Resources/AppColors.dart';

class OptionButtonSlideInstructionDialog {
  void show(BuildContext context, String uid) {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => AlertDialog(
                title: getTitle(context),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Wrap(children: <Widget>[EvaluationScreen(uid: uid,)])),
                    ),
                  ],
                ),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
              ));
    });
  }

  Widget getTitle(BuildContext context) => Container(
      alignment: Alignment.center,
      child: Text(
          AppLocalizations.of(context).translate("How did you feel today?"),
          style: TextStyle(
              fontSize: MediaQuery.of(context).size.width * 0.05, fontWeight: FontWeight.w500, color: ColorMain),
          textAlign: TextAlign.center,
      ));
}
