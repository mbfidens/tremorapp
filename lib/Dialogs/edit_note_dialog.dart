import 'package:flutter/material.dart';

class EditNoteDialog{
  final BuildContext context;
  final Widget contentWidget;
  EditNoteDialog(this.context, this.contentWidget);

  BuildContext currentDialogContext;

  void show(){
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showDialog(
          barrierDismissible: true,
          context: context,
          builder: (BuildContext context) {
            currentDialogContext = context;
            return AlertDialog(
              content: contentWidget,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
            );
          });
    });
  }

  BuildContext get dialogContext => currentDialogContext;
}