import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Screens/main_signin_screen.dart';
import 'package:Tremor_App/Backend/User/user_authentication.dart';

class LogInDialog {
  bool isCurrentlyOnLogIn = false;

  LogInDialog();

  show(BuildContext context, UserAuthentication authentication) {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (!isCurrentlyOnLogIn) {
        isCurrentlyOnLogIn = true;
        debugPrint('Detected unsigned user');
        await showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) => AlertDialog(
                  title: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.1,
                      child: Stack(
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: getBackButton(context)),
                          Align(
                            alignment: Alignment.center,
                            child: AutoSizeText(
                              AppLocalizations.of(context)
                                  .translate("Sign in"),

                              style: TextStyle(
                                  fontSize:
                                      MediaQuery.of(context).size.width *
                                          0.08,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                    
                        ],
                      )),
                  content: Container(child: MainSignInScreen(authentication)),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                )).whenComplete(() {
          isCurrentlyOnLogIn = false;
        });
      }
    });
  }

  Widget getBackButton(context) => Container(
               child: IconButton(
          icon: Icon(Icons.arrow_back_sharp),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      );
}
