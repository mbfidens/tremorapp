import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class MeditationGuideWidget extends StatefulWidget {
    final int periodInMs;
    final String firstText;
    final String secondText;
    final Function() onFinished;
    MeditationGuideWidget(this.periodInMs, this.firstText, this.secondText, this.onFinished);

    @override
    _MeditationGuideWidgetState createState() => _MeditationGuideWidgetState();
}

class _MeditationGuideWidgetState extends State<MeditationGuideWidget> with SingleTickerProviderStateMixin {
    AnimationController animationController;
    Animation<double> animateMultiple;

    String currentlyShownText = 'Initial text';
    static const double lowerSizeBoundInPercent = 70;
    static const double higherSizeBoundInPercent = 100;
    AnimationStatus previousStatus = AnimationStatus.forward;

    bool isShownText = false;

    @override
    void initState() {
        super.initState();
        currentlyShownText = widget.firstText;
        Future.delayed(Duration(milliseconds: 16000), (){
           isShownText = true;
        });

        animationController = AnimationController(vsync: this, duration: Duration(milliseconds: widget.periodInMs))
            ..addListener(() {
                setState(() {

                });
            });

        animationController.repeat(reverse: true);
        animateMultiple = Tween<double>(begin: lowerSizeBoundInPercent, end: higherSizeBoundInPercent).animate(animationController);
        animationController.addStatusListener((status) {
            if(status != previousStatus){
                currentlyShownText = currentlyShownText == widget.firstText ? widget.secondText : widget.firstText;
            }
            previousStatus = status;
        });
    }

    @override
    void dispose() {
        animationController.dispose();
        super.dispose();
    }

    @override
    Widget build(BuildContext context) {
        return Container(
            color: Colors.transparent,
            child: Stack(
                alignment: Alignment.center,
                children: <Widget>[getBackground(), getText(), getClipped()],
            ),
        );
    }

    Widget getBackground() => Transform.rotate(
        angle: 2 * math.pi * ((getSizeMultiple() - 0.8) / 0.2),
        child: Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width * 0.6 * getSizeMultiple(),
            height: MediaQuery.of(context).size.width * 0.6 * getSizeMultiple(),
            decoration: getCircleDecoration(color: Colors.white),
            child: Container(
                width: MediaQuery.of(context).size.width * 0.59 * getSizeMultiple(),
                height: MediaQuery.of(context).size.width * 0.56 * getSizeMultiple(),
                decoration: getCircleDecoration(color: Colors.white),
            ),
        ),
    );

    //(x-0.5)^2 = - 4 * 0.062 * (y - 0.998)
    //y = 0.998 - 0.248 * (x-0.5)^2
    Widget getText() => Container(
        child: Text(
            isShownText? currentlyShownText : '',
            style: TextStyle(
                color: ColorMain.withOpacity(0.998 - 3.6 * (math.pow((getSizeProportion() - 0.5), 2))),
                fontWeight: FontWeight.w300,
                fontSize: MediaQuery.of(context).size.width * 0.07,
            ),
        ),
    );

    Decoration getCircleDecoration({Color color = ColorMain}) => BoxDecoration(
        color: color,
        borderRadius: BorderRadius.all(Radius.circular(180)),
    );

    //minimum percentage to 1
    double getSizeMultiple() => animateMultiple.value / higherSizeBoundInPercent;

    //0 to 1
    double getSizeProportion() =>
        ((getSizeMultiple() - lowerSizeBoundInPercent / 100) / ((higherSizeBoundInPercent - lowerSizeBoundInPercent) / 100));

    Widget getClipped() => Container(
        alignment: Alignment.center,
        child: Stack(children: <Widget>[
            Padding(
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.06),
                child: getCircle(),
            ),
            Padding(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.06),
                child: getCircle(),
            ),
            Padding(
                padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.width * 0.06),
                child: getCircle(),
            ),
            Padding(
                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.04),
                child: getCircle(),
            ),
            Padding(
                padding:
                EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.width * 0.04),
                child: getCircle(),
            ),
            Padding(
                padding:
                EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.width * 0.04),
                child: getCircle(),
            ),
            Padding(
                padding:
                EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, bottom: MediaQuery.of(context).size.width * 0.04),
                child: getCircle(),
            )
        ]),
    );

    Widget getCircle() => Transform.rotate(
        angle: 2 * math.pi * ((getSizeMultiple() - 0.8) / 0.4),
        child: ClipPath(
            clipper: MyCustomClipper(),
            child: Container(
                width: MediaQuery.of(context).size.width * 0.65 * getSizeMultiple(),
                height: MediaQuery.of(context).size.width * 0.65 * getSizeMultiple(),
                color: ColorMain,
            ),
        ),
    );
}

class MyCustomClipper extends CustomClipper<Path> {
    @override
    Path getClip(Size size) {
        Path path = Path()
            ..addOval(Rect.fromCircle(center: Offset(size.width / 2, size.height / 2), radius: size.width / 2.05))
        //..addOval(Rect.fromCircle(center: Offset(size.width / 2, size.height / 2), radius: size.width / 2))
            ..addOval(Rect.fromLTWH(
                size.width / 2 - size.width / 1.05 / 2, size.width / 2 - size.width / 1.09 / 2, size.width / 1.05, size.width / 1.09))
            ..fillType = PathFillType.evenOdd
            ..close();
        return path;
    }

    @override
    bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
