import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';

class EvaluationSliderWidget extends StatefulWidget {
  @override
  _EvaluationSliderWidgetState createState() => _EvaluationSliderWidgetState();
}

class _EvaluationSliderWidgetState extends State<EvaluationSliderWidget> {
  double _value = 50;

  String getFeelDescriptorString(){
    switch(_value.round()){
      case 0:
        return 'Horrible!';
      case 10:
        return 'Very bad!';
      case 20:
        return 'Very bad';
      case 30:
        return 'Bad';
      case 40:
        return 'Slightly bad';
      case 50:
        return 'Moderate';
      case 60:
        return 'Slightly good';
      case 70:
        return 'Good';
      case 80:
        return 'Very good';
      case 90:
        return 'Awesome';
      case 100:
        return 'Perfect!';
      default:
        return _value.toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(child:
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text('  0%'),
        SliderTheme(
        data: SliderTheme.of(context).copyWith(
          activeTrackColor: ColorMain,
          inactiveTrackColor: Colors.white,
          thumbColor: ColorMain,
          overlayColor: Colors.green[100],
          activeTickMarkColor: ColorMain,
          inactiveTickMarkColor: Colors.black54,
          valueIndicatorColor: ColorMain,
          trackShape: RoundedRectSliderTrackShape(),
          trackHeight: 4.0,
          thumbShape: CustomSliderThumbCircle(thumbRadius: 13, max: 100, min: 0),
          overlayShape: RoundSliderOverlayShape(overlayRadius: 18.0),
          tickMarkShape: RoundSliderTickMarkShape(),
          valueIndicatorShape: PaddleSliderValueIndicatorShape(),
          valueIndicatorTextStyle: TextStyle(
            color: Colors.white,
          ),
        ),
        child: Slider(
          value: _value,
          min: 0,
          max: 100,
          divisions: 10,
          label: getFeelDescriptorString(),
          onChanged: (value) {
            setState(
                  () {
                _value = value;
              },
            );
          },
        ),
      ),
        Text('100%'),],
    ),);
  }
}

class CustomSliderThumbCircle extends SliderComponentShape {
  final double thumbRadius;
  final int min;
  final int max;

  const CustomSliderThumbCircle({
    @required this.thumbRadius,
    this.min = 0,
    this.max = 10,
  });

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size.fromRadius(thumbRadius);
  }

  @override
  void paint(
      PaintingContext context,
      Offset center, {
        Animation<double> activationAnimation,
        Animation<double> enableAnimation,
        bool isDiscrete,
        TextPainter labelPainter,
        RenderBox parentBox,
        SliderThemeData sliderTheme,
        TextDirection textDirection,
        double value,
        double textScaleFactor,
        Size sizeWithOverflow,
      }) {
    final Canvas canvas = context.canvas;

    final paint = Paint()
      ..color = ColorMain
      ..style = PaintingStyle.fill;

    TextSpan span = new TextSpan(
      style: new TextStyle(
        fontSize: thumbRadius * .8,
        fontWeight: FontWeight.w700,
        color: Colors.white,
      ),
      text: getValue(value),
    );

    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr);
    tp.layout();
    Offset textCenter =
    Offset(center.dx - (tp.width / 2), center.dy - (tp.height / 2));

    canvas.drawCircle(center, thumbRadius * .9, paint);
    tp.paint(canvas, textCenter);
  }

  String getValue(double value) {
    return ((max * value).round()).toString();
  }
}