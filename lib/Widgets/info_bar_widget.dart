import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:Tremor_App/Resources/AppColors.dart';

class InfoBarWidget extends StatelessWidget {
  final int index;

  InfoBarWidget(this.index);

  @override
  Widget build(BuildContext context) {
    MyStatefulWidgetState state = MyStatefulWidget.of(context);
    return Container(
      alignment: Alignment.bottomLeft,
      height: 95,
      child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            state.getOptionType(index) != 2
                ? Row(
                children: List.generate(
                    state.getOptionItemLength(index),
                        (i) =>
                        Icon(Icons.favorite,
                            size: MediaQuery
                                .of(context)
                                .size
                                .width * 0.06,
                            color: state.getOptionPoints(index) -
                                i *
                                    state.getMax() /
                                    state.getOptionItemLength(index) >
                                0
                                ? ColorMain
                                : state.getOptionType(index) == 1
                                ? Colors.grey.shade300
                                : Colors.redAccent)))
                : Container(
              height: 20,
              child: AutoSizeText(state.getInfoBar(index),
                  style: TextStyle(
                    fontSize: MediaQuery
                        .of(context)
                        .size
                        .width * 0.045,
                  )),
            ),
            Expanded(child: Container()),
            Padding(
              padding: EdgeInsets.only(
                  right: MediaQuery
                      .of(context)
                      .size
                      .width * 0.05),
              child: Row(
                children: <Widget>[
                  Container(
                    child: AutoSizeText(
                      state.getOptionInfo(index),
                      maxLines: 1,
                    ),
                  ),
                  Icon(Icons.favorite,
                      size: MediaQuery
                          .of(context)
                          .size
                          .width * 0.06,
                      color: ColorMain)
                ],
              ),
            )
          ]),
      margin: EdgeInsets.only(left: 20),
    );
  }
}
