import 'package:flutter/material.dart';

class WarningWidget extends StatelessWidget {
  final String message;
  WarningWidget(this.message);

  @override
  Widget build(BuildContext context) {
    return message == '' ? emptyWidget() : warning(context);
  }

  Widget emptyWidget() => Container();

  Widget warning(BuildContext context) => Container(
        alignment: Alignment.center,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              Icons.error,
              color: Colors.redAccent,
              size: MediaQuery.of(context).size.width * 0.07,
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.01,
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.5,
              child: Text(
                message,
                style: TextStyle(color: Colors.redAccent, fontSize: MediaQuery.of(context).size.width * 0.04),
              ),
            ),
          ],
        ),
      );
}
