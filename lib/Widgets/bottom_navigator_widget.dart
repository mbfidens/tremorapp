import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:Tremor_App/Resources/AppColors.dart';

class BottomNavigatorWidget extends StatelessWidget {
  int selectedIndex;
  Function(int index) onTapped;

  BottomNavigatorWidget(this.selectedIndex, this.onTapped);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      backgroundColor: Colors.white,
      type: BottomNavigationBarType.fixed,
      items: getBottomNavigationBarItems(context),
      currentIndex: selectedIndex,
      onTap: onTapped,
      selectedItemColor:
          selectedIndex != 2 ? ColorMain : const Color(0xff81e5cd),
      elevation: 8,
    );
  }

  List<BottomNavigationBarItem> getBottomNavigationBarItems(
          BuildContext context) =>
      <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: const Icon(Icons.home),
          label: AppLocalizations.of(context).translate('Home'),
        ),
        BottomNavigationBarItem(
          icon: const Icon(Icons.local_florist),
          label: AppLocalizations.of(context).translate('Relaxation'),
        ),
        BottomNavigationBarItem(
          icon: const Icon(Icons.calendar_today),
          label: AppLocalizations.of(context).translate('History'),
        ),
        BottomNavigationBarItem(
          icon: const Icon(Icons.adjust),
          label: AppLocalizations.of(context).translate('Devices'),
        )
      ];
}
