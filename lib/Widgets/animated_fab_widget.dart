import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';

class AnimatedFabWidget extends StatefulWidget {
  final IconData buttonIcon;
  final Function() onPressed;
  final Color backgroundColor;
  String hero;
  AnimatedFabWidget(this.buttonIcon, this.onPressed, {this.hero, this.backgroundColor = ColorMain});

  @override
  _AnimatedFabWidgetState createState() => _AnimatedFabWidgetState();
}

class _AnimatedFabWidgetState extends State<AnimatedFabWidget> with SingleTickerProviderStateMixin {
  bool isOpened = false;
  AnimationController _animationController;
  Animation<double> _animateIcon;

  @override
  initState() {
    _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 500))
      ..addListener(() {
        setState(() {});
      });
    _animationController.repeat(reverse: true);
    _animateIcon = Tween<double>(begin: 80, end: 100).animate(_animationController);
    super.initState();
  }

  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return toggleButton();
  }

  Widget toggleButton() {
    return FloatingActionButton(
      heroTag: widget.hero,
      backgroundColor: widget.backgroundColor,
      onPressed: onButtonPress,
      tooltip: 'Toggle',
      child: Container(
        height: MediaQuery.of(context).size.width * 0.06,
        width: MediaQuery.of(context).size.width * 0.06,
        alignment: Alignment.center,
        child: Icon(
          widget.buttonIcon,
          color: Colors.white,
          size: MediaQuery.of(context).size.width * 0.06 * _animateIcon.value / 100,
        ),
      ),
    );
  }

  onButtonPress(){
    animate();
    widget.onPressed();
  }

  animate() {
    if (!isOpened) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
    isOpened = !isOpened;
  }
}
