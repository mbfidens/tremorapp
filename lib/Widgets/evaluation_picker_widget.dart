import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class EvaluationPickerWidget extends StatefulWidget {
  final Function(double rating) notifyParent;
  final double initialValue;

  EvaluationPickerWidget(this.notifyParent, this.initialValue);

  @override
  _EvaluationPickerWidgetState createState() => _EvaluationPickerWidgetState();
}

class _EvaluationPickerWidgetState extends State<EvaluationPickerWidget> {
  double value = 3;
  double minimum = 0;

  String getFeelEmojiAddress() {
    switch (value.round()) {
      case 0:
        return 'lib/Assets/Images/smile0.png';
      case 1:
        return 'lib/Assets/Images/smile1.png';
      case 2:
        return 'lib/Assets/Images/smile2.png';
      case 3:
        return 'lib/Assets/Images/smile3.png';
      case 4:
        return 'lib/Assets/Images/smile4.png';
      case 5:
        return 'lib/Assets/Images/smile5.png';
      default:
        return 'lib/Assets/Images/smile3.png';
    }
  }

  @override
  Widget build(BuildContext context) {
    value = widget.initialValue;
    return Stack(children: <Widget>[
      Container(
        alignment: Alignment.topRight,
        child: getEmoji(),
      ),
      Container(
          padding: EdgeInsets.only(top: 5),
        height: 70,
        alignment: Alignment.topLeft,
        child: getRatingBar(context),
      )
    ]);
  }

  Widget getRatingBar(BuildContext context) => Container(
        width: MediaQuery.of(context).size.width * 0.6,
        child: RatingBar(
            itemSize: MediaQuery.of(context).size.width * 0.1 < 50 ? (MediaQuery.of(context).size.width * 0.1) : 50,
            minRating: minimum,
            glowColor: Color.fromRGBO(237, 129, 142, 1),
            updateOnDrag: true,
            initialRating: widget.initialValue,
            unratedColor: Color.fromRGBO(237, 129, 142, 1),
            allowHalfRating: false,
            wrapAlignment: WrapAlignment.start,
            itemPadding: EdgeInsets.all(1),
            ratingWidget: RatingWidget(
              full: getFullHeart(),
              half: getEmptyHeart(Ionicons.ios_heart_half),
              empty: getEmptyHeart(Ionicons.md_heart_empty),
            ),
            onRatingUpdate: (double newValue) {
              minimum = 1;
              onRatingUpdate(newValue);
            }),
      );

  void onRatingUpdate(double newRating) {
    updateRating(context, newRating);
  }

  void updateRating(BuildContext context, double newRating) {
    MyStatefulWidgetState state = MyStatefulWidget.of(context);
    state.setOptionValue(newRating.round(), state.getOptionLength() - 1);
    setState(() {
      value = newRating;
      widget.notifyParent(value);
    });
  }

  Widget getFullHeart() => Container(
        alignment: Alignment.center,
        child: Image.asset(
          'lib/Assets/Images/heart_short.png',
        ),
      );

  Widget getEmptyHeart(IconData iconData) => Opacity(
        opacity: 0.4,
        child: getFullHeart(),
      );

  Widget getEmoji() => IconButton(
    onPressed: (){},
        icon: Image.asset(getFeelEmojiAddress()),
      );
}
