import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'dart:async';

class IntroTextWidget extends StatefulWidget {
  final Function() onFinished;
  IntroTextWidget(this.onFinished);

  @override
  _IntroTextWidgetState createState() => _IntroTextWidgetState();
}

class _IntroTextWidgetState extends State<IntroTextWidget> {
  static const List<String> introScript = <String>[
    "Lets relax",
    "Find a comfortable position",
    "It will only take two minutes",
    "So lets begin",
    ''
  ];
  int textDurationInMillis = 4000;

  double opacity = 0;
  int periodPart = 0;
  //One text line is divided in 5 parts - 20% fade in - 60% hold - 20% fade out
  int fadingCyclePartIndex = 0;
  int introScriptIndex = 0;
  Timer currentTimer;

  @override
  void initState() {
    super.initState();
    onNewPeriod();
    Timer.periodic(Duration(milliseconds: textDurationInMillis), (timer) {
      onNewPeriod(timer: timer);
    });
  }

  @override
  void deactivate() {
    super.deactivate();
    currentTimer.cancel();
  }

  void onNewPeriod({Timer timer}) {
    if (mounted) {
      setState(() {
        debugPrint('Intro script: ' + introScriptIndex.toString() + ' ' +
            introScript.elementAt(introScriptIndex));
        if (introScriptIndex == introScript.length - 1) {
          currentTimer.cancel();
          if (timer != null) {
            timer.cancel();
          }
          widget.onFinished();
        } else {
          fadingCyclePartIndex = 0;
          updateFadingCycleData();
          resetFadingCycle();
          introScriptIndex++;
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      duration: Duration(milliseconds: textDurationInMillis ~/ 5),
      opacity: opacity,
      child: Text(AppLocalizations.of(context).translate(introScript[introScriptIndex - 1]) ?? '',
          textAlign: TextAlign.center,
          style: TextStyle(color: ColorMain, fontSize: MediaQuery.of(context).size.width * 0.08, fontWeight: FontWeight.w300)),
    );
  }

  void resetFadingCycle() {
    if (currentTimer != null) {
      currentTimer.cancel();
    }
    currentTimer = Timer.periodic(Duration(milliseconds: textDurationInMillis ~/ 5.1), (timer) {
      updateFadingCycleData();
    });
  }

  void updateFadingCycleData() {
    setState(() {
      debugPrint(fadingCyclePartIndex.toString());
      if (fadingCyclePartIndex < 4) {
        opacity = 1;
      } else {
        opacity = 0;
      }
      fadingCyclePartIndex++;
    });
  }
}
