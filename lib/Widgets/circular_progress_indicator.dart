import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:Tremor_App/Widgets/award_widget.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'dart:math' as math;
import 'package:flutter/rendering.dart';

class CustomCircularProgressIndicator extends StatefulWidget {
  @override
  _CustomCircularProgressIndicatorState createState() => _CustomCircularProgressIndicatorState();
}

class _CustomCircularProgressIndicatorState extends State<CustomCircularProgressIndicator> {
  final int progress = 20;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width * 0.3,
        height: MediaQuery.of(context).size.width * 0.3,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black38,
              offset: Offset(0, 1),
              blurRadius: 9.0,
            ),
          ],
        ),
        padding: EdgeInsets.all(11.0),
        child: Stack(
          children: <Widget>[getPaint(), Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.center,
            child: RawMaterialButton(
              onPressed: (){
                //TODO(Edvinas): implement some action for the circular button
              },
              splashColor: Colors.transparent,
              child: Container(
                height: MediaQuery.of(context).size.width * 0.5,
                width: MediaQuery.of(context).size.width * 0.5,
              ),
            ),
          )],
        ));
  }

  Widget getPaint() => CustomPaint(
        foregroundPainter: new MyPainter(
          completeColor: ColorMain,
          completePercent: MyStatefulWidget.of(context).getOptionResult().toDouble(),
          width: 9.0,
        ),
        child: Center(
          child: Container(
            margin: EdgeInsets.all(17),
            child: FittedBox(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: getAndCheckResultText(context),
                          style: TextStyle(
                            color: ColorMain,
                            fontSize: MediaQuery.of(context).size.height * 0.08,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(
                          text: "%",
                          style: TextStyle(
                            color: ColorMain,
                            fontSize: 31,
                          ),
                        ),
                      ],
                    ),
                  ),
                  AutoSizeText(
                    getLabelText(context),
                    textScaleFactor: 0.9,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: ColorMain,
                      //  fontSize: MediaQuery.of(context).size.width * 0.035
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );

  String getLabelText(BuildContext context) {
    int result = MyStatefulWidget.of(context).getOptionResult();
    if (result >= 100) {
      return AppLocalizations.of(context).translate("PERFECT!");
    } else if (result >= 85) {
      return AppLocalizations.of(context).translate("GOOD");
    } else {
      return AppLocalizations.of(context).translate("NEED EFFORT");
    }
  }

  String getAndCheckResultText(BuildContext context) {
    int result = MyStatefulWidget.of(context).getOptionResult();
    if (result >= 100 && MyStatefulWidget.of(context).firstTime100Points && MyStatefulWidget.of(context).newDataAvailable) {
      MyStatefulWidget.of(context).firstTime100Points = false;
      showAwardDialog();
    }
    return result.toString();
  }

  void showAwardDialog() async {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            //backgroundColor: Colors.transparent,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(60.0)),
            content: RawMaterialButton(
              child: AwardWidget(),
              onPressed: onAwardDiscard,
            ),
          );
        },
      );
    });
  }

  void onAwardDiscard() {
    Navigator.of(context).pop();
  }
}

class MyPainter extends CustomPainter {
  Color lineColor;
  Color completeColor;
  double completePercent;
  double width;

  MyPainter({
    this.lineColor,
    this.completeColor,
    this.completePercent,
    this.width,
  });

  @override
  void paint(Canvas canvas, Size size) {
    Paint complete = new Paint()
      ..color = completeColor
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = width;
    Offset center = new Offset(size.width / 2, size.height / 2);
    double radius = math.min(size.width / 2, size.height / 2);
    double arcAngle = 2 * math.pi * (completePercent / 100);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), math.pi / 2, arcAngle, false, complete);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
