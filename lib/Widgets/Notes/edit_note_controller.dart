import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/Models/notes_model.dart';
import 'package:Tremor_App/Screens/edit_note_screen.dart';
import 'package:Tremor_App/Dialogs/edit_note_dialog.dart';

class EditNoteController{
  EditNoteDialog editNoteDialog;

  void getEditNoteDialog(String uid, BuildContext context, NotesModel noteModelToEdit,  Function({NotesModel updatedNote}) callback) {
    editNoteDialog =
        EditNoteDialog(context, Wrap(children: <Widget>[EditNoteScreen(note: noteModelToEdit, notifyParent: callback, uid: uid,)]));
    editNoteDialog.show();
  }

  void closeDialog() {
    debugPrint('Close dialog of context: ' + editNoteDialog.dialogContext.toString());
    if (editNoteDialog.dialogContext != null) {
      debugPrint('Initiating dialog pop');
      Navigator.of(editNoteDialog.dialogContext).pop();
      editNoteDialog = null;
    }
  }
}