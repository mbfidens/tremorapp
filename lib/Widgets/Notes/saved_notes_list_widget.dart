import 'dart:async';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:Tremor_App/Backend/Logs/note_logs.dart';
import 'package:Tremor_App/Screens/edit_note_screen.dart';
import 'package:Tremor_App/Backend/Models/notes_model.dart';
import 'package:Tremor_App/Widgets/Notes/saved_notes_list_item_widget.dart';
import 'package:Tremor_App/Widgets/Notes/add_note_widget.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

class SavedNotesListWidget extends StatefulWidget {

  final String uid;

  SavedNotesListWidget({Key key, @required this.uid}) : super(key: key);
  @override
  _SavedNotesListWidgetState createState() => _SavedNotesListWidgetState();
}

class _SavedNotesListWidgetState extends State<SavedNotesListWidget> {
  List<Widget> notesList = <Widget>[];
  int editedNoteIndex;
  NoteLogs logs = NoteLogs();
  bool newNoteCreationInProgress = false;

  @override
  void initState() {
    super.initState();
    logs.dataInit(context, widget.uid).whenComplete(() {
      updateState();
      Future.delayed(Duration(seconds: 3), () {
        updateState();
      });
    });
  }

  /*@override
  void dispose() {
    updateState();
    super.dispose();
  }*/

  @override
  Widget build(BuildContext context) {
    return newNoteCreationInProgress
        ? KeyboardVisibilityBuilder(builder: (context, isKeyboardVisible) {
          return Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.center,
            child: Container(
              width: MediaQuery.of(context).size.width * 0.8,
              height: MediaQuery.of(context).size.height * 0.8,
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  isKeyboardVisible ? Container(height: MediaQuery.of(context).size.width * 0.08,) : Spacer(),
                  EditNoteScreen(
                    uid: widget.uid,
                    notifyParent: ({NotesModel updatedNote}) {
                      onNoteEditCallback(
                        editedNote: updatedNote,
                      );
                    },
                    note: NotesModel.empty(),
                    resizeOnKeyboard: false,
                  ),
                  Spacer(),],
              ),
            ),
          ),
          getBackButton()
        ],
      );
    })
        : Column(
            children: <Widget>[getHeading()] + notesList,
          );
  }

  void updateNotesList() {
    notesList.clear();
    notesList.add(getNewNoteCreatorButton());
    notesList = notesList + List<Widget>.generate(logs.getLogs().length, (index) => getSavedItemsWidget(index));
  }

  Widget getHeading() => Container(
    height: MediaQuery.of(context).size.height * 0.05,
  );

  Widget getSavedItemsWidget(int index) => Padding(
      padding: getItemPaddingInsets(),
      child: SavedNotesListItemWidget( logs.getLogs()[index],  ({NotesModel editedNote}) {
        editedNoteIndex = index;
        debugPrint('yyyyyyyyyyyyyyyyyyyyyyyyy');
      //  NoteLogs().logFirebase(logs.readLogs().toString());
        onNoteEditCallback(editedNote: editedNote);
      }, widget.uid));

  Widget getNewNoteCreatorButton() => Padding(
        padding: getItemPaddingInsets(),
        child: AddNoteWidget(({NotesModel editedNote}) {
          newNoteCreationInProgress = true;
          onNoteEditCallback(editedNote: editedNote);
        }),
      );

  EdgeInsets getItemPaddingInsets() => EdgeInsets.symmetric(
    vertical: MediaQuery.of(context).size.width * 0.025,
      horizontal: MediaQuery.of(context).size.width * 0.05);

  void onNoteEditCallback({NotesModel editedNote}) {
    if (editedNote != null) {
      if (editedNote.isEmpty) {
        if (newNoteCreationInProgress) {
          newNoteCreationInProgress = false;
        } else {
          logs.removeLog(editedNoteIndex,widget.uid);
        }
      } else {
        if (newNoteCreationInProgress) {
          logs.addLog(editedNote,widget.uid);
          newNoteCreationInProgress = false;
        } else {
          logs.updateLog(editedNoteIndex, editedNote,widget.uid);
        }
      }
    } else {}
    updateState();
  }

  void updateState() {
    setState(() {
      updateNotesList();
    });
  }

  Widget getBackButton() => Container(
        alignment: Alignment.centerLeft,
        color: Colors.transparent,
        child: RawMaterialButton(
            onPressed: () {
              setState(() {
                newNoteCreationInProgress = false;
              });
            },
            child: Container(
              decoration: BoxDecoration(
                  color: ColorMain,
                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(30), topRight: Radius.circular(30))),
              child: Padding(
                padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.03),
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                        //size: MediaQuery.of(context).size.width * 0.06,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.02),
                      child: Text(
                        AppLocalizations.of(context).translate("Notes"),
                        style: TextStyle(
                            color: Colors.white,
                            //fontSize: MediaQuery.of(context).size.height * 0.02,
                            fontWeight: FontWeight.w600),
                      ),
                    )
                  ],
                ),
              ),
            )),
      );
}
