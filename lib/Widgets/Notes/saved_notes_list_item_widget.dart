import 'package:flutter/material.dart';
import 'package:Tremor_App/Widgets/filled_note_widget.dart';
import 'package:Tremor_App/Backend/Models/notes_model.dart';
import 'package:Tremor_App/Widgets/Notes/edit_note_controller.dart';

// ignore: must_be_immutable
class SavedNotesListItemWidget extends StatefulWidget {
  NotesModel initialModelData;
  final String uid;
  final Function({NotesModel editedNote}) notifyParent;
  SavedNotesListItemWidget(this.initialModelData, this.notifyParent, this.uid);

  @override
  _SavedNotesListItemWidgetState createState() => _SavedNotesListItemWidgetState();
}

class _SavedNotesListItemWidgetState extends State<SavedNotesListItemWidget> {
  EditNoteController editNoteController = EditNoteController();
  bool dialogIsClosing = false;

  @override
  Widget build(BuildContext context) {
    return FilledNoteWidget(note: widget.initialModelData, notifyParent: onEditedModel);
  }

  void onEditedModel({NotesModel updatedNote}) {
    if (updatedNote != null) {
      setState(() {
        widget.initialModelData = updatedNote.isEmpty ? NotesModel.empty() : updatedNote;
        editNoteController.closeDialog();
        dialogIsClosing = true;
        widget.notifyParent(editedNote: updatedNote);
      });
    } else
      if(dialogIsClosing) {
        dialogIsClosing = false;
      } else{
        editNoteController.getEditNoteDialog(widget.uid, context, widget.initialModelData, onEditedModel);
    }
  }
}