import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class AddNoteWidget extends StatefulWidget {
  final Function() onEditButtonPress;
  AddNoteWidget(this.onEditButtonPress);

  @override
  _AddNoteWidgetState createState() => _AddNoteWidgetState();
}

class _AddNoteWidgetState extends State<AddNoteWidget> {

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.13,
        decoration: BoxDecoration(
          border: Border.all(color: Theme.of(context).primaryColor, width: 2),
          borderRadius: BorderRadius.circular(16),
        ),
        child: Material(
          borderRadius: BorderRadius.circular(16),
          clipBehavior: Clip.antiAlias,
          child: RawMaterialButton(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.add,
                        color: Theme.of(context).primaryColor,
                      ),
                      AutoSizeText(
                        AppLocalizations.of(context).translate('Add new note'),
                        style: TextStyle(
                            fontFamily: 'ZillaSlab',
                            color: Theme.of(context).primaryColor,
                           // fontSize: MediaQuery.of(context).size.width * 0.05
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
            onPressed: (){
              setState(() {
                widget.onEditButtonPress();
              });
            },
          ),
        ));
  }
}
