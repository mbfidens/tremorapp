import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:flutter/physics.dart';
import 'package:Tremor_App/Dialogs/slide_instruction_dialog.dart';

class StepperButtonWidget extends StatefulWidget {
  const StepperButtonWidget({
    Key key,
    this.optionIndex,
    this.initialValue,
    this.onChanged,
    this.direction = Axis.horizontal,
    this.withSpring = true,
  }) : super(key: key);

  final int optionIndex;

  /// the orientation of the stepper its horizontal or vertical.
  final Axis direction;

  /// the initial value of the stepper
  final int initialValue;

  /// called whenever the value of the stepper changed
  final ValueChanged<int> onChanged;

  /// if you want a springSimulation to happens the the user let go the stepper
  /// defaults to true
  final bool withSpring;

  // Coded By Raj Chowdhury

  @override
  _Stepper2State createState() => _Stepper2State();
}

class _Stepper2State extends State<StepperButtonWidget> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  double _startAnimationPosX;
  double _startAnimationPosY;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this, lowerBound: -0.25, upperBound: 0.25);
    _controller.value = 0.0;
    _controller.addListener(() {});

    if (widget.direction == Axis.horizontal) {
      _animation = Tween<Offset>(begin: Offset(0.0, 0.0), end: Offset(1.5, 0.0)).animate(_controller);
    } else {
      _animation = Tween<Offset>(begin: Offset(0.0, 0.0), end: Offset(0.0, 1.5)).animate(_controller);
    }
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.direction == Axis.horizontal) {
      _animation = Tween<Offset>(begin: Offset(0.0, 0.0), end: Offset(1.5, 0.0)).animate(_controller);
    } else {
      _animation = Tween<Offset>(begin: Offset(0.0, 0.0), end: Offset(0.0, 1.5)).animate(_controller);
    }
  }

  @override
  Widget build(BuildContext context) {
    MyStatefulWidgetState state = MyStatefulWidget.of(context);
    return FittedBox(
      child: Container(
        child: Material(
          type: MaterialType.canvas,
          clipBehavior: Clip.antiAlias,
          borderRadius: BorderRadius.circular(20.0),
          color: Colors.white,
          child: Stack(children: <Widget>[
            Container(
                height: 80,
                width: 180,
                padding: EdgeInsets.only(left: 15),
                child: Row(
                  children: <Widget>[
                    Container(
                        width: 40,
                        child: state.isMin(widget.optionIndex)
                            ? Container(
                                width: 40,
                              )
                            : Center(
                                child: RawMaterialButton(
                                    child: Icon(Icons.arrow_back_sharp, size: 50.0, color: ColorMain.withOpacity(0.5)),
                                    onPressed: () {
                                      setState(() {
                                        if (widget.optionIndex != null) {
                                          state.decOptionValue(widget.optionIndex);
                                        }
                                      });
                                    }))),
                    Container(
                      width: 80,
                    ),
                    Container(
                      width: 40,
                      alignment: Alignment.topCenter,
                      child: state.isMax(widget.optionIndex)
                          ? Center(
                              child: Container(
                                width: 40,
                                height: 40,
                                alignment: Alignment.centerLeft,
                                child: Icon(
                                  Icons.done,
                                  size: 40,
                                  color: Colors.transparent,
                                ),
                              ),
                            )
                          : Center(
                              child: RawMaterialButton(
                                  child: Icon(Icons.arrow_forward_sharp, size: 50.0, color: ColorMain.withOpacity(0.5)),
                                  onPressed: () {
                                    setState(() {
                                      if (widget.optionIndex != null) {
                                        state.incOptionValue(widget.optionIndex);
                                        showInstructionsIfNeeded(context);
                                      }
                                    });
                                  }),
                            ),
                    ),
                  ],
                )),
            Row(
              children: <Widget>[
                Container(
                  width: 60,
                ),
                Container(
                    height: 80,
                    width: 80,
                    child: GestureDetector(
                      onHorizontalDragStart: _onPanStart,
                      onHorizontalDragUpdate: _onPanUpdate,
                      onHorizontalDragEnd: _onPanEnd,
                      child: SlideTransition(
                        position: _animation,
                        child: Container(
                            width: 40,
                            child: Material(
                              borderRadius: BorderRadius.circular(30.0),
                              color: Colors.white,
                              child: AnimatedSwitcher(
                                duration: const Duration(milliseconds: 300),
                                transitionBuilder: (Widget child, Animation<double> animation) {
                                  return ScaleTransition(child: child, scale: animation);
                                },
                                child: AutoSizeText(
                                  state.getOptionValue(widget.optionIndex).toString(),
                                  key: ValueKey<int>(state.getOptionValue(widget.optionIndex)),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: ColorMain,
                                      fontSize: MediaQuery.of(context).size.width * 0.14,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            )),
                      ),
                    ))
              ],
            ),
          ]),
        ),
      ),
    );
  }

  double offsetFromGlobalPos(Offset globalPosition) {
    RenderBox box = context.findRenderObject() as RenderBox;
    Offset local = box.globalToLocal(globalPosition);
    _startAnimationPosX = ((local.dx * 0.75) / box.size.width) - 0.4;
    _startAnimationPosY = ((local.dy * 0.75) / box.size.height) - 0.4;
    if (widget.direction == Axis.horizontal) {
      return ((local.dx * 0.75) / box.size.width) - 0.4;
    } else {
      return ((local.dy * 0.75) / box.size.height) - 0.4;
    }
  }

  void _onPanStart(DragStartDetails details) {
    _controller.stop();
    _controller.value = offsetFromGlobalPos(details.globalPosition);
  }

  void _onPanUpdate(DragUpdateDetails details) {
    _controller.value = offsetFromGlobalPos(details.globalPosition);
  }

  void _onPanEnd(DragEndDetails details) {
    _controller.stop();
    //bool isHor = widget.direction == Axis.horizontal;
    bool changed = false;
    if (_controller.value <= -0.10) {
      setState(() {
        if (widget.optionIndex != null) {
          MyStatefulWidget.of(context).decOptionValue(widget.optionIndex);
        }
      });
      changed = true;
    } else if (_controller.value >= 0.1) {
      setState(() {
        if (widget.optionIndex != null) {
          MyStatefulWidget.of(context).incOptionValue(widget.optionIndex);
        }
      });
      changed = true;
    }
    if (widget.withSpring) {
      final SpringDescription _kDefaultSpring = new SpringDescription.withDampingRatio(
        mass: 0.9,
        stiffness: 250.0,
        ratio: 0.6,
      );
      if (widget.direction == Axis.horizontal) {
        _controller.animateWith(SpringSimulation(_kDefaultSpring, _startAnimationPosX, 0.0, 0.0));
      } else {
        _controller.animateWith(SpringSimulation(_kDefaultSpring, _startAnimationPosY, 0.0, 0.0));
      }
    } else {
      _controller.animateTo(0.0, curve: Curves.bounceOut, duration: Duration(milliseconds: 500));
    }

    if (changed && widget.onChanged != null) {
      widget.onChanged(MyStatefulWidget.of(context).getOptionValue(widget.optionIndex));
    }
  }

  void showInstructionsIfNeeded(BuildContext context) {
    if (MyStatefulWidget.of(context).slideInstructionsVisible) {
      int buttonPressCount = MyStatefulWidget.of(context).optionArrowsUsed;
      if (buttonPressCount >= 5) {
        MyStatefulWidget.of(context).optionArrowsUsed = 0;
        SlideInstructionDialog dialog = SlideInstructionDialog();
        dialog.show(context);
      } else {
        MyStatefulWidget.of(context).optionArrowsUsed = buttonPressCount + 1;
      }
    }
  }
}
