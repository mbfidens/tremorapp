import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/validator.dart';

class FillFormWidget extends StatelessWidget {
  final Icon icon;
  final String hint;
  final TextEditingController controller;
  final bool obsecure;
  final BuildContext context;
  final Validator validator = Validator();

  FillFormWidget(this.icon, this.hint, this.controller, this.obsecure, this.context);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.width * 0.05,),
      child: TextFormField(
        validator: validator.getValidator(context, hint),
        controller: controller,
        obscureText: obsecure,
        textCapitalization: validator.getInputCapitalization(hint),
        keyboardType: validator.getInputType(hint),
        decoration: getDecoration(),
      ),
    );
  }

  bool checkIfPasswordIsCorrect(String password) {
    final Pattern pattern = r'^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{5,}$';
    RegExp regex = new RegExp(pattern);

    debugPrint('Checking password: ' + password + ' ' + regex.hasMatch(password).toString());
    if (regex.hasMatch(password) && password.length > 5)
      return true;
    else
      return false;
  }

  InputDecoration getDecoration() => InputDecoration(
      labelStyle: TextStyle(fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.width * 0.03),
      hintStyle: TextStyle(fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.width * 0.05),
      hintText: AppLocalizations.of(context).translate(hint),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide(
          color: Theme.of(context).primaryColor,
          width: 2,
        ),
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide(
          color: Theme.of(context).primaryColor,
          width: 3,
        ),
      ),
      prefixIcon: Padding(
        child: IconTheme(
          data: IconThemeData(color: Theme.of(context).primaryColor),
          child: icon,
        ),
        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05,
            right: MediaQuery.of(context).size.width * 0.03),
      ));
}
