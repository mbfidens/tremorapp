import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';

class AwardWidget extends StatefulWidget {
  @override
  _AwardWidgetState createState() => _AwardWidgetState();
}

class _AwardWidgetState extends State<AwardWidget> with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animateImage;

  @override
  initState() {
    animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 10000))
      ..addListener(() {
        setState(() {});
      });
    animationController.repeat(reverse: true);
    animateImage = Tween<double>(begin: 70, end: 100).animate(animationController);
    super.initState();
  }

  @override
  dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        Container(
          alignment: Alignment.topCenter,
          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.width * 0.1),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              getImage(context),
              Text(
                AppLocalizations.of(context).translate("Congratulations!"),
                style:
                    TextStyle(color: ColorMain, fontSize: MediaQuery.of(context).size.width * 0.06, fontWeight: FontWeight.w600),
                textAlign: TextAlign.center,
              ),
              Text(AppLocalizations.of(context).translate("You have completed all tasks!"),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: ColorMain, fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w400)),
            ],
          ),
        )
      ],
    );
  }

  Widget getImage(BuildContext context) => Image(
        width: MediaQuery.of(context).size.width * 0.7 * animateImage.value / 100,
        image: AssetImage('lib/Assets/Images/award.png'),
      );
}
