import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/Models/notes_model.dart';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:intl/intl.dart';

class FilledNoteWidget extends StatelessWidget {
  const FilledNoteWidget({@required this.note, @required this.notifyParent});

  final NotesModel note;
  final Function({NotesModel updatedNote}) notifyParent;

  @override
  Widget build(BuildContext context) {
    if(note.title != null){
    return Container(
        height: MediaQuery.of(context).size.height * 0.20,
        decoration: BoxDecoration(
          border: Border.all(color: Theme.of(context).primaryColor, width: 2),
          borderRadius: BorderRadius.circular(16),
        ),
        child: Material(
          borderRadius: BorderRadius.circular(16),
          clipBehavior: Clip.antiAlias,
          color: Theme.of(context).dialogBackgroundColor,
          child: InkWell(
            borderRadius: BorderRadius.circular(16),
            onTap: () {
              notifyParent();
            },
            onLongPress: () {
              notifyParent();
            },
            splashColor: ColorMain.withAlpha(20),
            highlightColor: ColorMain.withAlpha(10),
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[getTitle(context), getContent(context), Spacer(), getToolbar(context)],
              ),
            ),
          ),
        ));}
    else return Container(
    );
  }

  Widget getTitle(BuildContext context) => Container(
      padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.03),
    child: Text(
          '${note.title.trim().length <= 20 ? note.title.trim() : note.title.trim().substring(0, 20) + '...'}',
          style: TextStyle(
              fontFamily: 'ZillaSlab',
              fontSize: MediaQuery.of(context).size.width * 0.04,
              fontWeight: note.isImportant ? FontWeight.w800 : FontWeight.normal),
        ),
  );

  Widget getContent(BuildContext context) => Container(
      padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.03),
        child: Text(
          '${note.content.trim().split('\n').first.length <= 30 ? note.content.trim().split('\n').first : note.content.trim().split('\n').first.substring(0, 30) + '...'}',
          style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.04, color: Colors.grey.shade400),
        ),
      );

  Widget getToolbar(BuildContext context) => Container(
        margin: EdgeInsets.all(MediaQuery.of(context).size.width * 0.03),
        alignment: Alignment.centerRight,
        child: Row(
          children: <Widget>[
            Icon(Icons.flag, size: 16, color: note.isImportant ? ColorMain : Colors.transparent),
            Spacer(),
            Text(
              getNeatDate(),
              textAlign: TextAlign.right,
              style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width * 0.03, color: Colors.grey.shade300, fontWeight: FontWeight.w500),
            ),
          ],
        ),
      );

  String getNeatDate() => DateFormat.yMd().add_Hm().format(note.date) ?? '';
}
