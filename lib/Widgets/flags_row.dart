import 'package:Tremor_App/Backend/app_language.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FlagsRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        getFlag(context, "lt"),
        getFlag(context, "en"),
        getFlag(context, "ru"),
      ],
    );
  }

  getFlag(BuildContext context, String localeName) => Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30)),
          color: Provider.of<AppLanguage>(context).appLocal.languageCode == localeName ? ColorMain : null,
        ),
        child: IconButton(
          icon: Image.asset('lib/Assets/Images/' + localeName + '.png'),
          onPressed: () {
            AppLanguage appLanguage = Provider.of<AppLanguage>(context, listen: false);
            appLanguage.changeLanguage(Locale(localeName));
          },
        ),
      );
}
