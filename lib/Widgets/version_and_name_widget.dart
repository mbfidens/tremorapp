import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:auto_size_text/auto_size_text.dart';

class VersionAndNameWidget extends StatefulWidget {
  const VersionAndNameWidget({Key key}) : super(key: key);

  @override
  _VersionAndNameWidgetState createState() => _VersionAndNameWidgetState();
}

class _VersionAndNameWidgetState extends State<VersionAndNameWidget> {
  String appName = 'ViliMap';
  String packageName;
  String version = '';
  String buildNumber;

  @override
  void initState() {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      if (packageInfo.appName != null) {
        appName = packageInfo.appName;
      }
      if (packageInfo.packageName != null) {
        packageName = packageInfo.packageName;
      }
      if (packageInfo.version != null) {
        version = packageInfo.version;
      }
      if (packageInfo.buildNumber != null) {
        buildNumber = packageInfo.buildNumber;
      }
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
        child: AutoSizeText(
          appName + ' ' + version,
          style: TextStyle(color: Colors.black38),
        ),
      ),
    );
  }
}
