import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/Decorations.dart';
import 'package:Tremor_App/Resources/AppColors.dart';

class NavigatorButtonWidget extends StatelessWidget {
  final Function() onPressedCallback;
  final String text;
  final RoundedRectangleDecorator decorator;
  final bool isPositionedOnLeft;
  final IconData iconLeft;
  final IconData iconRight;
  final Color color;

  NavigatorButtonWidget(
      {@required this.onPressedCallback,
      @required this.text,
      this.decorator,
      this.isPositionedOnLeft = false,
      this.iconLeft = Icons.arrow_back,
      this.iconRight = Icons.arrow_forward,
      this.color = ColorMain,
      });

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onPressedCallback,
      focusColor: Colors.blue,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.05,
        width: getWidth(context),
        decoration: getDecoration(),
        alignment:
            isPositionedOnLeft ? Alignment.centerLeft : Alignment.centerRight,
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: getItemsList(context),
        ),
      ),
    );
  }

  Widget getSpacer(bool isRightSideSpacer) => Container(
        height: 0,
        width: isPositionedOnLeft
            ? isRightSideSpacer ? 500 : 0
            : isRightSideSpacer ? 0 : 500,
      );

  List<Widget> getItemsList(BuildContext context) => isPositionedOnLeft
      ? <Widget>[
          getIcon(context),
          getLabel(context),
        ]
      : <Widget>[
          getLabel(context),
          getIcon(context),
        ];

  Widget getLabel(BuildContext context) => Container(
        height: MediaQuery.of(context).size.height * 0.03,
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 5),
        child: AutoSizeText(
          text,
          minFontSize: 5,
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w600),
        ),
      );

  Widget getIcon(BuildContext context) => Padding(
        padding: EdgeInsets.only(right: 8.0),
        child: Icon(
          isPositionedOnLeft ? iconLeft : iconRight,
          color: Colors.white,
          size: MediaQuery.of(context).size.height * 0.03,
        ),
      );

  Decoration getDecoration() =>
      decorator != null ? decorator.getRoundedEdges() : getDefaultDecoration();

  Decoration getDefaultDecoration() => isPositionedOnLeft
      ? RoundedRectangleDecorator(
              color: color, bottomLeft: 0, bottomRight: 30, topLeft: 0)
          .getRoundedEdges()
      : RoundedRectangleDecorator(
              color: color, bottomLeft: 30, bottomRight: 0, topRight: 0)
          .getRoundedEdges();

  double getWidth(BuildContext context) => text == ''
      ? MediaQuery.of(context).size.width * 0.155
      : MediaQuery.of(context).size.width * 0.45;
}
