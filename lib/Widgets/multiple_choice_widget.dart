import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';

class MultipleChoiceWidget extends StatefulWidget {
  final List<String> names;

  MultipleChoiceWidget(this.names);

  @override
  _MultipleChoiceWidgetState createState() => _MultipleChoiceWidgetState();
}

class _MultipleChoiceWidgetState extends State<MultipleChoiceWidget> {
  static int selected = 0;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      setState(() {
        selected = 0;
        translatePositionToDays(selected);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: List.generate(
          widget.names.length,
          (index) => getItem(index),
        ));
  }

  Widget getItem(int index) => Container(
        padding: EdgeInsets.only(
            top: MediaQuery.of(context).size.width * 0.02,
            left: MediaQuery.of(context).size.width * 0.02,
            right: MediaQuery.of(context).size.width * 0.02),
        alignment: Alignment.center,
        child: Container(
          decoration: BoxDecoration(
            color: selected == index ? ColorMain : Colors.grey,
            borderRadius: BorderRadius.circular(80),
          ),
          width: MediaQuery.of(context).size.width * 0.25,
          child: getButton(index),
        ),
      );

  Widget getButton(int index) => TextButton(
        style: ButtonStyle(
          overlayColor: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
            if (states.contains(MaterialState.focused)) return Colors.transparent;
            return null; // Defer to the widget's default.
          }),
        ),
        onPressed: () {
          setState(() {
            selected = index;
            translatePositionToDays(index);
          });
        },
        child: AutoSizeText(
          widget.names[index],
          style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w400, color: Colors.white),
        ),
      );

  void translatePositionToDays(int position) {
    switch (position) {
      case 0:
        MyStatefulWidget.of(context).setHistoryInterval(7);
        break;
      case 1:
        MyStatefulWidget.of(context).setHistoryInterval(10);
        break;
      case 2:
        MyStatefulWidget.of(context).setHistoryInterval(14);
        break;
    }
  }
}
