import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CheckboxWidget extends StatefulWidget {
    final Function(bool newValue) notifyParent;
    CheckboxWidget(this.notifyParent);

  @override
  _CheckboxWidgetState createState() => _CheckboxWidgetState();
}

class _CheckboxWidgetState extends State<CheckboxWidget> {
    bool value = false;

    @override
  void initState() {
    Future<SharedPreferences> sharedPrefs = SharedPreferences.getInstance();
    sharedPrefs.then((SharedPreferences prefs) {
        setState(() {
            if (prefs.getInt("hideOptionsSliderInstructions") != null &&
                prefs.getInt("hideOptionsSliderInstructions") == 1) {
                value = true;
            }
        });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Checkbox(
        activeColor: ColorMain,
        focusColor: ColorMain,
        value: value,
        onChanged: (bool newValue) {
            setState(() {
                value = newValue;
                widget.notifyParent(newValue);
            });
        },
    );
  }
}
