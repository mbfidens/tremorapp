import 'package:Tremor_App/Widgets/evaluation_picker_widget.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:Tremor_App/Widgets/info_bar_widget.dart';
import 'package:Tremor_App/Widgets/stepper_button_widget.dart';

class MenuItemWidget extends StatefulWidget {
  const MenuItemWidget(this.index, this.isForOptionsEditing);

  final int index;
  final bool isForOptionsEditing;

  @override
  _MenuItemWidgetState createState() => _MenuItemWidgetState();
}

class _MenuItemWidgetState extends State<MenuItemWidget> {
  double height = 60;
  static bool finishedAnimation = false;

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      getAnimatedContainer(),
      getItemListTile(context, widget.index),
      isLastItem(context) ? Container() : getBackgroundButton(),
      getInfoBarIfNeeded()
    ]);
  }

  Widget getAnimatedContainer() => AnimatedContainer(
        onEnd: () {
          setState(() {
            finishedAnimation = true;
          });
        },
        alignment: Alignment.centerLeft,
        height: height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
        ),
        duration: Duration(milliseconds: 500),
        curve: Curves.fastOutSlowIn,
      );

  Widget getItemListTile(BuildContext context, int index) {
    if (isLastItem(context) && widget.isForOptionsEditing) {
      return getEvaluationTile(index);
    } else {
      return getNormalTile(index);
    }
  }

  Widget getNormalTile(int index) {
    MyStatefulWidgetState state = MyStatefulWidget.of(context);
    return Container(
      height: 60,
      padding:
          EdgeInsets.only(left: getWidth(0.02), right: getWidth(0.05), top: 10),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
              color: Colors.white,
              padding: EdgeInsets.only(right: 5, left: 10),
              child: Image(
                  width: 40,
                  height: 40,
                  image: AssetImage(state.getOptionIcon(index)))),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 10),
              child: AutoSizeText(state.getOptionKeyword(index),
                  wrapWords: false,
                  minFontSize: 3,
                  maxLines: 2,
                  style: TextStyle()),
            ),
          ),
          getButtonConsideringType(),
        ],
      ),
    );
  }

  Widget getEvaluationTile(int index) {
    return Container(
      height: 70,
      padding:
          EdgeInsets.only(left: getWidth(0.06), right: getWidth(0.05), top: 15),
      child: Stack(children: <Widget>[
        EvaluationPickerWidget((double rate) {},
            MyStatefulWidget.of(context).getOptionValue(index).toDouble()),
        Row(
          children: [
            Container(
              height: 0,
              width: getWidth(0.55),
            ),
            Expanded(
              child: getBackgroundButton(),
            ),
          ],
        )
      ]),
    );
  }

  Widget getBackgroundButton() => RawMaterialButton(
      splashColor: Colors.transparent,
      onPressed: () {
        setState(() {
          finishedAnimation = false;
          height = height == 95 ? 60 : 95;
        });
      },
      child: getPressableAnimatedContainer());

  Widget getPressableAnimatedContainer() => AnimatedContainer(
        margin: EdgeInsets.only(top: 10),
        width: MediaQuery.of(context).size.width / 1.9,
        height: height,
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(30),
        ),
        duration: Duration(seconds: 1),
        curve: Curves.fastOutSlowIn,
      );

  Widget getInfoBarIfNeeded() => height == 95 && finishedAnimation
      ? InfoBarWidget(widget.index)
      : Container();

  Widget getButtonConsideringType() => widget.isForOptionsEditing
      ? getEditOptionsButton()
      : getHistoryViewButton();

  Widget getEditOptionsButton() => Container(
        height: MediaQuery.of(context).size.height * 0.12,
        width: MediaQuery.of(context).size.width * 0.28,
        child: Center(
            child: StepperButtonWidget(
          initialValue:
              MyStatefulWidget.of(context).getOptionValue(widget.index),
          optionIndex: widget.index,
        )),
      );

  Widget getHistoryViewButton() => Container(
        decoration: BoxDecoration(
          color: MyStatefulWidget.of(context).getChosenHistoryMenuItem() ==
                  widget.index
              ? ColorMain
              : Colors.blue.shade300,
          borderRadius: BorderRadius.circular(80),
        ),
        height: MediaQuery.of(context).size.height * 0.05,
        width: MediaQuery.of(context).size.width * 0.20,
        child: Center(
            child: TextButton(
          style: ButtonStyle(
            overlayColor: MaterialStateProperty.resolveWith<Color>(
                (Set<MaterialState> states) {
              if (states.contains(MaterialState.pressed))
                return Colors.transparent;
              return null; // Defer to the widget's default.
            }),
          ),
          onPressed: () {
            setState(() {
              MyStatefulWidget.of(context).setHistoryButtonIndex(
                  MyStatefulWidget.of(context).getChosenHistoryMenuItem() ==
                          widget.index
                      ? null
                      : widget.index);
            });
          },
          child: Container(
            child: Icon(Icons.remove_red_eye, color: Colors.white),
          ),
        )),
      );

  double getWidth(double ratio) => MediaQuery.of(context).size.width * ratio;

  bool isLastItem(BuildContext context) =>
      widget.index == MyStatefulWidget.of(context).getOptionLength() - 1;
}
