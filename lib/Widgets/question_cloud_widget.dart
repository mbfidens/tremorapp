import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Widgets/bubble_widget.dart';
import 'package:Tremor_App/Dialogs/option_button_slide_instruction_dialog.dart';
import 'package:Tremor_App/Widgets/animated_fab_widget.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';

class QuestionCloudWidget extends StatelessWidget {
  final String uid;
  final BubbleWidget bubble;
  QuestionCloudWidget(this.bubble, this.uid);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.02),
            child: getBubble(),
          ),
          isTimeForAnimation()
              ? AnimatedFabWidget(Icons.favorite, () {
                  evaluationDialog(context);
                }, hero: 'CloudFab',)
              : getFloatingButton(context)
        ],
      ),
    );
  }

  Widget getBubble() => isTimeForReminderString() ? bubble : BubbleWidget.empty();

  Widget getFloatingButton(BuildContext context) => Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: FloatingActionButton(
          backgroundColor: ColorMain.withOpacity(0.7),
          elevation: 5,
          child: Icon(
            Icons.favorite,
            color: Colors.white,
          ),
          onPressed: () {
            evaluationDialog(context);
          },
        ),
      );

  void evaluationDialog(BuildContext context) {
    OptionButtonSlideInstructionDialog dialog = OptionButtonSlideInstructionDialog();
    dialog.show(context, MyStatefulWidget.of(context).uid);
  }

  bool isTimeForAnimation() => DateTime.now().hour >= 0;

  bool isTimeForReminderString() => DateTime.now().hour >= 19;
}
