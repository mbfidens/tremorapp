import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';

class DatePickerFabWidget extends StatefulWidget {
  final String uid;

  const DatePickerFabWidget({Key key, @required this.uid}) : super(key: key);

  @override
  _DatePickerFabWidgetState createState() => _DatePickerFabWidgetState();
}

class _DatePickerFabWidgetState extends State<DatePickerFabWidget> {
  List<DateTime> listOfEditedDates = [];

  @override
  Widget build(BuildContext context) {
    return getDefaultFab();
  }

  bool checkIfTodaySelected() {
    bool isToday = MyStatefulWidget.of(context).getCurrentlySelectedDate().difference(DateTime.now()).inDays == 0 ? true : false;
    return isToday;
  }

  Widget getDefaultFab() => FloatingActionButton(
        heroTag: 'DateFab',
        backgroundColor: checkIfTodaySelected() ? ColorMain : Colors.redAccent.withOpacity(0.9),
        elevation: 5,
        child: Icon(
          Icons.calendar_today,
          color: Colors.white,
        ),
        onPressed: () {
          showDateSelector(context);
        },
      );

  Future<void> showDateSelector(BuildContext context) async {
    MyStatefulWidget.of(context).getSaved.forEach((element) {
      listOfEditedDates.add(element.date);
    });

    final DateTime selectedDateTime = await showRoundedDatePicker(
      height: MediaQuery.of(context).size.height * 0.5,
      context: context,
      locale: AppLocalizations.of(context).locale,
      initialDate: MyStatefulWidget.of(context).getCurrentlySelectedDate(),
      lastDate: DateTime.now(),
      borderRadius: 30,
      theme: ThemeData(
        primaryColor: ColorMain.withOpacity(0.9),
      ),
      builderDay: dayBuilder,
      styleDatePicker: getDatePickerStyle(),
    );

    if (selectedDateTime != null) {
      setState(() {
        MyStatefulWidget.of(context).setCurrentlySelectedDate(selectedDateTime);
      });
    }
  }

  Widget dayBuilder(DateTime dateTime, bool isCurrentDay, bool isSelected, TextStyle defaultTextStyle) {
    DateTime dateTimeAt12 = DateTime(dateTime.year, dateTime.month, dateTime.day, 12);
    return Container(
      decoration: BoxDecoration(color: getColor(isSelected, dateTimeAt12), shape: BoxShape.circle),
      child: Center(
        child: Text(
          dateTime.day.toString(),
          style: defaultTextStyle,
        ),
      ),
    );
  }

  Color getColor(bool isSelected, DateTime dateToCheck) {
    if (isSelected) {
      return ColorMain;
    } else {
      return checkIfAnyDayAlreadyExists(dateToCheck) ? ColorMain.withOpacity(0.2) : Colors.transparent;
    }
  }

  bool checkIfAnyDayAlreadyExists(DateTime dateToCheck) => listOfEditedDates.any((e) => e.difference(dateToCheck).inDays == 0);

  MaterialRoundedDatePickerStyle getDatePickerStyle() => MaterialRoundedDatePickerStyle(
        textStyleDayButton: TextStyle(fontSize: 0, color: Colors.white),
        textStyleYearButton: TextStyle(
          fontSize: MediaQuery.of(context).size.width * 0.08,
          color: Colors.white,
        ),
        textStyleDayHeader: TextStyle(
          fontSize: 0,
        ),
        textStyleCurrentDayOnCalendar: TextStyle(
            fontSize: MediaQuery.of(context).size.width * 0.04,
            color: ColorMain,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic),
        textStyleDayOnCalendar: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.04, color: ColorMain),
        textStyleDayOnCalendarSelected:
            TextStyle(fontSize: MediaQuery.of(context).size.width * 0.04, color: Colors.white, fontWeight: FontWeight.bold),
        textStyleDayOnCalendarDisabled: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.04, color: Colors.grey),
        decorationDateSelected: BoxDecoration(color: ColorMain.withOpacity(0.9), shape: BoxShape.circle),
        textStyleMonthYearHeader: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.05, color: ColorMain),
        sizeArrow: MediaQuery.of(context).size.width * 0.1,
        colorArrowNext: ColorMain,
        colorArrowPrevious: ColorMain,
        paddingMonthHeader: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.05),
        textStyleButtonPositive:
            TextStyle(fontSize: MediaQuery.of(context).size.width * 0.04, color: Colors.white, fontWeight: FontWeight.bold),
        textStyleButtonNegative:
            TextStyle(fontSize: MediaQuery.of(context).size.width * 0.04, color: Colors.white.withOpacity(0.9)),
        backgroundPicker: Colors.white,
        backgroundActionBar: ColorMain.withOpacity(0.9),
        backgroundHeaderMonth: Colors.white,
      );
}
