import 'package:Tremor_App/Screens/profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Screens/welcome_screen.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:Tremor_App/Backend/User/global_user_profile.dart';
import 'package:Tremor_App/Widgets/flags_row.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:Tremor_App/Backend/User/user_authentication.dart';
import 'package:Tremor_App/Dialogs/log_in_dialog.dart';
import 'package:Tremor_App/Widgets/version_and_name_widget.dart';

class DrawerWidget extends StatefulWidget {
  final UserAuthentication authentication;
  DrawerWidget(this.authentication);

  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 5,
      child: Container(
        child: Column(
          children: <Widget>[
            Expanded(
              child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  DrawerHeader(
                    decoration: BoxDecoration(
                      color: ColorMain,
                    ),
                    child: Column(children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(bottom: 5),
                          child: GlobalUserProfile(profile: widget.authentication.userProfile).avatar ?? getDefaultAvatar),
                      Container(
                        child: Flexible(
                          child: AutoSizeText(
                            getNameText(context),
                           maxLines: 2,
                            textAlign: TextAlign.center,
                           style: TextStyle(
                             color: Colors.white,
                                fontSize: MediaQuery.of(context).size.width * 0.05,
                           ),
                         ),
                        ),
                      )
                    ]),
                  ),
                  Container(
                    color: GlobalUserProfile(profile: widget.authentication.userProfile).isSigned == false
                        ? Colors.grey.shade400
                        : Colors.transparent,
                    child: ListTile(
                      leading: Icon(Icons.account_circle),
                      title: Text(
                          AppLocalizations.of(context).translate('Profile')),
                      onTap: () {
                       if (GlobalUserProfile(profile: widget.authentication.userProfile).isSigned == true) {
                         Navigator.of(context)
                             .push(MaterialPageRoute(builder: (context) {
                           return ProfileScreen(widget.authentication, update);
                         }));
                       }
                      },
                    ),
                  ),
                  Container(
                    child: ListTile(
                      leading: Icon(Icons.library_books),
                      title: Text(AppLocalizations.of(context)
                          .translate('Instructions for use')),
                      onTap: () {
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) {
                          return WelcomeScreen();
                        }));
                      },
                    ),
                  ),
                  Container(
                    child: ListTile(
                      leading: Icon(Icons.email),
                      title: Text(
                          AppLocalizations.of(context).translate("Contact us")),
                      onTap: () {
                        launch(_emailLaunchUri.toString());
                      },
                    ),
                  ),
                  getLogInOutButton(context),
                  FlagsRow(),
                ],
              ),
            ),
            VersionAndNameWidget(),
          ],
        ),
      ),
    );
  }

  void update(){
    setState(() {

    });
  }

  String getNameText(BuildContext context){
    String nameText = AppLocalizations.of(context)
        .translate("Please sign in");
    GlobalUserProfile userDetails = GlobalUserProfile(profile: widget.authentication.userProfile);

    if(userDetails.name != null){
      nameText = userDetails.name;
    } else {
      nameText = '';
    }
    if(userDetails.surname != null){
      nameText += ' ' + userDetails.surname;
    }
    return nameText;
  }

  Widget getLogInOutButton(BuildContext context) => GlobalUserProfile(profile: widget.authentication.userProfile).isSigned
      ? ListTile(
          leading: Icon(Icons.exit_to_app),
          title: Text(AppLocalizations.of(context).translate('Log out')),
          onTap: () {
            widget.authentication.signOut(context);
          },
        )
      : Container(
          //color: ColorMain,
          child: ListTile(
            leading: Icon(
              Icons.supervisor_account,
              color: Colors.black87,
            ),
            title: Text(
              AppLocalizations.of(context).translate('Sign in'),
              style: TextStyle(fontWeight: FontWeight.w900),
            ),
            onTap: () {
              LogInDialog logInDialog = LogInDialog();
              logInDialog.show(context, widget.authentication);
            },
          ),
        );

  final Uri _emailLaunchUri = Uri(
      scheme: 'mailto',
      path: 'info@vilimed.com',
      queryParameters: {'subject': 'Inquiry from ViliMap ' + DateTime.now().toIso8601String()});

  Widget get getDefaultAvatar => Container(
    height: 100,
    width: 100,
    child: Image(
      color: Colors.grey.shade200,
      image: AssetImage('lib/Assets/Images/user.png'),
    ),
  );
}


