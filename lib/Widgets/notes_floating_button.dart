import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:Tremor_App/Dialogs/option_button_slide_instruction_dialog.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';

class NotesFloatingButton extends StatelessWidget {
  NotesFloatingButton();

  @override
  Widget build(BuildContext context) {
    return getFloatingButton(context);
  }

  Widget getFloatingButton(BuildContext context) => Padding(
    padding: const EdgeInsets.only(right: 0),
    child: FloatingActionButton(
      backgroundColor: ColorMain,
      elevation: 5,
      child: Icon(
        Icons.note_add_outlined,
        color: Colors.white,
      ),
      onPressed: () {
        evaluationDialog(context);
      },
    ),
  );

  void evaluationDialog(BuildContext context) {
    OptionButtonSlideInstructionDialog dialog = OptionButtonSlideInstructionDialog();
    dialog.show(context, MyStatefulWidget.of(context).uid);
  }
}
