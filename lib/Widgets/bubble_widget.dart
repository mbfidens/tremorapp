import 'package:flutter/material.dart';
import 'package:Tremor_App/Resources/AppColors.dart';

class BubbleWidget extends StatelessWidget {
  final String message;
  final IconData iconData;
  BubbleWidget({@required this.message, this.iconData});

  factory BubbleWidget.empty() => BubbleWidget(message: '');

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      hoverColor: getWidgetColor(),
      child: Container(
        padding: EdgeInsets.all(getWidgetHeight(context)),
        child: getContent(context),
        decoration: BoxDecoration(
            color: getWidgetColor(),
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(0),
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30))),
      ),
      onPressed: (){},
    );
  }
  double getWidgetHeight(BuildContext context) => MediaQuery.of(context).size.height * (isWidgetEmpty() ? 0 : 0.01);
  
  Color getWidgetColor() => isWidgetEmpty() ? Colors.transparent : ColorMain;

  bool isWidgetEmpty() => message == '';

  Widget getContent(BuildContext context) => Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(padding: EdgeInsets.only(right: 5), child: Icon(iconData, size: getIconSize(context),)),
          Text(message, style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600), textAlign: TextAlign.right,),
        ],
      );

  double getIconSize(BuildContext context) => iconData != null ? MediaQuery.of(context).size.height * 0.03 : 0;
}
