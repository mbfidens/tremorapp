import 'package:flutter/services.dart';
import 'dart:async';


class KeyboardVisibilityTracker {
  final Function(bool visible) onChange;
  final Function onShow;
  final Function onHide;

  KeyboardVisibilityTracker({this.onChange, this.onShow, this.onHide});
}

class KeyboardVisibilityNotification {
  static const EventChannel _keyboardVisibilityStream =
  const EventChannel('keyboard_visibility');
  static Map<int, KeyboardVisibilityTracker> _list =
  Map<int, KeyboardVisibilityTracker>();
  static StreamSubscription _keyboardVisibilitySubscription;
  static int _currentIndex = 0;

  bool isKeyboardVisible = false;

  KeyboardVisibilityNotification() {
    _keyboardVisibilitySubscription ??= _keyboardVisibilityStream
        .receiveBroadcastStream()
        .listen(onKeyboardEvent);
  }

  void onKeyboardEvent(dynamic arg) {
    isKeyboardVisible = (arg as int) == 1;

    // send a message to all subscribers notifying them about the new state
    _list.forEach((subscriber, s) {
      try {
        if (s.onChange != null) {
          s.onChange(isKeyboardVisible);
        }
        if ((s.onShow != null) && isKeyboardVisible) {
          s.onShow();
        }
        if ((s.onHide != null) && !isKeyboardVisible) {
          s.onHide();
        }
      } catch (_) {}
    });
  }

  int addNewListener(
      {Function(bool) onChange, Function onShow, Function onHide}) {
    _list[_currentIndex] = KeyboardVisibilityTracker(
        onChange: onChange, onShow: onShow, onHide: onHide);
    return _currentIndex++;
  }

  int addNewSubscriber(KeyboardVisibilityTracker subscriber) {
    _list[_currentIndex] = subscriber;
    return _currentIndex++;
  }

  void removeListener(int subscribingId) {
    _list.remove(subscribingId);
  }

  dispose() {
    if (_list.length == 0) {
      _keyboardVisibilitySubscription?.cancel()?.catchError((e) {});
      _keyboardVisibilitySubscription = null;
    }
  }
}