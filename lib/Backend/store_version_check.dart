import 'dart:io';
import 'package:Tremor_App/Resources/AppColors.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:Tremor_App/Backend/app_localization.dart';

// Firebase remote config variable: force_update_current_build_number
// Change build number when new version is available on store to show pop up

class StoreVersionCheck {
  static const APP_STORE_URL =
      'https://apps.apple.com/lt/app/vilimap/id1551653373';
  static const PLAY_STORE_URL =
      'https://play.google.com/store/apps/details?id=com.vilimed.Tremor_App';

  versionCheck(context) async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    int currentBuildNumber = int.parse(info.buildNumber);
    final RemoteConfig remoteConfig = RemoteConfig.instance;

    try {
      await remoteConfig.fetch();
      await remoteConfig.activate();
      remoteConfig.getString('force_update_current_build_number');
      int newVersion = int.parse(remoteConfig
          .getString('force_update_current_build_number')
          .trim()
          .replaceAll(".", ""));
      if (newVersion > currentBuildNumber) {
        _showVersionDialog(context);
      }
    } catch (exception) {
      // Fetch throttled.
      print(exception);
    }
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _showVersionDialog(context) async {
    await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String title =
            AppLocalizations.of(context).translate("New Update Available");
        String message = AppLocalizations.of(context).translate(
            "There is a newer version of app available please update it now.");
        String btnLabel = AppLocalizations.of(context).translate("Update Now");
        String btnLabelCancel = AppLocalizations.of(context).translate("Later");
        return Platform.isIOS
            ? new CupertinoAlertDialog(
                title: Text(title),
                content: Text(
                  message,
                  textAlign: TextAlign.center,
                ),
                actions: <Widget>[
                  TextButton(
                    child: Text(
                      btnLabel,
                      textAlign: TextAlign.center,
                    ),
                    onPressed: () => _launchURL(APP_STORE_URL),
                  ),
                  TextButton(
                    child: Text(
                      btnLabelCancel,
                      textAlign: TextAlign.center,
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                ],
              )
            : new AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0)),
                title: Text(title),
                content: Text(
                  message,
                  textAlign: TextAlign.center,
                ),
                actions: <Widget>[
                  TextButton(
                    child: Text(
                      btnLabel,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: ColorMain,
                      ),
                    ),
                    onPressed: () => _launchURL(PLAY_STORE_URL),
                  ),
                  TextButton(
                    child: Text(
                      btnLabelCancel,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: ColorMain,
                      ),
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                ],
              );
      },
    );
  }
}
