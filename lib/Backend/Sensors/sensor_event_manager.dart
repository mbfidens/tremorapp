import 'package:flutter/material.dart';
import 'package:sensors_plus/sensors_plus.dart';
import 'package:Tremor_App/Backend/Models/SensorModel.dart';

class SensorEventManager {
    SensorEventManager();
    SensorModel accModel = SensorModel(DateTime.now(), SensorTypes.Accelerometer);
    SensorModel gyrModel = SensorModel(DateTime.now(), SensorTypes.Gyroscope);

    List<List<double>> accelerometerLPFBuffer = <List<double>>[<double>[]];
    List<List<double>> userAccelerometerLPFBuffer = <List<double>>[<double>[]];
    List<List<double>> gyroscopeLPFBuffer = <List<double>>[<double>[]];

    int accLastTime = 0;
    int gyrLastTime = 0;

    static const int LPF = 10000;

    void actOnNewAccData(BuildContext context, AccelerometerEvent event){
        int period = getTimeSinceLastAccEvent();
        accelerometerLPFBuffer.add(<double>[event.x, event.y, event.z]);
        if(checkIfPassedLPF(period)){
            accModel.add(context, getAverageAcc(), period);
            accLastTime = DateTime.now().microsecondsSinceEpoch;
        }
    }

    void actOnNewGyrData(BuildContext context, GyroscopeEvent event){
        int period = getTimeSinceLastGyrEvent();
        gyroscopeLPFBuffer.add(<double>[event.x, event.y, event.z]);
        if(checkIfPassedLPF(period)){
            gyrModel.add(context, getAverageGyr(), period);
            gyrLastTime = DateTime.now().microsecondsSinceEpoch;
        }
    }

    int getTimeSinceLastAccEvent() => DateTime.now().microsecondsSinceEpoch - accLastTime;

    int getTimeSinceLastGyrEvent() => DateTime.now().microsecondsSinceEpoch - gyrLastTime;

    bool checkIfPassedLPF(int period) => period > LPF;
    
    List<double> getAverageAcc() {
        List<double> result =  accelerometerLPFBuffer.reduce((List<double> sum, element){
            if(sum.isNotEmpty && sum != null) {
                sum[0] = sum[0] + element[0];
                sum[1] = sum[1] + element[1];
                sum[2] = sum[2] + element[2];
            } else{
                sum = <double>[element[0],element[1], element[2]];
            }
            return sum;
        });
        result[0] = result[0] / accelerometerLPFBuffer.length;
        result[1] = result[1] / accelerometerLPFBuffer.length;
        result[2] = result[2] / accelerometerLPFBuffer.length;
        accelerometerLPFBuffer = <List<double>>[];
        return result;
    }

    List<double> getAverageGyr() {
        List<double> result =  gyroscopeLPFBuffer.reduce((List<double> sum, element){
            if(sum.isNotEmpty && sum != null) {
                sum[0] = sum[0] + element[0];
                sum[1] = sum[1] + element[1];
                sum[2] = sum[2] + element[2];
            } else{
                sum = <double>[element[0],element[1], element[2]];
            }
            return sum;
        });
        result[0] = result[0] / gyroscopeLPFBuffer.length;
        result[1] = result[1] / gyroscopeLPFBuffer.length;
        result[2] = result[2] / gyroscopeLPFBuffer.length;
        gyroscopeLPFBuffer = <List<double>>[];
        return result;
    }
}