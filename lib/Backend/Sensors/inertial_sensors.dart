import 'dart:async';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/Sensors/sensor_event_manager.dart';
import 'package:sensors_plus/sensors_plus.dart';

class InertialSensors extends StatefulWidget {
  InertialSensors(
      {this.accActive = true,
      this.userAccActive = false,
      this.gyrActive = true,
      this.visible = false});
  final bool accActive;
  final bool userAccActive;
  final bool gyrActive;
  final bool visible;

  @override
  _InertialSensorsState createState() => _InertialSensorsState();
}

class _InertialSensorsState extends State<InertialSensors> {
  List<double> accLastValue;
  List<double> userAccLastValue;
  List<double> gyrLastValue;

  SensorEventManager sensorEventManager = SensorEventManager();

  List<StreamSubscription<dynamic>> streamSubscriptions = <StreamSubscription<dynamic>>[];

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[Text(widget.visible ? (getAccText() + getUserAccText() + getGyrText()) : '')],
    );
  }

  @override
  void dispose() {
    super.dispose();
    for (StreamSubscription<dynamic> subscription in streamSubscriptions) {
      subscription.cancel();
    }
  }

  @override
  void initState() {
    super.initState();
    if (widget.accActive) {
      addAccEvent();
    }
    if (widget.userAccActive) {
      addUserAccEvent();
    }
    if (widget.gyrActive) {
      addGyrEvent();
    }
  }

  String getAccText() => accLastValue != null
      ? ('A:' +
              (accLastValue[0] ?? 0).toStringAsFixed(2) +
              ' ' +
              (accLastValue[1] ?? 0).toStringAsFixed(2) +
              ' ' +
              (accLastValue[2] ?? 0).toStringAsFixed(2)) +
          '\n'
      : 'A:null \n';

  String getUserAccText() => userAccLastValue != null
      ? ('U:' +
              (userAccLastValue[0] ?? 0).toStringAsFixed(2) +
              ' ' +
              (userAccLastValue[1] ?? 0).toStringAsFixed(2) +
              ' ' +
              (userAccLastValue[2] ?? 0).toStringAsFixed(2)) +
          '\n'
      : 'U:null \n';

  String getGyrText() => gyrLastValue != null
      ? ('G:' +
              (gyrLastValue[0] ?? 0).toStringAsFixed(2) +
              ' ' +
              (gyrLastValue[1] ?? 0).toStringAsFixed(2) +
              ' ' +
              (gyrLastValue[2] ?? 0).toStringAsFixed(2)) +
          '\n'
      : 'G:null \n';

  void addAccEvent() {
    streamSubscriptions.add(accelerometerEvents.listen((AccelerometerEvent event) {
      setState(() {
        accLastValue = <double>[event.x, event.y, event.z];
        sensorEventManager.actOnNewAccData(context, event);
      });
    }));
  }

  void addUserAccEvent() {
    streamSubscriptions.add(userAccelerometerEvents.listen((UserAccelerometerEvent event) {
      setState(() {
        userAccLastValue = <double>[event.x, event.y, event.z];
      });
    }));
  }

  void addGyrEvent() {
    streamSubscriptions.add(gyroscopeEvents.listen((GyroscopeEvent event) {
      setState(() {
        gyrLastValue = <double>[event.x, event.y, event.z];
        sensorEventManager.actOnNewGyrData(context, event);
      });
    }));
  }
}
