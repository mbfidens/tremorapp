import 'dart:async';
import 'dart:io';
import 'package:Tremor_App/Backend/data_file.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:android_id/android_id.dart';
import 'package:flutter/cupertino.dart';
import 'package:Tremor_App/Backend/Storage/firebase_storage_controller.dart';
import 'package:flutter/material.dart';
import 'package:device_info_plus/device_info_plus.dart';

class GyroscopeLogs {
    final String dataString;
    final String name;
    GyroscopeLogs(this.dataString, this.name);

    static const _androidIdPlugin = AndroidId();

    void saveData(BuildContext context){
        //logLocally();
        //logFirebase(context);
    }

    void logLocally() {
        DataFile logFile = DataFile(name + '.txt');
        logFile.appendString('');
        logFile.appendString(dataString);
    }

    Future<void> logFirebase(BuildContext context) async {
            FirebaseStorageController firebaseStorageController = FirebaseStorageController();
            try {
                await firebaseStorageController.storeFile(
                    tempLocalFileName: 'gyroscope_firebase',
                    cloudFolderName: 'Gyroscope' + '/' + (await setIdForFirebase(context)),
                    cloudFileName: name,
                    dataToSave: dataString);
            } catch (e) {
                debugPrint('GyroscopeLogs.logFirebase() ' + e.toString());
            }
    }

    Future<String> setIdForFirebase(BuildContext context) async {
        DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
        String tempId = MyStatefulWidget.of(context).uid;

        if (tempId != null && tempId.length > 0) {
            return 'USRID ' + tempId;
        } else if (Platform.isAndroid) {
            AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
            print('Running on ${androidInfo.model}'); // e.g. "Moto G (4)"
            return 'ANDID ' + await _androidIdPlugin.getId();
        } else if (Platform.isIOS) {
            IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
            print('Running on ${iosInfo.utsname.machine}'); // e.g. "iPod7,1"
            return 'IOSID ' + iosInfo.identifierForVendor;
        } else {
            print('Running on unknown machine'); // e.g. "iPod7,1"
            return 'DATID ' + DateTime.now().toIso8601String();
        }
    }
}
