import 'dart:async';
import 'dart:convert';
import 'package:Tremor_App/Backend/Models/options_model.dart';
import 'package:Tremor_App/Backend/data_file.dart';
import 'package:Tremor_App/Resources/Exceptions.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:Tremor_App/Backend/Models/options_model_list.dart';
import 'package:Tremor_App/Backend/Storage/firebase_storage_controller.dart';
import 'dart:io';
import 'package:flutter/material.dart';

const int daysToSave = 365;

class OptionLogs {
  DataFile logFile;
  OptionsModelList options = OptionsModelList.empty();
  String userId;
  bool alreadyInitiated = false;
  FirebaseStorageController controller = FirebaseStorageController();

  Function({bool newDataDownloaded}) notifyParent;
  OptionLogs(this.notifyParent);

  int get length => options != null ? options.length : 0;

  List<OptionsModel> get getList => options != null ? options.data : null;

  Future<void> init(String uid) async {
    userId = uid;
    if (!alreadyInitiated) {
      logFile = DataFile('Options_$uid.txt');
      try {
        options = await getLogsFromMemory(uid);
        if (options == null) throw (LocalLogDoesNotExist);
        alreadyInitiated = true;
      } catch (e) {
        debugPrint('OptionLogs.init() ' + e.toString());
        readRemotelySavedLogs(uid);
      }
    }
  }

  Future<OptionsModelList> getLogsFromMemory(String uid) async {
    try {
      OptionsModelList local = await readLocallySavedLogs();
      OptionsModelList remote = await readRemotelySavedLogs(uid);

      if (local != null && remote != null) {
        debugPrint('BOTH LOGS READ SUCCESSFULLY');
        local = await onSuccessfulDataRead(
          uid,
          local,
          remote,
        );
      } else {
        if (local == null && remote != null) {
          debugPrint('LOCAL LOG IS NULL');
          local = remote;
          await logFile.reWriteJson(local.toJson());
        } else if (local == null && remote == null) {
          debugPrint('BOTH NULL');
          local = await onFailedDataRead(uid, local, remote);
        }
      }
      return local;
    } catch (e) {
      debugPrint('OptionsLogs.getLogsFromMemory() ' + e.toString());
      return OptionsModelList.empty();
    }
  }

  Future<OptionsModelList> onSuccessfulDataRead(String uid, OptionsModelList local, OptionsModelList remote) async {
    if (json.encode(local.toJson()) != json.encode(remote.toJson())) {
      OptionsModelList mergedList = remote;
      mergedList.merge(local);
      local = mergedList;
      await logFile.reWriteJson(local.toJson());
      await reWriteFirebase(local, uid);
    }
    return local;
  }

  Future<OptionsModelList> onFailedDataRead(
    String uid,
    OptionsModelList local,
    OptionsModelList remote,
  ) async {
    local = OptionsModelList.empty();
    await logFile.reWriteJson(local.toJson());
    return local;
  }

  Future<OptionsModelList> readLocallySavedLogs() async {
    try {
      String contents = await logFile.readString();
      if (contents == null || contents.isEmpty) return null;
      OptionsModelList list = OptionsModelList.fromJson(jsonDecode(contents));
      if (list == null) return null;
      return list;
    } on FormatException catch (e) {
      debugPrint('OptionLogs.readLocallySavedLogs() ' + e.toString());
      return OptionsModelList.empty();
    } catch (e) {
      debugPrint('OptionLogs.readLocallySavedLogs() ' + e.toString());
      return null;
    }
  }

  Future<OptionsModelList> readRemotelySavedLogs(String uid) async {
    try {
      File downloadedFile =
          await controller.getFile(localFileName: 'Options_Firebase_' + uid, cloudFolderName: 'OptionData', cloudFileName: uid);
      String downloadedDataString = await downloadedFile.readAsString();
      OptionsModelList decodedList = OptionsModelList.fromJson(jsonDecode(downloadedDataString));
      return decodedList;
    } on FormatException catch (e) {
      debugPrint('OptionLogs.readRemotelySavedLogs() ' + e.toString());
      return OptionsModelList.empty();
    } on FirebaseException catch (e) {
      debugPrint('OptionLogs.readRemotelySavedLogs() ' + e.toString());
      if (e.code == 'storage/object-not-found') {
        return OptionsModelList.empty();
      } else {
        return null;
      }
    } catch (e) {
      debugPrint('OptionLogs.readRemotelySavedLogs() ' + e.toString());
      return null;
    }
  }

  Future<void> logOption(OptionsModel dataToLog, String uid) async {
    try {
      if (uid != null && uid.length > 0) {
        userId = uid;
      }
      if (alreadyInitiated) {
        options.add(dataToLog);
        await logFile.reWriteJson(options.toJson());
        await logFirebase(uid);
      }
    } catch (e) {
      debugPrint('OptionLogs.logOption() ' + e.toString());
    }
  }

  Future<void> logFirebase(String uid) async {
    userId = uid;
    if (options != null && userId != null) {
      try {
        await syncDataWithCloud();
        await controller.storeFile(
            tempLocalFileName: 'Options_Firebase_' + uid,
            cloudFolderName: 'OptionData',
            cloudFileName: uid,
            dataToSave: jsonEncode(options.toJson()));
        debugPrint('OptionLogs.logFirebase() Stored in ID ' + uid);
      } catch (e) {
        debugPrint('OptionLogs.logFirebase() ' + e.toString());
      }
    }
  }

  Future<void> syncDataWithCloud() async {
    /*Log remotely only on successful data read*/
    OptionsModelList modelOnCloud = await readRemotelySavedLogs(userId);
    if (modelOnCloud != null) {
      if (modelOnCloud.length > 0) {
        modelOnCloud.merge(options);
        options = modelOnCloud;
      }
    } else {
      throw (RemoteLogCantBeAccessed);
    }
  }

  Future<void> reWriteFirebase(OptionsModelList newList, String uid) async {
    try {
      await controller.storeFile(
          tempLocalFileName: 'Options_Firebase_' + uid,
          cloudFolderName: 'OptionData',
          cloudFileName: uid,
          dataToSave: json.encode(newList.toJson()));
      debugPrint('OptionLogs.reWriteFirebase() Replaced data. Stored in ID ' + uid);
    } catch (e) {
      debugPrint('OptionLogs.reWriteFirebase() ' + e.toString());
    }
  }
}
