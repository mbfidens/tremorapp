import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:Tremor_App/Backend/Models/notes_model.dart';
import 'package:Tremor_App/Backend/Models/notes_model_list.dart';
import 'package:Tremor_App/Backend/data_file.dart';
import 'package:flutter/cupertino.dart';
import 'package:Tremor_App/Backend/Storage/firebase_storage_controller.dart';

class NoteLogs {
  DataFile file;
  NotesModelList savedData = NotesModelList.empty();

  NoteLogs();

  Future<void> dataInit(BuildContext context, String uid) async {
    file = DataFile('NoteLogs_$uid.txt');
    try {
      await readLogs(context, uid);
    } catch (e) {
      debugPrint(e.toString());
      readRemotelySavedLogs(context, uid);
    }
  }

  Future<String> readLogs(BuildContext context, String uid) async {
    file = DataFile('NoteLogs_$uid.txt');
    makeSureFileExists();
    try {
      final currentFile = await file.localFile;
      String contents = await currentFile.readAsString();
      savedData = NotesModelList.fromJson(jsonDecode(contents));
      readRemotelySavedLogs(context, uid);
      return contents;
    } catch (e) {
      debugPrint(e.toString());
      readRemotelySavedLogs(context, uid);
      return '';
    }
  }

  List<NotesModel> getLogs() => savedData.data;

  void makeSureFileExists() {
    file.appendString('');
  }

  void addLog(NotesModel noteToAdd, String uid) {
    if (noteToAdd != null) {
      savedData.add(noteToAdd);
      saveWithNewData(uid);
      debugPrint('Added log: ' + jsonEncode(savedData.data).toString());
    }
  }

  void removeLog(int index, String uid) {
    if (index != null && index < getLogs().length) {
      savedData.remove(index);
      saveWithNewData(uid);
      debugPrint('removed log: ' + jsonEncode(savedData.data).toString());
    }
  }

  void removeLogByNote(NotesModel note, String uid) {
    List<int> toRemove = [];
    if (note != null) {
      savedData.models.asMap().forEach((index, downloadedElement) {
        if (downloadedElement.date == note.date) {
          toRemove.add(index);
        }
      });
      for (int i = toRemove.length - 1; i >= 0; i--) {
        savedData.models.removeAt(toRemove[i]);
      }
      saveWithNewData(uid);
      debugPrint('removed log: ' + jsonEncode(savedData.data).toString());
    }
  }

  void updateLog(int index, NotesModel noteToAdd, String uid) {
    if (index != null && index < getLogs().length) {
      savedData.data[index] = noteToAdd;
      saveWithNewData(uid);
      debugPrint('Updated log: ' + jsonEncode(savedData.data).toString());
    }
  }

  void reWriteLogs(List<NotesModel> notesLogs, String uid) {
    if (notesLogs != null) {
      savedData.models = notesLogs;
      saveWithNewData(uid);
    }
  }

  void saveWithNewData(String uid) {
    file = DataFile('NoteLogs_$uid.txt');
    file.reWriteJson(savedData.toJson());
  }

  Future<void> logFirebase(NotesModelList data, String uid) async {
    FirebaseStorageController firebaseStorageController =
        FirebaseStorageController();
    debugPrint(
        'NoteLogs.logFirebase() Notes List logging to Firebase in progress');
    try {
      await firebaseStorageController.storeFile(
          tempLocalFileName: 'NoteLogs_' + uid,
          cloudFolderName: 'NoteLogsData',
          cloudFileName: uid,
          dataToSave: jsonEncode(data));
      debugPrint('Stored notes: ' + jsonEncode(data).toString());
      debugPrint('NoteLogs.logFirebase() Stored in ID ' + uid);
    } catch (e) {
      debugPrint('NoteLogs.logFirebase() ' + e.toString());
    }
  }

  Future<NotesModelList> readRemotelySavedLogs(
      BuildContext context, String uid) async {
    try {
      FirebaseStorageController firebaseStorageController =
          FirebaseStorageController();
      File downloadedFile = await firebaseStorageController.getFile(
          localFileName: 'NoteLogs_' + uid,
          cloudFolderName: 'NoteLogsData',
          cloudFileName: uid);
      String downloadedDataString = await downloadedFile.readAsString();
      // check and merge downloadedData with savedData
      NotesModelList downloadedData =
          NotesModelList.fromJson(jsonDecode(downloadedDataString));
      if (savedData.length == 0) {
        savedData = downloadedData;
      } else {
        List<int> toRemove = [];
        downloadedData.models.asMap().forEach((index, downloadedElement) {
          savedData.models.forEach((currentElement) {
            if (downloadedElement.date == currentElement.date) {
              toRemove.add(index);
            }
          });
        });
        for (int i = toRemove.length - 1; i >= 0; i--) {
          downloadedData.models.removeAt(toRemove[i]);
        }
        savedData.models.addAll(downloadedData.models);
      }
      saveWithNewData(uid);
      return downloadedData;
    } catch (e) {
      debugPrint('NoteLogs.readRemotelySavedLogs() error: ' + e.toString());
      return savedData;
    }
  }

  NotesModelList removeDeletedNoteFromCloud(
      NotesModelList remotedlySavedLogs, NotesModel noteToRemoveFromCloud) {
    List<int> toRemove = [];
    remotedlySavedLogs.models.asMap().forEach((index, downloadedElement) {
      if (downloadedElement.date == noteToRemoveFromCloud.date) {
        toRemove.add(index);
      }
    });
    for (int i = toRemove.length - 1; i >= 0; i--) {
      remotedlySavedLogs.models.removeAt(toRemove[i]);
    }
    return remotedlySavedLogs;
  }
}
