import 'package:Tremor_App/Backend/User/local_user_profile.dart';
import 'dart:convert';
import 'package:Tremor_App/Backend/Storage/firebase_storage_controller.dart';
import 'dart:async';
import 'package:Tremor_App/Backend/data_file.dart';
import 'package:flutter/cupertino.dart';
import 'package:Tremor_App/Resources/Exceptions.dart';

//todo tik prisijungus issavint localy firebase info, nes kitaip tuscia nureadina profile
class UserDataLog{
  LocalUserProfile profile;
  UserDataLog(this.profile);

  DataFile _file = DataFile('User.txt');

  Future<void> dataInit( String uid) async {
    //_file = DataFile('$uid.txt');
    String encodedData = await readLogs(uid);
    if(encodedData != null) {
      profile = LocalUserProfile.fromJson(jsonDecode(encodedData));
    } else{
      throw CantReadLocalUserData;
    }
  }

  Future<String> readLogs(String uid) async {
  //  _file = DataFile('$uid.txt');
    makeSureFileExists();
    final file = await _file.localFile;
    String contents = await file.readAsString();
    if(contents.length == 0){
      return null;
    } else{
      return contents;
    }
  }

  void makeSureFileExists(){
    _file.appendString('');
  }

  void deleteUserLocalFile(){
    _file.clean();
  }

  void saveUserData() async {
    saveLocally();
    saveFirebase();
  }

  Future<void> saveLocally() async {
    try {
      if (profile != null) {
        debugPrint('Saving user data: ' + jsonEncode(profile.toJson()));
        await _file.reWriteJson(profile.toJson());
      }
    } catch (e){
      debugPrint('UserDataLog.saveLocally() ' + e.toString());
    }
  }

  void saveFirebase() {
    if (profile != null) {
      FirebaseStorageController firebaseStorageController =
      FirebaseStorageController();
      firebaseStorageController.storeFile(
          tempLocalFileName: 'User', // galimai bus galima atstatyt i User
          cloudFolderName: 'Users',
          cloudFileName: profile.id,
          dataToSave: jsonEncode(profile.toJson()));
    }
  }
}
