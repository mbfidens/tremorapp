import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:path_provider/path_provider.dart';

class DataFile {
  String fileName;
  String directoryName = '';
  DataFile(this.fileName);

  bool checkIfNameIsCorrect(String name) => name != null;

  Future<String> get localPath async {
    final directory = await getApplicationDocumentsDirectory();
    String path = directory.path;
    await createDirectoryIfNeeded(path);

    return path + '/' + directoryName;
  }

  Future<void> createDirectoryIfNeeded(String path) async {
    if(fileName.contains(r'\') || fileName.contains('/')){
      fileName = fileName.replaceAll('/', r'\');
      List<String> nameWithPath = fileName.split(r'\');
      fileName = nameWithPath.last;
      nameWithPath.removeLast();
      directoryName = r'\' + nameWithPath.reduce((value, element) => value + r'\' + element);
      await Directory(path + directoryName).create(recursive: true);
    }
  }

  Future<File> get localFile async {
    if (checkIfNameIsCorrect(fileName)) {
      final path = await localPath;
      return File('$path' + r'\' + fileName);
    } else {
      debugPrint('DataFile.localFile() incorrect name');
      return null;
    }
  }

  Future<void> clean() async {
    try {
      final file = await localFile;
      await file.writeAsString('', mode: FileMode.write);
    } catch (e) {
      debugPrint('data_file.clean() ' + e.toString());
    }
  }

  Future<String> readString() async {
    try {
      final file = await localFile;
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      debugPrint('DataFile.readString() ' + e.toString());
      return null;
    }
  }

  Future<File> reWriteString(String data) async {
    try {
      checkIfStringDataIsCorrect(data);
      final file = await localFile;
      return file.writeAsString(data, mode: FileMode.write);
    } catch (e) {
      debugPrint('DataFile.reWriteString() ' + e.toString());
      return null;
    }
  }

  Future<File> appendString(String data) async {
    try {
      checkIfStringDataIsCorrect(data);
      final file = await localFile;
      return file.writeAsString(data, mode: FileMode.append);
    } catch (e) {
      debugPrint('DataFile.appendString() ' + e.toString());
      return null;
    }
  }

  Future<File> reWriteJson(Map<String, dynamic> data) async {
    try {
      checkIfJsonDataIsCorrect(data);
      String json = jsonEncode(data);
      return reWriteString(json);
    } catch (e) {
      debugPrint('DataFile.reWriteJson() ' + e.toString());
      return null;
    }
  }

  void checkIfStringDataIsCorrect(String data) {
    if (data == null) {
      throw Exception('DataFile() string data is null');
    }
  }

  void checkIfJsonDataIsCorrect(Map<String, dynamic> data) {
    if (data == null) {
      throw Exception('DataFile() json data is null');
    }
  }
}
