import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/Models/options_model.dart';
import 'package:Tremor_App/Backend/Logs/option_logs.dart';
import 'package:Tremor_App/Resources/CurrentDate.dart';
import 'package:collection/collection.dart';
import 'package:Tremor_App/Backend/Options/option_item.dart';

const double userBMIConst = 10000 / (184 * 184);
const int optionsLength = 7;
const int maxPoints = 100;
const int maxPointsPerItem = maxPoints ~/ (optionsLength - 1);

const int leftAfterRounding = (maxPoints % (optionsLength - 1));
/*Check if consistent with min and max!*/
const List<String> optionKeywords = <String>[
  "Exercise min. ",
  "Relaxations   ",
  "Sleep hours   ",
  "Healthy meals",
  "Cups of coffee",
  "Therapies",
  "Reflection"
];
const List<String> optionIcons = <String>[
  'lib/Assets/Images/sport.png',
  'lib/Assets/Images/flat_love.png',
  'lib/Assets/Images/flat_clock.png',
  'lib/Assets/Images/flat_salad.png',
  'lib/Assets/Images/flat_coffee.png',
  'lib/Assets/Images/vilim_ball.png',
  'lib/Assets/Images/heart.png'
];
const List<String> optionInfo = <String>[
  "  5 min. for ",
  " 1 time for  ",
  " 8 hours for ",
  " 1 meal for ",
  "  0 cups for ",
  " 1 therapy for ",
  "Reflection for"
];
List<int> initialValues = <int>[0, 0, 6, 0, 0, 0, 0];
const List<int> minValues = <int>[0, 0, 5, 0, 0, 0, 0];
const List<int> maxValues = <int>[30, 2, 8, 2, 1, 3, 1];
const List<int> sampleValues = <int>[5, 1, 1, 1, 1, 1, 1];
/*1- more better, -1 - less better, others - non linear or special*/
const List<int> positivityType = <int>[1, 1, 1, 1, -1, 1, 0];

class OptionsManager {
  OptionLogs optionLogs;
  Function({bool newDataDownloaded}) notifyParent;
  OptionsManager(this.optionLogs, this.notifyParent);
  factory OptionsManager.empty(Function({bool newDataDownloaded}) refresh) => OptionsManager(OptionLogs(refresh), refresh);

  OptionsModel todayOptions;
  CurrentDate currentDate = CurrentDate();
  List<OptionItem> todayOptionItems = generateItems();

  static List<OptionItem> generateItems() => List<OptionItem>.generate(
      optionsLength,
      (i) => OptionItem(
          index: i,
          name: optionKeywords[i],
          icon: optionIcons[i],
          info: optionInfo[i],
          value: initialValues[i],
          min: minValues[i],
          max: maxValues[i],
          sample: sampleValues[i],
          isPositive: positivityType[i],
          specialCalculator: (int x) => 0));

  void regenerateItems() {
    todayOptionItems = generateItems();
  }

  void takeDataFromOptionsList() {
    OptionsModel todayData = optionLogs.getList != null
        ? optionLogs.getList.firstWhere((element) => element.date.difference(DateTime.now()).inDays == 0, orElse: () => null)
        : null;
    if (todayData != null) {
      initialValues = todayData.options;
      regenerateItems();
    }
    updateTodayOptions();
  }

  void updateTodayOptions() {
    todayOptions = OptionsModel(
        currentDate.getDate(), getResult(), List<int>.generate(optionsLength, (index) => getValue(index)));
  }

  Future<void> saveData(String id) async {
    OptionsModel emptyModel = OptionsModel(null, 0, [0, 0, 6, 0, 0, 0, 0]);
    if (todayOptions != null && !(todayOptions.options == emptyModel.options)) {
      await optionLogs.logOption(todayOptions, id);
    }
  }

  int getValue(int index) {
    return todayOptionItems[index].value;
  }

  String getOptionKeyword(int index) {
    return optionKeywords[index];
  }

  String getOptionInfo(int index) {
    return optionInfo[index];
  }

  void setValue(int value, int index) {
    todayOptionItems[index].value = value;
  }

  void setName(String name, int index) {
    todayOptionItems[index].name = name;
  }

  void setInfo(String info, int index) {
    todayOptionItems[index].info = info;
  }

  int getResult() {
    int result = 0;
    for (int i = 0; i < optionsLength; i++) {
      result += getPoints(i);
    }
    int totalResult = result + leftAfterRounding;
    return totalResult;
  }

  String getInfo(int index) {
    return todayOptionItems[index].info;
  }

  int getLengthOneOption(int index) {
    return (todayOptionItems[index].max - todayOptionItems[index].min) ~/ todayOptionItems[index].sample;
  }

  int getType(int index) {
    return todayOptionItems[index].isPositive;
  }

  bool isMax(int index) {
    return todayOptionItems[index].value == todayOptionItems[index].max;
  }

  bool isMin(int index) {
    return todayOptionItems[index].value == todayOptionItems[index].min;
  }

  int getPoints(int index) {
    int points;
    OptionItem item = todayOptionItems[index];

    if (todayOptionItems[index].isPositive == -1) {
      points = item.value == 0 ? maxPointsPerItem : maxPointsPerItem - maxPointsPerItem * item.value ~/ item.max;
    } else if (todayOptionItems[index].isPositive == 1) {
      points = item.value == item.max ? maxPointsPerItem : maxPointsPerItem * (item.value - item.min) ~/ (item.max - item.min);
    } else {
      points = 0;
    }
    return points;
  }

  String getOptionInfoBar(int index) {
    return '';
  }

  void addPoints(int index) {
    OptionItem item = todayOptionItems[index];
    item.value = item.value >= item.max ? item.max : item.sample + item.value;
    updateTodayOptions();
  }

  void minusPoints(int index) {
    OptionItem item = todayOptionItems[index];
    item.value = item.value <= item.min ? item.min : item.value - item.sample;
    updateTodayOptions();
  }

  String getIcon(int index) {
    return todayOptionItems[index].icon;
  }

  String getName(int index) {
    return todayOptionItems[index].name;
  }

  int getMax() {
    return maxPointsPerItem;
  }
}
