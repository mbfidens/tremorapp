import 'package:flutter/material.dart';

class OptionItem {
    final int index;
    String name;
    final String icon;
    int value;
    String info;
    final int min;
    final int max;
    final int sample;
    final int isPositive;
    final Function(int) specialCalculator;

    OptionItem(
        {@required this.index,
            @required this.name,
            @required this.icon,
            @required this.value,
            @required this.info,
            @required this.min,
            @required this.max,
            @required this.sample,
            @required this.isPositive,
            this.specialCalculator});
}