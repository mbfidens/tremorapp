/*[BluetoothDevice{
id: D8:80:39:FE:81:78,
 name: RN4870-8178,
 type: BluetoothDeviceType.le,
  isDiscoveringServices:  false,
  _services: []]//

  class BluetoothDevice {
  final DeviceIdentifier id;
  finae;
  final BluetoothDeviceType type;

*/


import 'package:flutter_blue/flutter_blue.dart';

class SavedBluetoothDevicesDataList {
  final List<SavedBluetoothDevicesData> savedData;
  SavedBluetoothDevicesDataList(this.savedData);

  SavedBluetoothDevicesDataList.fromJson(List<dynamic> usersJson) :
        savedData = usersJson.map((savedData) => savedData.fromJson(savedData)).toList();
}

class SavedBluetoothDevicesData {

  DeviceIdentifier localid;
  String name;
  BluetoothDeviceType type;
  SavedBluetoothDevicesData({this.localid, this.name, this.type});


  SavedBluetoothDevicesData.fromJson(Map<String, dynamic> json)
      : localid = DeviceIdentifier(json['id']),
        name = json['name'],
        type = BluetoothDeviceType.values[json['type']];


  Map<String, dynamic> toJson() => {
    'id': localid.id.toString(),
    'name': name.toString(),
    'type': type.index,
  };

}