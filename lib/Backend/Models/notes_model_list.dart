import 'package:Tremor_App/Backend/Models/notes_model.dart';
import 'dart:convert';

class NotesModelList {
  List<NotesModel> models;

  NotesModelList(this.models);

  factory NotesModelList.empty() {
    return NotesModelList(<NotesModel>[]);
  }

  int get length => models != null ? models.length : 0;
  List<NotesModel> get data => models;

  void add(NotesModel newData) {
    models.insert(0, newData);
  }

  void remove(int index) {
    models.removeAt(index);
  }

  NotesModelList.fromJson(Map<String, dynamic> m) {
    models = convertToReadableFormat(m['data'].cast<String>());
  }

  Map<String, dynamic> toJson() => {
        'data': convertToSavableFormat(),
      };

  List<String> convertToSavableFormat() => models.map((e) => json.encode(e.toJson())).toList();

  List<NotesModel> convertToReadableFormat(List<String> jsonLines) =>
      jsonLines.map((e) => NotesModel.fromJson(jsonDecode(e))).toList();

  //TODO(Andrius) : add merge with cloud function
}
