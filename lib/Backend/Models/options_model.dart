import 'package:flutter/foundation.dart';

class OptionsModel {
  /*
    [date],[sum],[exercise],[relaxation],[sleep],[mass],[coffee]...[*]
  */
  DateTime date;
  int result;
  List<int> options;
  OptionsModel(this.date, this.result, this.options);

  @override
  bool operator ==(otherModel) =>
      this.result == otherModel.result && this.date == otherModel.date && listEquals(this.options, otherModel.options);

  @override
  int get hashCode => date.hashCode ^ result.hashCode ^ options.hashCode;

  OptionsModel.fromJson(Map<String, dynamic> m) {
    date = DateTime.parse(m['date']);
    result = m['result'];
    options = m['options'].cast<int>();
  }

  Map<String, dynamic> toJson() => {
        'date': date.toIso8601String(),
        'result': result,
        'options': options,
      };
}
