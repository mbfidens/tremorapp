import 'package:flutter/cupertino.dart';
import 'package:Tremor_App/Backend/Logs/accelerometer_logs.dart';
import 'package:Tremor_App/Backend/Logs/gyroscope_logs.dart';

enum SensorTypes{
  Accelerometer,
  Gyroscope,
  UserAccelerometer
}

class SensorModel {
  DateTime dateTime;
  SensorTypes sensorType;

  List<List<String>> dataXYZT = <List<String>>[];

  SensorModel(this.dateTime, this.sensorType);

  int samplesToSave = 4096;

  void add(BuildContext context, List<double> newValuesXYZ, int time) {
    dataXYZT.add(<String>[
      newValuesXYZ[0].toStringAsFixed(2),
      newValuesXYZ[1].toStringAsFixed(2),
      newValuesXYZ[2].toStringAsFixed(2),
      time.toString()]);
    saveIfFullList(context);
  }

  void saveIfFullList(BuildContext context) {
    if (dataXYZT.length >= samplesToSave) {
      dateTime = DateTime.now();
      saveAndClearData(context);
    }
  }

  void saveAndClearData(BuildContext context){
    dynamic logs;
    if(sensorType == SensorTypes.Accelerometer) {
      logs = AccelerometerLogs(convertToCSV(), getName());
    } else if(sensorType == SensorTypes.Gyroscope){
      logs = GyroscopeLogs(convertToCSV(), getName());
    }
    logs.saveData(context);
    clear();
  }

  void clear(){
    dataXYZT = <List<String>>[];
  }

  String convertToCSV(){
    String result = '';
    dataXYZT.forEach((element) {
      result = result + element[0] + ' ' + element[1] + ' ' +element[2] + ' ' + element[3] + ' ';
    });
    return result;
  }

  String getName() =>
      (dateTime.toIso8601String().replaceAll('.', '-').replaceAll(':', '-') + '.txt') ??
      ('#nulled#' + DateTime.now().toIso8601String().replaceAll('.', '-').replaceAll(':', '-') + '.txt');
}
