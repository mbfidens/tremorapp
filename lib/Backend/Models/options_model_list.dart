import 'package:Tremor_App/Backend/Models/options_model.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

class OptionsModelList {
  List<OptionsModel> models;

  OptionsModelList(this.models);

  factory OptionsModelList.empty() {
    return OptionsModelList(<OptionsModel>[]);
  }

  List<OptionsModel> get data => models;
  int get length => models != null ? models.length : 0;

  bool checkIfSameDay(OptionsModel firstModel, OptionsModel secondModel) =>
      firstModel.date.year == secondModel.date.year &&
      firstModel.date.month == secondModel.date.month &&
      firstModel.date.day == secondModel.date.day;

  OptionsModelList.fromJson(Map<String, dynamic> m) {
    models = convertToReadableFormat(m['data'].cast<String>());
  }

  Map<String, dynamic> toJson() => {
        'data': convertToSavableFormat(),
      };

  List<String> convertToSavableFormat() => models.map((e) => json.encode(e.toJson())).toList();

  List<OptionsModel> convertToReadableFormat(List<String> jsonLines) =>
      jsonLines.map((e) => OptionsModel.fromJson(jsonDecode(e))).toList();

  void merge(OptionsModelList listToMerge) {
    /*Merging in favor of new*/
    sort();
    listToMerge.sort();
    if (listToMerge.models != null) {
      listToMerge.models.forEach((element) {
        add(element);
      });
    }
  }

  void sort() {
    List<OptionsModel> sortedList = List.from(models);
    sortedList.sort((a, b) => a.date.isAfter(b.date) ? -1 : 1);
    models = List.from(sortedList);
  }

  int getIndexIfDayDuplicates(OptionsModel newModel) {
    int existingIndex = 0;
    bool foundDuplicate = false;

    if (models == null) {
      return null;
    } else {
      for (int i = 0; i < length; i = i + 1) {
        if (checkIfSameDay(models.elementAt(i), newModel)) {
          foundDuplicate = true;
          break;
        } else {
          existingIndex++;
        }
      }
      if (foundDuplicate) {
        return existingIndex;
      } else {
        return null;
      }
    }
  }

  void addNewModel(OptionsModel newModel) {
    int index = 0;
    if (models.length > 0) {
      for(int i = 0; i < models.length; i = i + 1) {
        if(models[i].date.isAfter(newModel.date)) {
          index = index + 1;
        } else{
          break;
        }
      }
      models.insert(index, newModel);
    } else {
      models.add(newModel);
    }
  }

  void add(OptionsModel newModel) {
    if (newModel != null) {
      int oldIndex = getIndexIfDayDuplicates(newModel);

      if (oldIndex == null) {
        addNewModel(newModel);
      } else {
        models.isNotEmpty ? models[oldIndex] = newModel : models.add(newModel);
      }

      final removeDuplicates = models.map((e) => e.date).toSet();
      models.retainWhere((x) => removeDuplicates.remove(x.date));
    }
  }

  void remove(int index) {
    models.removeAt(index);
  }

  void cutExpiredData(int daysToSave) {
    if (length >= daysToSave) {
      models = models.sublist(length - 1 - daysToSave, daysToSave - 1);
    }
  }
}
