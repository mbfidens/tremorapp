import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Validator {
  bool isName(String hint) => hint.toUpperCase() == 'NAME' || hint.toUpperCase() == 'SURNAME';
  bool isEmail(String hint) => hint.toUpperCase() == 'EMAIL' || hint.toUpperCase() == 'REPEAT EMAIL';
  bool isPassword(String hint) => hint.toUpperCase() == 'PASSWORD' || hint.toUpperCase() == 'REPEAT PASSWORD';
  bool isAge(String hint) => hint.toUpperCase() == 'AGE';
  bool isHeight(String hint) => hint.toUpperCase() == 'WEIGHT';
  bool isWeight(String hint) => hint.toUpperCase() == 'HEIGHT';

  TextInputType getInputType(String hint) =>
      hint.toUpperCase() == 'AGE' ? TextInputType.number : isEmail(hint) ? TextInputType.emailAddress : TextInputType.text;

  TextCapitalization getInputCapitalization(String hint) => isName(hint) ? TextCapitalization.words : TextCapitalization.none;

  FormFieldValidator getValidator(BuildContext context, String hint) {
    if (isEmail(hint)) {
      return getEmailValidator(context);
    } else if (isName(hint)) {
      return getNameValidator(context);
    } else if (isPassword(hint)) {
      return getPasswordValidator(context);
    } else if (isAge(hint)) {
      return getAgeValidator(context);
    } else if (isHeight(hint)) {
      return getHeightValidator(context);
    } else if (isWeight(hint)) {
      return getWeightValidator(context);
    } else {
      return null;
    }
  }

  FormFieldValidator getPasswordValidator(BuildContext context) => (value) {
        if (value.isEmpty) {
          return AppLocalizations.of(context).translate("Please enter information");
        } else if (checkIfPasswordIsCorrect(value)) {
          return null;
        } else {
          return AppLocalizations.of(context).translate("Password must have 6 symbols and contain number");
        }
      };

  FormFieldValidator getNameValidator(BuildContext context) => (value) {
        if (value.isEmpty) {
          return AppLocalizations.of(context).translate("Please enter information");
        } else if (checkIfNameIsCorrect(value)) {
          return null;
        } else {
          return AppLocalizations.of(context).translate("Invalid name");
        }
      };

  FormFieldValidator getEmailValidator(BuildContext context) => (value) {
        if (value.isEmpty) {
          return AppLocalizations.of(context).translate("Please enter information");
        } else if (checkIfEmailIsCorrect(value)) {
          return null;
        } else {
          return AppLocalizations.of(context).translate("Wrong email format");
        }
      };

  FormFieldValidator getAgeValidator(BuildContext context) => (value) { // apple: age must be optional
    if (value.isEmpty || value == null) {
      return null;
    } else if (checkIfAgeIsCorrect(value)) {
          return null;
      } else {
          return AppLocalizations.of(context).translate('Check entered data');
      }
  };

  FormFieldValidator getHeightValidator(BuildContext context) => (value) {
      if (value.isEmpty) {
          return AppLocalizations.of(context).translate("Please enter height");
      } else if (checkIfHeightIsCorrect(value)) {
          return null;
      } else {
          return AppLocalizations.of(context).translate('Check entered data');
      }
  };

  FormFieldValidator getWeightValidator(BuildContext context) => (value) {
      if (value.isEmpty) {
          return AppLocalizations.of(context).translate("Please enter weight");
      } else if (checkIfWeightIsCorrect(value)) {
          return null;
      } else {
          return AppLocalizations.of(context).translate('Check entered data');
      }
  };

  bool checkIfEmailIsCorrect(String email) {
    final Pattern bannedSymbolPattern = r'[[\]/\\|=+)(*]';
    RegExp regex = new RegExp(bannedSymbolPattern);
    if (!regex.hasMatch(email) && EmailValidator.validate(email))
      return true;
    else
      return false;
  }

  bool checkIfNameIsCorrect(String name) {
    final Pattern bannedSymbolPattern = r'[!@#<>?":_`~;[\]/\\|=+)(*&^%0-9 £-]';
    RegExp regex = new RegExp(bannedSymbolPattern);
    if (!regex.hasMatch(name) && name.length > 2 && name.length < 20)
      return true;
    else
      return false;
  }

  bool checkIfSurnameIsCorrect(String surname) {
    final Pattern bannedSymbolPattern = r'[!@#<>?":_`~;[\]/\\|=+)(*&^%0-9 £-]';
    RegExp regex = new RegExp(bannedSymbolPattern);
    if (!regex.hasMatch(surname) && surname.length > 2 && surname.length < 20)
      return true;
    else
      return false;
  }

  bool checkIfAgeIsCorrect(String age) {
    final Pattern bannedSymbolPattern = r'^[0-9]+$';
    RegExp regex = new RegExp(bannedSymbolPattern);
    debugPrint('Age: ' + age);
    if (regex.hasMatch(age) ) {
      if(int.parse(age) > 0 && int.parse(age) < 120){
      return true;}
    else return false;
    }
    else
      return false;
  }

  bool checkIfHeightIsCorrect(String height) {
    final Pattern pattern = r'^[0-9]+$';
    RegExp regex = new RegExp(pattern);
    if (regex.hasMatch(height) ) {
      if(int.parse(height) > 63 && int.parse(height) < 230){
        return true;}
      else return false;
    }
    else
      return false;
  }

  bool checkIfWeightIsCorrect(String weigth) {
    final Pattern pattern = r'^[0-9]+$';
    RegExp regex = new RegExp(pattern);
    if (regex.hasMatch(weigth) ) {
      if(int.parse(weigth) > 3 && int.parse(weigth) < 3000){
        return true;}
      else return false;
    }
    else
      return false;
  }

  bool checkIfPasswordIsCorrect(String password) {
    if (password.length > 5)
      return true;
    else
      return false;
  }
}
