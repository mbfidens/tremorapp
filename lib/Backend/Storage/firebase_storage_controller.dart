import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:io';
import 'package:Tremor_App/Backend/data_file.dart';

class FirebaseStorageController {

  Future<void> storeFile(
      {@required String tempLocalFileName,
      @required String cloudFolderName,
      @required String cloudFileName,
      @required String dataToSave}) async {
    try {
      DataFile localFile = DataFile(tempLocalFileName);
      await localFile.reWriteString(dataToSave);
      uploadFile(await localFile.localFile, cloudFolderName, cloudFileName);
    } catch (e) {
      debugPrint('FirebaseStorageController.storeFile:' + e.toString());
    }
  }

  Future<File> getFile({@required String localFileName, @required String cloudFolderName, @required String cloudFileName}) async {
    try {
      if(await checkIfInternetIsAvailable()) {
        DataFile localFile = DataFile(localFileName);
        localFile.reWriteString('');
        await downloadFile(await localFile.localFile, cloudFolderName, cloudFileName);
        return localFile.localFile;
      } else{
        return null;
      }
    } catch (e) {
      debugPrint('FirebaseStorageController.getFile() ' + e.toString());
      return null;
    }
  }

  Future<File> downloadFile(File file, String folderName, String filename) async {
    Reference ref = FirebaseStorage.instance.ref().child(folderName + '/' + filename);
    try {
      if(await checkIfInternetIsAvailable()) {
        final String url = await ref.getDownloadURL();
        DownloadTask task = ref.writeToFile(file);

        int byteCount = (await task).totalBytes;
        String name = ref.name;
        String path = ref.fullPath;
        return file;
      } else{
        return null;
      }
    } on PlatformException catch (e) {
      debugPrint('FirebaseStorageController.downloadFile() ' + e.toString());
      return null;
    } catch (e) {
      debugPrint('FirebaseStorageController.downloadFile() ' + e.toString());
      return null;
    }
  }

  Future<bool> checkIfInternetIsAvailable () async {
    final result = await InternetAddress.lookup('example.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    } else{
      return false;
    }
  }

  Future<UploadTask> uploadFile(File file, String folderName, String filename) async {
    try {
      Reference storageReference = FirebaseStorage.instance.ref().child(folderName + '/' + filename);

      final UploadTask uploadTask = storageReference.putFile(file);
      final TaskSnapshot downloadUrl = (await uploadTask);
      final String url = (await downloadUrl.ref.getDownloadURL());
      debugPrint("URL is $url");
      return Future.value(uploadTask);
    } catch (e) {
      debugPrint('FirebaseStorageController.uploadFile() ' + e.toString());
      return null;
    }
  }
}
