import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/Models/notes_model.dart';
import 'package:Tremor_App/Backend/Logs/note_logs.dart';
import 'dart:convert';

class NotesDataController {
  final Function() notifyParent;
  String uid;
  NotesDataController(this.notifyParent, this.uid);

  NoteLogs logsController = NoteLogs();
  List<NotesModel> notes = <NotesModel>[];
  bool newDataAvailable = true;
  bool newDataSinceLastRead = false;
  bool isInitialized = false;

  NotesDataController.init(this.notifyParent, BuildContext context) {
    logsController.dataInit(context, uid);
  }

  void initializeOrCheckUpdateStatus() {
    if (newDataAvailable) {
      newDataAvailable = false;
      updateNotesList();
    }
  }

  void updateNotesList() async {
    notes = logsController.getLogs();
    if (notes != null) {
      notifyParent();
      notes.last != null && notes.isNotEmpty
          ? debugPrint("Notes initialized - last - " + jsonEncode(notes.last.toJson()))
          : debugPrint("Notes updated: null or empty");
    }
  }

  List<NotesModel> readNotesFromFile() => logsController.getLogs();

  int getLength() {
    debugPrint('Returned notes list length: ' + notes.length.toString() + ' is null? (' + (notes == null).toString() + ')');
    return notes != null ? notes.length : 0;
  }

  bool checkIfNewDataSinceLastReadAvailable() => newDataSinceLastRead;

  NotesModel getNote(int index) {
    newDataSinceLastRead = false;
    if (index < getLength() && getLength() > 0) {
      debugPrint('Returned note: ' + jsonEncode(notes[index].toJson()));
      return getNote(index);
    } else {
      debugPrint('Returned note: empty');
      return NotesModel.empty();
    }
  }

  void removeNote(int index) {
    notes.removeAt(index);
    saveNotes(uid);
  }

  void updateNote(NotesModel editedNote, int index) {
    notes[index] = editedNote;
    saveNotes(uid);
  }

  void addNote(NotesModel newNote) {
    if (newNote != null && !newNote.isEmpty) {
      notes.add(newNote);
      saveNotes(uid);
    }
  }

  void saveNotes( String uid) {
    logsController.reWriteLogs(notes, uid);
    notifyAboutNewAvailableData();
  }

  void notifyAboutNewAvailableData() {
    newDataAvailable = true;
    newDataSinceLastRead = true;
    notifyParent();
  }
}
