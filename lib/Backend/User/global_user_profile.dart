import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/User/local_user_profile.dart';

class GlobalUserProfile {
  LocalUserProfile profile;
  GlobalUserProfile({this.profile});

  factory GlobalUserProfile.loadFromMemory(){
    //TODO(Edvinas):implement user data management
    return GlobalUserProfile();
  }

  void clear() {
    profile = null;
  }

  String get name => isSigned && profile.name != null
      ? profile.name
      : null;

  String get surname => isSigned && profile.surname != null
      ? profile.surname
      : null;

  bool get isSigned => profile != null;

  Widget get avatar {
    return profile != null && profile.imageUrl != null && profile.imageUrl.isNotEmpty
        ? CircleAvatar(
            backgroundImage: NetworkImage(
              profile.imageUrl ?? '',
            ),
            radius: 50,
            backgroundColor: Colors.transparent,
            onBackgroundImageError: (dynamic, stackTrace) {
              debugPrint('Background image error:' + stackTrace.toString());
            },
          )
        : null;
  }
}
