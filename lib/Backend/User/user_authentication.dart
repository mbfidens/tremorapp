import 'package:Tremor_App/Backend/Storage/firebase_storage_controller.dart';
import 'dart:convert';
import 'package:Tremor_App/Backend/User/local_user_profile.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'dart:async';
import 'dart:io';
import 'package:Tremor_App/Backend/Logs/user_data_log.dart';
import 'package:Tremor_App/Resources/Exceptions.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:http/http.dart' as http;
import 'package:the_apple_sign_in/the_apple_sign_in.dart';

enum SignInTypes { Google, Email, AppleID }

class UserAuthentication {
  final Function() notifyParent;

  UserAuthentication(this.notifyParent);

  GoogleSignIn googleSignIn = GoogleSignIn();
  FirebaseAuth auth = FirebaseAuth.instance;
  User user;
  LocalUserProfile userProfile;

  void signOut(BuildContext context) async {
    //TODO(): Save user data
    //TODO(): Add confirmation dialog
    try {
      //TODO(): fix duality
      await signOutData(context).whenComplete(() => auth.signOut().whenComplete(/*(value) => googleSignIn.signOut().then(*/
          () => {
                googleSignIn.isSignedIn().then((value) {
                  if (value == true) {
                    googleSignIn.signOut();
                  }
                }),
                userProfile = null,
                //MyStatefulWidget.of(context).setIdForCloud(null),
                notifyParent()
              }) /*)*/);
      debugPrint('UserAuthentication.signOutData() Signing out in progress');
      /* await auth.signOut().then((value) => googleSignIn.signOut().then(
          (value) => {
                userProfile = null,
                MyStatefulWidget.of(context).setIdForCloud(null),
          notifyParent()
              }));*/
      //await googleSignIn.signOut();
      // userProfile = null;
      // MyStatefulWidget.of(context).setIdForCloud(null);
      debugPrint('UserAuthentication.signOutData() Signed out');
      // notifyParent();
    } catch (e) {
      debugPrint('Error while Signing out: ' + e.toString());
      //TODO():log error
    }
  }

  Future<void> signOutData(BuildContext context) async {
    MyStatefulWidget.of(context).setIdForCloud(user.uid);
    try {
      await MyStatefulWidget.of(context).options.saveData(user.uid);
    } catch (e) {
      debugPrint('UserAuthentication.signOutData() ' + e.toString());
    }
  }

  Future<bool> trySignInWithPreviousData(BuildContext context) async {
    authenticateUser(context).whenComplete(() {
      return Future.value(user != null);
    });
    return false;
  }

  Future<User> authenticateUser(BuildContext context) async {
    try {
      user = auth.currentUser;
      if (user != null) {
        // debugPrint('Authenticated user: ' + user.displayName);
        setUserData(context);
      }
      return user;
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }

  bool isSignedInGoogle() {
    bool isLoggedIn = false;
    googleSignIn.isSignedIn().then((value) {
      isLoggedIn = value;
    });
    return isLoggedIn;
  }

  Future<void> signInWithGoogle(BuildContext context) async {
    try {
      final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
      final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      user = (await auth.signInWithCredential(credential)).user;
      setUserData(context);
      notifyParent();
      print('xxxxxxxxxxxxxxxxx\n/n');
      print(user);
      print('xxxxxxxxxxxxxxxxx\n/n');
    } catch (e) {
      debugPrint('UserAuthentication.signInWithGoogle() ' + e.toString());
    }
  }

  Future<void> signInWithEmailAndPassword(BuildContext context, {@required String email, @required String password}) async {
    try {
      user = (await auth.signInWithEmailAndPassword(
        email: email.trim(),
        password: password,
      ))
          .user;
      setUserData(context);
      notifyParent();
    } catch (e) {
      debugPrint('UserAuthentication.signInWithEmailAndPassword() ' + e.toString());
      throw LoginException(e.toString());
    }
  }

  Future<void> registerWithEmailAndPassword(BuildContext context, {@required String email, @required String password}) async {
    try {
      user = (await auth.signInWithEmailAndPassword(
        email: email.trim(),
        password: password,
      ))
          .user;
      setUserData(context);
      notifyParent();
    } catch (e) {
      debugPrint('Error while Signing in with email: ' + e.toString());
      throw LoginException(e.toString());
    }
  }


  Future<User> signInWithApple(BuildContext context, {List<Scope> scopes = const []}) async {
    final result = await TheAppleSignIn.performRequests(
        [AppleIdRequest(requestedScopes: scopes)]);
    switch (result.status) {
      case AuthorizationStatus.authorized:
        final appleIdCredential = result.credential;
        final oAuthProvider = OAuthProvider('apple.com');
        final credential = oAuthProvider.credential(
          idToken: String.fromCharCodes(appleIdCredential.identityToken),
          accessToken:
          String.fromCharCodes(appleIdCredential.authorizationCode),
        );
        final userCredential =
        await auth.signInWithCredential(credential);
        user = userCredential.user;
       /* if (scopes.contains(Scope.fullName)) {
          final fullName = appleIdCredential.fullName;
          if (fullName != null &&
              fullName.givenName != null &&
              fullName.familyName != null) {
            final displayName = '${fullName.givenName} ${fullName.familyName}';
            await firebaseUser.updateDisplayName(displayName);
          }
        }*/

        setUserData(context);

        return user;
      case AuthorizationStatus.error:
        throw PlatformException(
          code: 'ERROR_AUTHORIZATION_DENIED',
          message: result.error.toString(),
        );

      case AuthorizationStatus.cancelled:
        throw PlatformException(
          code: 'ERROR_ABORTED_BY_USER',
          message: 'Sign in aborted by user',
        );
      default:
        throw UnimplementedError();
    }
  }

  Future<void> setUserData(BuildContext context) async {
    setPrimaryData(context);
    getUserData();
  }

  //Impossible to sign in without this info
  void setPrimaryData(BuildContext context) {
    userProfile = LocalUserProfile(
        name: user.displayName != null ? user.displayName.split(' ')[0] : null,
        surname: user.displayName != null ? user.displayName.split(' ')[1] : null,
        id: user.uid,
        email: user.email != null ? user.email : null,
        imageUrl: user.photoURL ?? null);
    if (MyStatefulWidget.of(context) != null) {
      MyStatefulWidget.of(context).setIdForCloud(user.uid);
    } else {
      Future.delayed(Duration(seconds: 5), () {
        if (MyStatefulWidget.of(context) != null) {
          MyStatefulWidget.of(context).setIdForCloud(user.uid);
        }
      });
    }
  }

  Future<void> getUserData() async {
    UserDataLog logger = UserDataLog(userProfile);
    try {
      await logger.dataInit(user.uid);
      debugPrint('UserAuthentication.getUserData() Local user data read successfully: ' + userProfile.name);
      checkIfIdMatch(logger);
    } on CantReadLocalUserData catch (e) {
      debugPrint('**UserAuthentication.getUserData()' + e.message);
    } on UserDataLocallyDoesNotExist catch (e) {
      debugPrint('**UserAuthentication.getUserData() User Data Locally Does Not Exist: ' + e.toString());
      getUserDataFromFirebase(logger);
    } catch (e) {
      debugPrint('**UserAuthentication.getUserData() Error while reading local user data: ' + e.toString());
      //Somehow cant find difference between simple e and CantReadLocalUserData;therefore these lines are added
      if (e == CantReadLocalUserData) {
        await getUserDataFromFirebase(logger);
      }
    }
  }

  void checkIfIdMatch(UserDataLog logger) {
    if (!logger.profile.isEmpty && logger.profile.id != null && logger.profile.id == user.uid) {
      userProfile = logger.profile;
    } else {
      logger.profile = userProfile;
      logger.saveLocally();
      throw UserDataLocallyDoesNotExist;
    }
  }

  Future<void> getUserDataFromFirebase(UserDataLog logger) async {
    try {
      FirebaseStorageController firebaseStorageController = FirebaseStorageController();
      File userFile =
          await firebaseStorageController.getFile(localFileName: 'User', cloudFolderName: 'Users', cloudFileName: user.uid);
      userProfile = LocalUserProfile.fromJson(jsonDecode(await userFile.readAsString()));
      logger.profile = userProfile;
      logger.saveLocally();
    } catch (e) {
      //TODO(Edvinas): detect when internet is broken vs file does not exist
      debugPrint('**UserAuthentication.getUserDataFromFirebase() ' + e.toString());
      logger.profile = userProfile;
      logger.saveFirebase();
    }
  }
}
