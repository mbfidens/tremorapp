class LocalUserProfile {
  String email;
  String password;
  String name;
  String surname;
  int age;
  int weight;
  int height;
  String residency;
  String condition;
  String imageUrl;
  String registrationDate;
  String id;
  String sn;
  String version;

  LocalUserProfile(
      {this.email,
      this.password,
      this.name,
      this.surname,
      this.age,
      this.weight,
      this.height,
      this.residency,
      this.condition,
      this.imageUrl,
      this.registrationDate,
      this.id,
      this.sn,
      this.version});

  LocalUserProfile.fromJson(Map<String, dynamic> m) {
    email = m['email'];
    password = m['password'];
    name = m['name'];
    surname = m['surname'];
    age = m['age'];
    weight = m['weight'];
    height = m['height'];
    residency = m['residency'];
    condition = m['condition'];
    imageUrl = m['url'];
    registrationDate = m['registrationDate'];
    id = m['id'];
    sn = m['sn'];
    version = m['version'];
  }

  Map<String, dynamic> toJson() => {
        'email': email,
        'password': password,
        'name': name,
        'surname': surname,
        'age': age,
        'weight': weight,
        'height': height,
        'residency': residency,
        'condition': condition,
        'url': imageUrl,
        'registrationDate': registrationDate,
        'id' : id,
        'sn' : sn,
        'version' : version
      };

  bool get isEmpty => name == null;
}
//TODO(Edvinas): add device ID to profile