
import 'package:Tremor_App/Backend/app_localization.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter/rendering.dart';

import 'package:rate_my_app/rate_my_app.dart';


class RateMyAppReminder {

  RateMyApp rateMyApp = RateMyApp(
    preferencesPrefix: 'rateMyApp_',
    minDays: 5,
    // Show rate popup on first day of install.
    minLaunches: 10,
    // Show rate popup after 5 launches of app after minDays is passed.
    remindDays: 5,
    remindLaunches: 10,
  );

  rateMyAppCheck(context, bool mounted) async {
    await rateMyApp.init();
    if (mounted && rateMyApp.shouldOpenDialog) {
      rateMyApp.showRateDialog(
        context,
        title: AppLocalizations.of(context).translate("Rate this app"),
        // The dialog title.
        message:
        AppLocalizations.of(context).translate("App rate dialog message"),
        rateButton: AppLocalizations.of(context).translate("RATE"),
        // The dialog "rate" button text.
        noButton: AppLocalizations.of(context).translate("NO THANKS"),
        // The dialog "no" button text.
        laterButton: AppLocalizations.of(context).translate("MAYBE LATER"),
        // The dialog "later" button text.
        listener: (button) {
          // The button click listener (useful if you want to cancel the click event).
          switch (button) {
            case RateMyAppDialogButton.rate:
              print('Clicked on "Rate".');
              break;
            case RateMyAppDialogButton.later:
              print('Clicked on "Later".');
              break;
            case RateMyAppDialogButton.no:
              print('Clicked on "No".');
              break;
          }
          return true; // Return false if you want to cancel the click event.
        },
        // ignoreNativeDialog: Platform.isAndroid, // Set to false if you want to show the Apple's native app rating dialog on iOS or Google's native app rating dialog (depends on the current Platform).
        dialogStyle: DialogStyle(
          titleAlign: TextAlign.center,
          messageAlign: TextAlign.center,
          messagePadding: EdgeInsets.only(bottom: 20.0),
          dialogShape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0)),
        ),
        // Custom dialog styles.
        onDismissed: () =>
            rateMyApp.callEvent(RateMyAppEventType.laterButtonPressed),
      );
    }
  }
}