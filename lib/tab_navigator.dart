import 'package:Tremor_App/Backend/User/global_user_profile.dart';
import 'package:Tremor_App/Backend/app_localization.dart';
import 'package:Tremor_App/Backend/rate_my_app_reminder.dart';
import 'package:app_tracking_transparency/app_tracking_transparency.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:Tremor_App/Screens/home_screen.dart';
import 'package:Tremor_App/Screens/history_screen.dart';
import 'package:Tremor_App/Backend/User/user_authentication.dart';
import 'package:Tremor_App/Screens/relaxation_screen.dart';
import 'package:Tremor_App/Dialogs/log_in_dialog.dart';
import 'package:Tremor_App/Resources/MyInheritedWidget.dart';
import 'package:Tremor_App/Widgets/bottom_navigator_widget.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:Tremor_App/Dialogs/exit_app_dialog.dart';
import 'Backend/Logs/note_logs.dart';
import 'Screens/ble_screen.dart';
import 'package:Tremor_App/Dialogs/gdpr_dialog.dart';
import 'package:Tremor_App/Widgets/drawer_widget.dart';
import 'package:Tremor_App/Screens/welcome_screen.dart';
import 'package:Tremor_App/Backend/store_version_check.dart';

bool disableGoogleAuthAndDataSavingForReleaseTesting = false;

class TabBarNavigator extends StatefulWidget {
  const TabBarNavigator({Key key}) : super(key: key);

  @override
  _TabBarNavigatorState createState() => _TabBarNavigatorState();
}

class _TabBarNavigatorState extends State<TabBarNavigator> {
  int selectedIndex = 0;
  UserAuthentication authentication;
  String status = 'none';
  LogInDialog logInDialog = LogInDialog();
  final List<Widget> widgetOptions = <Widget>[
    HomeScreen(
      uid: null,
    ),
    RelaxationScreen(),
    HistoryScreen(
      uid: null,
    ),
    BleScreen(),
  ];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => prepareApp());
    WidgetsBinding.instance.addPostFrameCallback((_) async {});
  }

  Future<void> prepareApp() async {
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp();
    authentication = UserAuthentication(refresh);
    if (disableGoogleAuthAndDataSavingForReleaseTesting == false) {
      handleInitialSignInStatus().then(
          (value) => MyStatefulWidget.of(context).initOptionLogsAfterSignIn());
    }
    if (disableGoogleAuthAndDataSavingForReleaseTesting == false) {
      checkSignInStatusPeriodically();
    }
    showFirstTimeIntroScreen();
    RateMyAppReminder().rateMyAppCheck(context, mounted);
    setSlideInstructionVisibility(context);
    try {
      StoreVersionCheck().versionCheck(context);
    } catch (e) {
      print('TabNavigator.prepareApp() => ' + e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    String uid;
    if (authentication != null) {
      if (authentication.userProfile != null) {
        uid = authentication.userProfile.id;
      } else {
        uid = '';
      }
    } else {
      uid = '';
    }
    return Scaffold(
        drawer: DrawerWidget(authentication),
        floatingActionButton: selectedIndex == 0
            ? Builder(builder: (context) {
                return Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01),
                  child: FloatingActionButton(
                    backgroundColor: Colors.white,
                    elevation: 0,
                    onPressed: () {
                      Scaffold.of(context).openDrawer();
                    },
                    child: Icon(Icons.menu),
                  ),
                );
              })
            : null,
        floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
        body: WillPopScope(
            onWillPop: onBackPress,
            child: Center(
                child: selectedIndex == widgetOptions.length - 1
                    ? BleScreen(
                        uid: uid,
                      )
                    : selectedIndex == widgetOptions.length - 2
                        ? HistoryScreen(uid: authentication.userProfile.id)
                        : selectedIndex == widgetOptions.length - 4
                            ? HomeScreen(uid: uid)
                            : widgetOptions.elementAt(selectedIndex))),
        bottomNavigationBar:
            BottomNavigatorWidget(selectedIndex, onItemTapped));
  }

  int drawingScreen = 2;
  int requestcount = 0;

  Future<void> onItemTapped(int index) async {
    DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    final androidInfo = await deviceInfoPlugin.androidInfo;

    if (disableGoogleAuthAndDataSavingForReleaseTesting == false) {
      MyStatefulWidget.of(context).saveData();
    }


    if (index == drawingScreen && int.parse(androidInfo.version.release) < 13  ) {
      var status = await Permission.storage.status;
      if (status.isGranted == true) {
        selectedIndex = index;
      } else {
        if (status.isPermanentlyDenied == true ||
            status.isRestricted == true ||
            status.isLimited == true ||
            requestcount > 1) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: AutoSizeText(AppLocalizations.of(context).translate(
                "Please allow storage permission to use this feature")),
          ));
          openAppSettings();
        } else {
          Permission.storage.request();
          requestcount++;
        }
      }
    } else {
      selectedIndex = index;
    }
    setState(() {});
  }

  void refresh() {
    setState(() {
      if (!GlobalUserProfile(profile: authentication.userProfile).isSigned) {
        logInDialog.show(context, authentication);
        debugPrint(
            'TabNavigator.Refresh() Detected unsigned user. Launching LogIn Dialog,');
      }
    });
  }

  Future<void> exitAppDialog(BuildContext context) async {
    refresh();
    await MyStatefulWidget.of(context).saveData();
    switch (await showDialog(
        context: context,
        builder: (BuildContext context) {
          return ExitAppDialog();
        })) {
      case 0:
        break;
      case 1:
        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
        break;
    }
  }

  Future<bool> onBackPress() {
    exitAppDialog(context);
    return Future.value(false);
  }

  void showFirstTimeIntroScreen() async {
    Timer.periodic(Duration(seconds: 1), (timer) {
      if (GlobalUserProfile(profile: authentication.userProfile).isSigned) {
        loadNotesFromFirebase(authentication.userProfile.id);
        showWelcomeScreenIfFirstTime();
        final status = AppTrackingTransparency.requestTrackingAuthorization();
        debugPrint(status.toString());
        showGdprScreenIfFirstTime();
        timer.cancel();
      }
    });
  }

  Future<void> loadNotesFromFirebase(String uid) async {
    NoteLogs logs = NoteLogs();
    logs.dataInit(context, uid);
  }

  Future<bool> handleInitialSignInStatus() async {
    try {
      return await authentication.trySignInWithPreviousData(context);
    } catch (e) {
      debugPrint(e.toString());
      return false;
    }
  }

  void checkSignInStatusPeriodically() async {
    Timer.periodic(Duration(seconds: 1), (timer) {
      if (!GlobalUserProfile(profile: authentication.userProfile).isSigned) {
        logInDialog.show(context, authentication);
      }
    });
  }

  void showGdprScreenIfFirstTime() {
    Future<SharedPreferences> sharedPrefs = SharedPreferences.getInstance();
    sharedPrefs.then((SharedPreferences prefs) {
      if (prefs.getInt("isGdprAgreed") == null ||
          prefs.getInt("isGdprAgreed") != 1) {
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return GdprDialog();
            });
      }
    });
  }

  void showWelcomeScreenIfFirstTime() {
    Future<SharedPreferences> sharedPrefs = SharedPreferences.getInstance();
    sharedPrefs.then((SharedPreferences prefs) {
      if (prefs.getInt("isWelcomedBefore") == null ||
          prefs.getInt("isWelcomedBefore") != 1) {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return WelcomeScreen();
        }));
      }
    });
  }

  void setSlideInstructionVisibility(BuildContext context) {
    Future<SharedPreferences> sharedPrefs = SharedPreferences.getInstance();
    sharedPrefs.then((SharedPreferences prefs) {
      if (prefs.getInt("hideOptionsSliderInstructions") != null &&
          prefs.getInt("hideOptionsSliderInstructions") == 1) {
        MyStatefulWidget.of(context).slideInstructionsVisible = false;
      }
    });
  }
}
