import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:Tremor_App/Backend/Models/options_model.dart';
import 'package:Tremor_App/Backend/data_file.dart';
import 'package:Tremor_App/Backend/Models/options_model_list.dart';
import 'package:flutter_test/flutter_test.dart' as flutter_tests;
import 'package:test/test.dart';
import 'package:Tremor_App/Backend/Logs/option_logs.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';
import 'data_file_test.dart';
import 'mocks/fake_path_provider.dart';
import 'package:mockito/mockito.dart';
import 'package:Tremor_App/Backend/Storage/firebase_storage_controller.dart';
import 'package:mockito/annotations.dart';

void update({bool newDataDownloaded}) {}
const String userId = '12345';
//File Options_12345.txt must contain this string
const String fileContent = r'{"data":["{\"date\":\"2021-07-22T00:00:00.000\",\"result\":30,\"options\":[0,0,7,0,0,0,5]}"]}';
const String fileContentFull =
    r'{"data":["{\"date\":\"2021-07-22T00:00:00.000\",\"result\":30,\"options\":[0,0,7,0,0,0,5]}","{\"date\":\"1997-10-07T00:00:00.000\",\"result\":99,\"options\":[8,2,3,4,5,6]}","{\"date\":\"1996-10-02T00:00:00.000\",\"result\":100,\"options\":[9,2,3,4,5,6]}"]}';
/*Must be replaced when run on different devices*/
const String kApplicationDocumentsPath = r'D:\Android projects\fix\tremorapp\lib\Assets\TestMaterial';

OptionsModel fileContentModel = OptionsModel(DateTime(2021, DateTime.july, 22), 30, <int>[0, 0, 7, 0, 0, 0, 5]);
OptionsModel model = OptionsModel(DateTime(1996, DateTime.october, 2), 100, <int>[9, 2, 3, 4, 5, 6]);
OptionsModel model2 = OptionsModel(DateTime(1997, DateTime.october, 7), 99, <int>[8, 2, 3, 4, 5, 6]);

@GenerateMocks([FirebaseStorageController])
void main() {
  flutter_tests.TestWidgetsFlutterBinding.ensureInitialized();
  FirebaseStorageController mockedController = MockFirebaseStorageController();

  group('Before initialization', () {
    setUp(() async {
      PathProviderPlatform.instance = FakePathProviderPlatform();
    });

    test('On initialization User ID shall be null', () {
      OptionLogs optionLogs = OptionLogs(update);
      expect(optionLogs.userId, null);
    });

    test('On initialization Options List shall be empty', () {
      OptionLogs optionLogs = OptionLogs(update);
      expect(optionLogs.options.length, 0);
    });

    test('On initialization length shall be 0', () {
      OptionLogs optionLogs = OptionLogs(update);
      expect(optionLogs.length, 0);
    });
  });

  group('Local data reading', () {
    test('Local logs shall be correctly decoded', () async {
      OptionLogs optionLogs = OptionLogs(update);
      optionLogs.logFile = DataFile('Options_' + userId + '.txt');
      OptionsModel savedModel = OptionsModelList.fromJson(json.decode(fileContent)).data.first;
      OptionsModelList decodedList = await optionLogs.readLocallySavedLogs();

      expect(decodedList.data.last == savedModel, true);
      expect(decodedList.length, 1);
    });

    test('Local logs return null if file does not exist', () async {
      OptionLogs optionLogs = OptionLogs(update);
      optionLogs.logFile = DataFile('Options_' + 'BadId' + '.txt');
      expect(await optionLogs.readLocallySavedLogs(), null);
    });
  });

  group('Local data writing', () {
    test('When new log is added - data is decodable', () async {
      OptionLogs optionLogs = OptionLogs(update);
      optionLogs.logFile = DataFile('Options_' + userId + '1' + '.txt');
      optionLogs.logFile.reWriteString(json.encode(OptionsModelList.empty().toJson()));
      optionLogs.alreadyInitiated = true;

      await optionLogs.logOption(model, userId + '1');
      await optionLogs.logOption(model2, userId + '1');
      await optionLogs.logOption(null, userId + '1');

      OptionsModelList decodedList = await optionLogs.readLocallySavedLogs();
      expect(decodedList.data.length, 2);
      expect(decodedList.data[0] == model2, true);
      expect(decodedList.data[1] == model, true);
    });
  });

  group('Remote data read', () {
    test('Remote data is returned', () async {
      OptionLogs logs = OptionLogs(({bool newDataDownloaded}) {});
      logs.userId = userId;
      logs.controller = mockedController;

      when(mockedController.getFile(
              localFileName: 'Options_Firebase_' + userId, cloudFolderName: 'OptionData', cloudFileName: userId))
          .thenAnswer((_) => DataFile('Options_' + userId + '.txt').localFile);

      OptionsModelList list = await logs.readRemotelySavedLogs(userId);
      expect(list.data.length, 1);
      expect(list.data.first == fileContentModel, true);
    });

    test('Remote data is null when reading is unsuccessful', () async {
      OptionLogs logs = OptionLogs(({bool newDataDownloaded}) {});
      logs.userId = userId;
      logs.controller = mockedController;

      when(mockedController.getFile(
              localFileName: 'Options_Firebase_' + userId, cloudFolderName: 'OptionData', cloudFileName: userId))
          .thenAnswer((_) => null);

      expect(await logs.readRemotelySavedLogs(userId), null);
    });

    test('Remote data is empty when downloaded data is corrupted', () async {
      OptionLogs logs = OptionLogs(({bool newDataDownloaded}) {});
      logs.userId = userId;
      logs.controller = logs.controller = mockedController;
      DataFile corruptDataFile = DataFile('Options_' + 'EmptyDataFile' + '.txt');
      await corruptDataFile.reWriteString(fileContent + '☺☻♥♦♣♠');

      when(mockedController.getFile(
              localFileName: 'Options_Firebase_' + userId, cloudFolderName: 'OptionData', cloudFileName: userId))
          .thenAnswer((_) => corruptDataFile.localFile);

      OptionsModelList list = await logs.readRemotelySavedLogs(userId);
      expect(list.data.length, 0);
    });
  });

  group('Remote data write', () {
    test('Data appears to remote repository', () async {
      OptionLogs logs = OptionLogs(({bool newDataDownloaded}) {});
      logs.userId = userId;
      logs.alreadyInitiated = true;
      logs.controller = logs.controller = mockedController;
      String fileName = getRandomFileName();

      when(mockedController.storeFile(
              tempLocalFileName: 'Options_Firebase_' + userId,
              cloudFolderName: 'OptionData',
              cloudFileName: userId,
              dataToSave: json.encode(OptionsModelList([model]).toJson())))
          .thenAnswer((_) async {
        DataFile dataFile = DataFile('OptionData' + r'\' + fileName);
        await dataFile.reWriteString(json.encode(OptionsModelList([model]).toJson()));
      });

      logs.options.add(model);
      await logs.logFirebase(userId);
      expect(await DataFile('OptionData' + r'\' + fileName).readString(), json.encode(OptionsModelList([model]).toJson()));

      await (await DataFile('OptionData' + r'\' + fileName).localFile).delete();
    });
  });

  group('Decoding logs from memory', () {
    test('When local data and remote data are identical, they are returned without changes', () async {
      OptionLogs logs = OptionLogs(({bool newDataDownloaded}) {});
      logs.logFile = DataFile('Options_Firebase_' + userId + '.txt');
      logs.userId = userId;
      logs.alreadyInitiated = true;
      logs.controller = mockedController;
      expect(logs.options.length, 0);

      when(mockedController.getFile(
              localFileName: 'Options_Firebase_' + userId, cloudFolderName: 'OptionData', cloudFileName: userId))
          .thenAnswer((_) => DataFile('Options_' + userId + '.txt').localFile);

      expect((await logs.getLogsFromMemory(userId)).data.first == fileContentModel, true);
    });
  });

  test('When local data has extra data point, cloud data is supplemented', () async {
    OptionLogs logs = OptionLogs(({bool newDataDownloaded}) {});
    logs.logFile = DataFile('Options_' + userId + '1' + '.txt');
    logs.userId = userId;
    logs.alreadyInitiated = true;
    logs.controller = logs.controller = mockedController;
    String cloudFileName = getRandomFileName();

    expect(logs.options.length, 0);
    OptionsModelList correctList = OptionsModelList.fromJson(json.decode(fileContentFull));
    correctList.add(fileContentModel);
    String resultString = json.encode(correctList.toJson());

    when(mockedController.getFile(
            localFileName: 'Options_Firebase_' + userId, cloudFolderName: 'OptionData', cloudFileName: userId))
        .thenAnswer((_) => DataFile('Options_' + userId + '.txt').localFile);

    when(mockedController.storeFile(
            tempLocalFileName: 'Options_Firebase_' + userId,
            cloudFolderName: 'OptionData',
            cloudFileName: userId,
            dataToSave: fileContentFull))
        .thenAnswer((_) async {
    });

    expect(json.encode((await logs.getLogsFromMemory(userId)).toJson()), resultString);
  });

  test('When cloud data has extra data point, local data is supplemented', () async {
    OptionLogs logs = OptionLogs(({bool newDataDownloaded}) {});
    logs.logFile = DataFile('Options_' + userId + '.txt');
    logs.userId = userId;
    logs.alreadyInitiated = true;
    logs.controller = mockedController;

    when(mockedController.getFile(
            localFileName: 'Options_Firebase_' + userId, cloudFolderName: 'OptionData', cloudFileName: userId))
        .thenAnswer((_) => DataFile('Options_' + userId + '1' + '.txt').localFile);

    expect(json.encode((await logs.getLogsFromMemory(userId)).toJson()), fileContentFull);
    expect(await logs.logFile.readString(), fileContentFull);

    await logs.logFile.reWriteString(fileContent);
  });

  test('When local data is NULL, set it to remote data', () async {
    OptionLogs logs = OptionLogs(({bool newDataDownloaded}) {});
    String nonExistentName = getRandomFileName();
    logs.logFile = DataFile('Options_' + nonExistentName);
    logs.userId = userId;
    logs.alreadyInitiated = true;
    logs.controller = mockedController;

    when(mockedController.getFile(
            localFileName: 'Options_Firebase_' + userId, cloudFolderName: 'OptionData', cloudFileName: userId))
        .thenAnswer((_) => DataFile('Options_' + userId + '1' + '.txt').localFile);

    expect(json.encode((await logs.getLogsFromMemory(userId)).toJson()), fileContentFull);
    expect(await logs.logFile.readString(), fileContentFull);

    await (await logs.logFile.localFile).delete();
  });

  test('When both data is NULL, set local to empty', () async {
    OptionLogs logs = OptionLogs(update);
    String nonExistentName = getRandomFileName();
    logs.logFile = DataFile('Options_' + nonExistentName);
    logs.userId = userId;
    logs.alreadyInitiated = true;
    logs.controller = mockedController;
    String emptyListString = json.encode(OptionsModelList.empty().toJson());

    when(mockedController.getFile(
            localFileName: 'Options_Firebase_' + userId, cloudFolderName: 'OptionData', cloudFileName: userId))
        .thenAnswer((_) => null);

    when(mockedController.storeFile(
            tempLocalFileName: 'Options_Firebase_' + userId,
            cloudFolderName: 'OptionData',
            cloudFileName: userId,
            dataToSave: emptyListString))
        .thenAnswer((_) async {
      debugPrint('Called store file');
      DataFile dataFile = DataFile('OptionData' + r'\' + 'nulled_data_' + nonExistentName);
      await dataFile.reWriteString(emptyListString);
    });

    expect(json.encode((await logs.getLogsFromMemory(userId)).toJson()), emptyListString);

    await (await logs.logFile.localFile).delete();
  });

  group('Initialization', () {
    test('Initialization when remote is null - options are set to local', () async {
      OptionLogs logs = OptionLogs(update);
      String tempId = '9511';
      logs.controller = mockedController;

      DataFile data = DataFile('Options_' + tempId + '.txt');
      await data.reWriteString(fileContent);

      when(mockedController.getFile(
              localFileName: 'Options_Firebase_' + tempId + '.txt', cloudFolderName: 'OptionData', cloudFileName: tempId))
          .thenAnswer((_) => data.localFile);

      await logs.init(tempId);

      expect(logs.alreadyInitiated, true);
      expect(logs.logFile.fileName, 'Options_' + tempId + '.txt');
      expect(json.encode(logs.options.toJson()), fileContent);

      await (await logs.logFile.localFile).delete();
    });

    test('Initialization when local is null - options are set to remote', () async {
      OptionLogs logs = OptionLogs(update);
      String tempId = getRandomFileName().replaceAll('.txt', '');
      String correctFileName = 'Options_' + getRandomFileName();
      logs.controller = mockedController;

      DataFile data = DataFile(correctFileName);
      await data.reWriteString(fileContent);

      when(mockedController.getFile(
              localFileName: 'Options_Firebase_' + tempId, cloudFolderName: 'OptionData', cloudFileName: tempId))
          .thenAnswer((_) => data.localFile);

      await logs.init(tempId);

      expect(logs.alreadyInitiated, true);
      expect(json.encode(logs.options.toJson()), fileContent);

      await (await logs.logFile.localFile).delete();
      await (await data.localFile).delete();
    });

    test('Initialization when both are null - options are set to empty', () async {
      OptionLogs logs = OptionLogs(update);
      String tempId = getRandomFileName().replaceAll('.txt', '');
      logs.controller = mockedController;

      when(mockedController.getFile(
              localFileName: 'Options_Firebase_' + tempId, cloudFolderName: 'OptionData', cloudFileName: tempId))
          .thenAnswer((_) => null);

      await logs.init(tempId);

      expect(logs.alreadyInitiated, true);
      expect(json.encode(logs.options.toJson()), json.encode(OptionsModelList.empty().toJson()));

      await (await logs.logFile.localFile).delete();
    });
  });

  group('complex scenarios', (){
    test(
        'When internet is unavailable and local is empty- options are set to empty, '
            'but firebase is not overwritten when new logs are introduced', () async {
      OptionLogs logs = OptionLogs(update);
      String localId = '159263';
      logs.controller = mockedController;
      logs.logFile = DataFile('Options_' + localId + '.txt');
      await logs.logFile.reWriteString(json.encode(OptionsModelList.empty().toJson()));

      when(mockedController.getFile(
          localFileName: 'Options_Firebase_' + localId, cloudFolderName: 'OptionData', cloudFileName: localId))
          .thenThrow(FirebaseException(plugin:'Firebase.StorageException', code: 'random-code'));

      await logs.init(localId);

      bool triedUpload = false;
      when(mockedController.storeFile(
          tempLocalFileName: 'Options_Firebase_' + userId,
          cloudFolderName: 'OptionData',
          cloudFileName: userId,
          dataToSave:  json.encode(OptionsModelList([model]).toJson())))
          .thenAnswer((_) async {
        triedUpload = true;
      });

      expect(logs.alreadyInitiated, true);
      expect(json.encode(logs.options.toJson()), json.encode(OptionsModelList.empty().toJson()));

      await logs.logOption(model, userId);
      expect(json.encode(logs.options.toJson()), json.encode(OptionsModelList([model]).toJson()));
      expect(triedUpload, false);

      await (await logs.logFile.localFile).delete();
    });
  });
}

class MockFirebaseStorageController extends Mock implements FirebaseStorageController {}
