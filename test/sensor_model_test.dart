import 'package:flutter_test/flutter_test.dart' as test;
import 'package:test/test.dart';
import 'package:Tremor_App/Backend/Models/SensorModel.dart';

void main() {
    List<List<String>> dataString = <List<String>>[['x1','y1','z1','t1'], ['x2','y2','z2','t2'],['x3','y3','z3','t3']];
    SensorModel model = SensorModel(DateTime.now(), SensorTypes.Accelerometer);

    group('Data types', () {
        test.test('Sensor model shall identify sensor type', () {
            expect(model.sensorType is SensorTypes, true);
        });

        test.test('Sensor model shall save accelerometer data', () {
            expect(model.sensorType, SensorTypes.Accelerometer);
        });
    });

    group('Input tests', () {
        test.test('Initially data list shall be empty', () {
            expect(model.dataXYZT.isEmpty, true);
        });

        test.test('Data in List<List<String>> type shall be addable in portions', () {
            model.dataXYZT = dataString;
            expect(model.dataXYZT.length, 3);
        });

        test.test('Data in shall be clearable', () {
            model.clear();
            expect(model.dataXYZT.length, 0);
        });
    });

    group('Conversion tests', () {
        test.test('Data shall be  convertible to csv', () {
            model.dataXYZT = dataString;
            String dataCSV = 'x1 y1 z1 t1 x2 y2 z2 t2 x3 y3 z3 t3 ';
            expect(model.convertToCSV() == dataCSV, true);
        });
    });
}
