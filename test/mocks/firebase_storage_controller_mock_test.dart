import 'package:Tremor_App/Backend/Storage/firebase_storage_controller.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:Tremor_App/Backend/data_file.dart';
import 'fake_path_provider.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'dart:math';

String localFileName = 'firebase_storage_test.txt';
String fileContent = 'This is storage test file';
String cloudFolderName = 'cloudFolderName';
String cloudFileName = 'cloudFileName.txt';
Random random = Random();

String getRandomFileName() => 'test_file_' + random.nextInt(99999).toString() + '.txt';
String getRandomFileDirectory() => 'test_directory_' + random.nextInt(99999).toString();

@GenerateMocks([FirebaseStorageController])
void main() {
  WidgetsFlutterBinding.ensureInitialized();
  FirebaseStorageController storageController = MockFirebaseStorageController();

  group('Data faking', () {
    setUpAll(() async {
      PathProviderPlatform.instance = FakePathProviderPlatform();
    });

    test('File must be downloaded', () async {
      Future<File> localFile = DataFile(localFileName).localFile;
      when(storageController.getFile(
              localFileName: localFileName, cloudFolderName: cloudFolderName, cloudFileName: cloudFileName))
          .thenAnswer((_) => localFile);
      File downloadedFile = await storageController.getFile(
          localFileName: localFileName, cloudFolderName: cloudFolderName, cloudFileName: cloudFileName);

      expect(await downloadedFile.readAsString(), fileContent);
    });

    test('File must be uploaded', () async {
      String newFileName = getRandomFileName();
      String newFolderName = getRandomFileDirectory();
      String newLocalFileName = 'local_name.txt';

      when(storageController.storeFile(
              tempLocalFileName: newLocalFileName,
              cloudFolderName: newFolderName,
              cloudFileName: newFileName,
              dataToSave: fileContent))
          .thenAnswer((_) async {
        DataFile dataFile = DataFile(newFolderName + r'\' + newFileName);
        await dataFile.reWriteString(fileContent);
      });
      await storageController.storeFile(
          tempLocalFileName: newLocalFileName,
          cloudFolderName: newFolderName,
          cloudFileName: newFileName,
          dataToSave: fileContent);

      DataFile uploadedFile = DataFile(newFolderName + r'\' + newFileName);

      expect(await uploadedFile.readString(), fileContent);

      String pathToDir = (await uploadedFile.localFile).path;
      pathToDir = pathToDir.substring(0, pathToDir.lastIndexOf(r'\'));
      await (await uploadedFile.localFile).delete();
      Directory(pathToDir).deleteSync();
    });
  });
}

class MockFirebaseStorageController extends Mock implements FirebaseStorageController {}
