import 'package:plugin_platform_interface/plugin_platform_interface.dart';
import 'package:test/fake.dart';
import 'dart:async';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';

/*Must be replaced when run on different devices*/
const String kApplicationDocumentsPath = r'D:\Android projects\fix\tremorapp\lib\Assets\TestMaterial';

class FakePathProviderPlatform extends Fake with MockPlatformInterfaceMixin implements PathProviderPlatform {
    @override
    Future<String> getApplicationDocumentsPath() async {
        return kApplicationDocumentsPath;
    }
}