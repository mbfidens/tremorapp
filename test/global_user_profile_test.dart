import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart' as test;
import 'package:test/test.dart';
import 'package:Tremor_App/Backend/User/global_user_profile.dart';
import 'package:Tremor_App/Backend/User/local_user_profile.dart';

void main() {
    GlobalUserProfile emptyUser = GlobalUserProfile();
    GlobalUserProfile user = GlobalUserProfile(profile: LocalUserProfile(name: 'Name', id: '1', surname: 'Surname'));

    group('Null tests', () {
        test.test('Empty user profile shall return name as null', () {
            expect(emptyUser.name, null);
        });

        test.test('Empty user profile shall return surname as null', () {
            expect(emptyUser.surname, null);
        });

        test.test('Empty user profile shall return isSigned as false', () {
            expect(emptyUser.isSigned, false);
        });

        test.test('Empty user profile shall return avatar as null', () {
            expect(emptyUser.avatar, null);
        });
    });

    group('Outptu tests', () {
        test.test('Filled user profile shall return name as "Name"', () {
            expect(user.name, 'Name');
        });

        test.test('Filled user profile shall return surname as "Surname"', () {
            expect(user.surname, "Surname");
        });

        test.test('Filled user profile shall return isSigned as true', () {
            expect(user.isSigned, true);
        });
    });

    group('Avatar tests', () {
        test.test('Avatar getter shall return null if url is null', () {
            expect(user.avatar, null);
        });

        test.test('Avatar getter shall return null if url is empty', () {
            user.profile.imageUrl  = '';
            expect(user.avatar, null);
        });

        test.test('Avatar getter shall return null if url is empty', () {
            user.profile.imageUrl  = 'https://yt3.ggpht.com/yti/APfAmoERL_itQH4Z0PcJMeFlcF-s_wM8wnN2niVsGHQzFg=s108-c-k-c0x00ffffff-no-rj';
            expect(user.avatar is CircleAvatar, true);
        });
    });
}
