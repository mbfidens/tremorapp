import 'dart:convert';

import 'package:Tremor_App/Backend/Logs/note_logs.dart';
import 'package:Tremor_App/Backend/Models/notes_model.dart';
import 'package:Tremor_App/Backend/Models/notes_model_list.dart';

import 'package:Tremor_App/Backend/data_file.dart';

import 'package:flutter_test/flutter_test.dart' as flutter_tests;
import 'package:test/test.dart';

import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';
import 'package:test/fake.dart';
import 'dart:async';
import 'package:flutter/material.dart';

NoteLogs notesLogs;
BuildContext context;
const String uid = 'notes12345';
const String fileContent =
    r'{"data":["{\"id\":0,\"title\":\"33\",\"content\":\"33\",\"isImportant\":0,\"date\":\"2021-07-27T11:53:11.098300\"}"]}';
NotesModelList decodedList;
NotesModel model = NotesModel(
    id: 123456,
    title: "title1",
    content: "content1",
    isImportant: true,
    date: DateTime(2020, DateTime.october, 9));
NotesModel model2 = NotesModel(
    id: 65895,
    title: "title2",
    content: "content2",
    isImportant: true,
    date: DateTime(2021, DateTime.october, 6));

void main() {
  notesLogs = NoteLogs();
  flutter_tests.TestWidgetsFlutterBinding.ensureInitialized();

  group('Before initialization', () {
    test('On initialization Options List shall be empty', () {
      expect(notesLogs.savedData.length, 0);
    });
  });

  group('Local data reading', () {
    test('Log should be added', () async {
      notesLogs.addLog(model, uid);
      expect(notesLogs.savedData.length, 1);
    });

    test('Log should be removed by index', () async {
      notesLogs.removeLog(0, uid);
      expect(notesLogs.savedData.length, 0);
    });

    test('Log should be removed by note', () async {
      notesLogs.addLog(model, uid);
      notesLogs.addLog(model2, uid);
      notesLogs.removeLogByNote(model, uid);
      expect(notesLogs.savedData.length, 1);
    });
  });
}
