import 'package:flutter_test/flutter_test.dart' as test;
import 'package:test/test.dart';
import 'dart:convert';
import 'package:Tremor_App/Backend/Models/notes_model.dart';
import 'package:Tremor_App/Backend/Models/notes_model_list.dart';

bool checkIfSameModels(NotesModel firstModel, NotesModel secondModel) {
  if (firstModel.id == secondModel.id &&
      firstModel.date == secondModel.date &&
      firstModel.content == secondModel.content &&
      firstModel.isImportant == secondModel.isImportant &&
      firstModel.title == secondModel.title) {
    return true;
  } else {
    return false;
  }
}

void main() {
  NotesModel model1 = NotesModel(id: 0, title: 'name', content: 'Text of content', isImportant: false, date: DateTime.now());
  NotesModel model2 = NotesModel(id: 1, title: 'name1', content: 'Text of content1', isImportant: true, date: DateTime.now());
  NotesModel model3 = NotesModel(id: 2, title: 'name2', content: 'Text of content2', isImportant: false, date: DateTime.now());
  NotesModelList list = NotesModelList(<NotesModel>[model1, model2, model3]);

  group('Output tests', () {
    test.test('List length shall be available', () {
      expect(list.length, 3);
    });

    test.test('List length shall getter shall output 0 if list is empty', () {
      NotesModelList emptyList = NotesModelList.empty();
      expect(emptyList.length, 0);
    });

    test.test('Models should be attainable through getter "data"', () {
      expect(checkIfSameModels(list.data[1], model2), true);
    });
  });

  group('Input tests', () {
    test.test('Model shall be added to start of the list', () {
      list.add(model1);
      list.add(model2);
      expect(list.length, 5);
      expect(checkIfSameModels(list.models.first, model2), true);
    });

    test.test('Remove at 0 shall remove first', () {
      list.remove(0);
      expect(list.length, 4);
      expect(checkIfSameModels(list.models.first, model1), true);
    });

    test.test('Remove at 3 shall remove last', () {
        list.remove(3);
        expect(list.length, 3);
        expect(checkIfSameModels(list.models.last, model2), true);
    });

    test.test('Remove at 1 shall remove second', () {
        expect(checkIfSameModels(list.models[1], model1), true);
        list.remove(1);
        expect(list.length, 2);
        expect(checkIfSameModels(list.models[1], model2), true);
    });
  });

  group('JSON serialisation tests', () {
    test.test('list shall be convertable to json', () {
      expect(list.toJson() is Map<String, dynamic>, true);
    });

    test.test('list models shall be decodable after conversion', () {
      NotesModelList newList = NotesModelList.fromJson(json.decode(json.encode(list.toJson())));
      expect(checkIfSameModels(list.models.first, newList.models.first), true);
      expect(checkIfSameModels(list.models.last, newList.models.last), true);
      expect(checkIfSameModels(list.models.first, newList.models.last), false);
      expect(checkIfSameModels(list.models[1], newList.models[1]), true);
    });
  });
}
