import 'package:Tremor_App/Backend/User/local_user_profile.dart';
import 'package:Tremor_App/Screens/ble_screen.dart';
import 'package:Tremor_App/tab_navigator.dart';
import 'package:flutter_test/flutter_test.dart' as test;
import 'package:test/test.dart';
import 'dart:convert';

void main() {
  String serialNumber = serialFromDatabase.key;
  LocalUserProfile profile = LocalUserProfile(
      email: "car@sad.lt",
      password: "Password5",
      name: "Bob",
      surname: "Jeans",
      age: 20,
      weight: 80,
      height: 180,
      residency: "Kaunas",
      condition: "Tremor",
      imageUrl: "www.google.lt",
      registrationDate: DateTime.now().toIso8601String(),
      id: "45sdfee",
      sn: serialNumber,
      version: "LT");
  LocalUserProfile newProfile =
      LocalUserProfile.fromJson(json.decode(json.encode(profile.toJson())));

  group('Coversion tests', () {
    test.test('model shall be convertable to json', () {
      expect(profile.toJson() is Map<String, dynamic>, true);
    });

    test.test('Email should be reversible in conversion', () {
      expect(profile.email == newProfile.email, true);
    });

    test.test('Password should be reversible in conversion', () {
      expect(profile.password == newProfile.password, true);
    });

    test.test('Name should be reversible in conversion', () {
      expect(profile.name == newProfile.name, true);
    });

    test.test('Surname should be reversible in conversion', () {
      expect(profile.surname == newProfile.surname, true);
    });

    test.test('Age should be reversible in conversion', () {
      expect(profile.age == newProfile.age, true);
    });

    test.test('Weight should be reversible in conversion', () {
      expect(profile.weight == newProfile.weight, true);
    });

    test.test('Height should be reversible in conversion', () {
      expect(profile.height == newProfile.height, true);
    });

    test.test('Residency should be reversible in conversion', () {
      expect(profile.residency == newProfile.residency, true);
    });

    test.test('Condition should be reversible in conversion', () {
      expect(profile.condition == newProfile.condition, true);
    });

    test.test('ImageUrl should be reversible in conversion', () {
      expect(profile.imageUrl == newProfile.imageUrl, true);
    });

    test.test('Sn should be reversible in conversion', () {
      expect(profile.sn == newProfile.sn, true);
    });

    test.test('Version should be reversible in conversion', () {
      expect(profile.version == newProfile.version, true);
    });

    test.test('Registration date should be reversible in conversion', () {
      expect(profile.registrationDate == newProfile.registrationDate, true);
    });

    test.test('id should be reversible in conversion', () {
      expect(profile.id, newProfile.id);
    });
  });
}
