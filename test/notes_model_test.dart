import 'package:flutter_test/flutter_test.dart' as test;
import 'package:test/test.dart';
import 'dart:convert';
import 'package:Tremor_App/Backend/Models/notes_model.dart';

void main() {
  NotesModel model = NotesModel(id: 1, title: 'name', content: 'Text of content', isImportant: false, date: DateTime.now());
  NotesModel newModel = NotesModel.fromJson(json.decode(json.encode(model.toJson())));

  group('Coversion tests', () {
    test.test('model shall be convertable to json', () {
      expect(model.toJson() is Map<String, dynamic>, true);
    });

    test.test('Data should be reversible in conversion', () {
      expect(model.date == newModel.date, true);
    });

    test.test('Date should be reversible in conversion', () {
      expect(model.date.isAtSameMomentAs(newModel.date), true);
    });

    test.test('id should be reversible in conversion', () {
      expect(model.id, newModel.id);
    });

    test.test('Name should be reversible in conversion', () {
      expect(model.title, newModel.title);
    });

    test.test('isImportant should be reversible in conversion', () {
      expect(model.isImportant, newModel.isImportant);
    });

    test.test('Content should be reversible in conversion', () {
      expect(model.content == newModel.content, true);
    });
  });

  group('Output tests', () {
    NotesModel emptyModel = NotesModel.empty();
    test.test('Empty() shall return emty model', () {
      expect(emptyModel.content == '' && emptyModel.title == '' && emptyModel.isImportant == false, true);
    });
    test.test('isEmty() shall return true if empty', () {
        expect(emptyModel.isEmpty, true);
    });
  });
}
