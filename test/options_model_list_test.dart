import 'package:flutter_test/flutter_test.dart' as test;
import 'package:test/test.dart';
import 'dart:convert';
import 'package:Tremor_App/Backend/Models/options_model.dart';
import 'package:Tremor_App/Backend/Models/options_model_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

void main() {
  OptionsModel model1 = OptionsModel(DateTime(1996, DateTime.october, 2, 1, 1, 1, 1, 2), 100, <int>[9, 2, 3, 4, 5, 6]);
  OptionsModel model2 = OptionsModel(DateTime(1997, DateTime.october, 3), 101, <int>[10, 2, 3, 4, 5, 7]);
  OptionsModel model3 = OptionsModel(DateTime(1998, DateTime.october, 4), 102, <int>[11, 2, 3, 4, 5, 8]);
  OptionsModelList list = OptionsModelList(<OptionsModel>[model1, model2, model3]);

  group('Output tests', () {
    test.test('List length shall be available', () {
      expect(list.length, 3);
    });

    test.test('List length shall getter shall output 0 if list is empty', () {
      OptionsModelList emptyList = OptionsModelList.empty();
      expect(emptyList.length, 0);
    });

    test.test('Models should be attainable through getter "data"', () {
      expect(list.data[1] == model2, true);
    });

    test.test('Only models with same date shall output true', () {
      OptionsModel model1SameDay =
          OptionsModel(DateTime(1996, DateTime.october, 2, 1, 1, 1, 1, 1), 100, <int>[9, 2, 3, 4, 5, 6]);
      OptionsModel model1NextDay =
          OptionsModel(DateTime(1996, DateTime.october, 3, 1, 1, 1, 1, 1), 100, <int>[9, 2, 3, 4, 5, 6]);

      expect(list.checkIfSameDay(model1, model1), true);
      expect(list.checkIfSameDay(model2,  model1), false);
      expect(list.checkIfSameDay(model1, model1SameDay), true);
      expect(list.checkIfSameDay(model1, model1NextDay), false);
    });
  });

  group('JSON serialisation tests', () {
    OptionsModelList newList = OptionsModelList.fromJson(json.decode(json.encode(list.toJson())));

    test.test('list shall be convertable to json', () {
      expect(list.toJson() is Map<String, dynamic>, true);
    });

    test.test('list models shall be decodable after conversion', () {
      expect(list.models.first == newList.models.first, true);
      expect(list.models.last == newList.models.last, true);
      expect(list.models.first == newList.models.last, false);
      expect(list.models[1] == model2, true);
    });
  });

  group('Sorting tests', () {
    OptionsModelList newList = OptionsModelList(<OptionsModel>[model1, model2, model3]);
    newList.sort();

    test.test('most recent (model3) becomes first', () {
      expect((model3 == newList.data.first), true);
    });

    test.test('oldest (model1) becomes last', () {
      expect((model1 == newList.data.last), true);
    });

    test.test('middle (model2) remains with unchanged position', () {
      expect(model2 == newList.data[1], true);
    });
  });

  group('Input tests', () {
    OptionsModelList tempList = OptionsModelList.empty();

    test.test('Null can not be added', () {
      tempList.add(null);
      expect(tempList.length, 0);
    });

    test.test('One copy of model shall be added per addition', () {
      tempList.add(model1);
      expect(tempList.length, 1);
      expect(tempList.data.first == model1, true);
    });

    test.test('Models shall be added in sequence from newest to oldest', () {
      tempList.add(model2);
      expect(tempList.length, 2);
      expect(tempList.data.last == model1, true);
      expect(tempList.data.first == model2, true);
    });

    test.test('Duplicated days shall return index of 0 when its first in list', () {
      expect(tempList.getIndexIfDayDuplicates(model2), 0);
    });

    test.test('Duplicated days shall return index of 1 in when its second', () {
      expect(tempList.getIndexIfDayDuplicates(model1), 1);
    });

    test.test('Newest model of the same day shall replace old model in list when its last element', () {
      OptionsModel modelSameDayAs1 =
          OptionsModel(DateTime(1996, DateTime.october, 2, 1, 1, 1, 9, 9), 3, <int>[4, 3, 2, 1, 1, 7]);
      tempList.add(modelSameDayAs1);

      expect(tempList.length, 2);
      expect(tempList.data.last == modelSameDayAs1, true);
    });

    test.test('Newest model of the same day shall replace old model in list when its first element', () {
      OptionsModel modelSameDayAs2 = OptionsModel(DateTime(1997, DateTime.october, 3), 1, <int>[1, 0, 1, 2, 3, 4]);
      tempList.add(modelSameDayAs2);

      expect(tempList.length, 2);
      expect(tempList.data.first == modelSameDayAs2, true);
    });

    test.test('Newest model of the same day shall replace old model in list when its middle element', () {
      tempList.add(model3);
      OptionsModel modelSameDayAs2 = OptionsModel(DateTime(1997, DateTime.october, 3), 1, <int>[1, 0, 1, 2, 3, 4]);
      tempList.add(modelSameDayAs2);

      expect(tempList.data[1] == modelSameDayAs2, true);
    });
  });

  group('Merge tests', () {
    OptionsModel model4 = OptionsModel(DateTime(1999, DateTime.october, 4), 103, <int>[12, 2, 3, 4, 5, 8]);
    OptionsModel model5 = OptionsModel(DateTime(2000, DateTime.october, 4), 104, <int>[13, 2, 3, 4, 5, 8]);
    OptionsModelList listToMerge = OptionsModelList(<OptionsModel>[model4, model5]);
    OptionsModelList mergedList = OptionsModelList(<OptionsModel>[model1, model2, model3]);

    mergedList.merge(listToMerge);
    debugPrint('after: ' + mergedList.toJson().toString());

    test.test('Different lists shall have summed length after merging', () {
      expect(mergedList.length, list.length + listToMerge.length);
    });

    test.test('After merging different lists, first element is model5', () {
      expect(mergedList.data.first == model5, true);
    });

    test.test('After merging different lists, last element is model1', () {
      expect(mergedList.data.last == model1, true);
    });

    test.test('After merging different lists, second element is model4', () {
      expect(mergedList.data[1] == model4, true);
    });
  });
}
