import 'package:Tremor_App/Backend/validator.dart';
import 'package:flutter_test/flutter_test.dart' as test;
import 'package:test/test.dart';

void main() {
  Validator validator = Validator();

  test.test('Name', () {
    expect(validator.checkIfNameIsCorrect("Bob") , true);
  });

  test.test('Name', () {
    expect(validator.checkIfNameIsCorrect("A") , false);
  });

  test.test('Name', () {
    expect(validator.checkIfNameIsCorrect("") , false);
  });

  test.test('Name', () {
    expect(validator.checkIfNameIsCorrect("/Åaaa") , false);
  });

  test.test('Name', () {
    expect(validator.checkIfNameIsCorrect("Penelopius") , true);
  });

  test.test('Name', () {
    expect(validator.checkIfNameIsCorrect("Penelopius1") , false);
  });

  test.test('Name', () {
    expect(validator.checkIfNameIsCorrect("Penelopius1Penelopius1") , false);
  });

  test.test('Surname', () {
    expect(validator.checkIfSurnameIsCorrect("Gob") , true);
  });

  test.test('Surname', () {
    expect(validator.checkIfSurnameIsCorrect("") , false);
  });

  test.test('Surname', () {
    expect(validator.checkIfSurnameIsCorrect("A") , false);
  });

  test.test('Surname', () {
    expect(validator.checkIfSurnameIsCorrect("/Åaaa") , false);
  });

  test.test('Surname', () {
    expect(validator.checkIfSurnameIsCorrect("Penelopius") , true);
  });

  test.test('Surname', () {
    expect(validator.checkIfSurnameIsCorrect("Penelopius1") , false);
  });

  test.test('Height', () {
    expect(validator.checkIfHeightIsCorrect("250") , false);
  });

  test.test('Height', () {
    expect(validator.checkIfHeightIsCorrect("10") , false);
  });

  test.test('Height', () {
    expect(validator.checkIfHeightIsCorrect("160") , true);
  });

  test.test('Height', () {
    expect(validator.checkIfHeightIsCorrect("") , false);
  });

  test.test('Height', () {
    expect(validator.checkIfHeightIsCorrect("sad") , false);
  });

  test.test('Age', () {
    expect(validator.checkIfAgeIsCorrect("sad") , false);
  });

  test.test('Age', () {
    expect(validator.checkIfAgeIsCorrect("") , false);
  });

  test.test('Age', () {
    expect(validator.checkIfAgeIsCorrect("fe") , false);
  });

  test.test('Age', () {
    expect(validator.checkIfAgeIsCorrect("15") , true);
  });

  test.test('Age', () {
    expect(validator.checkIfAgeIsCorrect("200") , false);
  });

  test.test('Age', () {
    expect(validator.checkIfAgeIsCorrect("80") , true);
  });

  test.test('Email', () {
    expect(validator.checkIfEmailIsCorrect("") , false);
  });

  test.test('Email', () {
    expect(validator.checkIfEmailIsCorrect("sadsa") , false);
  });

  test.test('Email', () {
    expect(validator.checkIfEmailIsCorrect("saasd@sadd") , false);
  });

  test.test('Email', () {
    expect(validator.checkIfEmailIsCorrect("long@long.lt") , true);
  });

  test.test('Email', () {
    expect(validator.checkIfEmailIsCorrect("lOn/+g1@long.lt") , false);
  });

  test.test('Password', () {
    expect(validator.checkIfPasswordIsCorrect("") , false);
  });

  test.test('Password', () {
    expect(validator.checkIfPasswordIsCorrect("a") , false);
  });

  test.test('Password', () {
    expect(validator.checkIfPasswordIsCorrect("latula456") , true);
  });

  test.test('Password', () {
    expect(validator.checkIfPasswordIsCorrect("Lat@ula!456") , true);
  });

  test.test('Weight', () {
    expect(validator.checkIfWeightIsCorrect("") , false);
  });

  test.test('Weight', () {
    expect(validator.checkIfWeightIsCorrect("sd") , false);
  });

  test.test('Weight', () {
    expect(validator.checkIfWeightIsCorrect("8000") , false);
  });

  test.test('Weight', () {
    expect(validator.checkIfWeightIsCorrect("2") , false);
  });

  test.test('Weight', () {
    expect(validator.checkIfWeightIsCorrect("50") , true);
  });

}