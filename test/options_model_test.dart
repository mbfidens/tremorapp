import 'package:flutter_test/flutter_test.dart' as test;
import 'package:test/test.dart';
import 'dart:convert';
import 'package:Tremor_App/Backend/Models/options_model.dart';
import 'package:flutter/foundation.dart';

void main() {
  OptionsModel model = OptionsModel(DateTime(1996, DateTime.october, 2), 100, <int>[9, 2, 3, 4, 5, 6]);
  OptionsModel newModel = OptionsModel.fromJson(json.decode(json.encode(model.toJson())));

  group('Conversion tests', () {
    test.test('model shall be convertible to json', () {
      expect(model.toJson() is Map<String, dynamic>, true);
    });

    test.test('Data should be reversible in conversion', () {
      expect(model.date == newModel.date, true);
    });

    test.test('Result should be reversible in conversion', () {
      expect(model.result == newModel.result, true);
    });

    test.test('Options should be reversible in conversion', () {
      expect(listEquals(model.options, newModel.options), true);
    });
  });
}
