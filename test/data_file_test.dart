import 'package:flutter_test/flutter_test.dart' as flutter_tests;
import 'package:test/test.dart';
import 'package:Tremor_App/Backend/data_file.dart';
import 'dart:io';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';
import 'dart:convert';
import 'dart:math';
import 'mocks/fake_path_provider.dart';

DataFile dataFile;
const String correctName = 'name.txt';
const String incorrectName = 'notName';
const String content = 'This is file content';
const String content2 = 'Rewritten content';
Map<String, dynamic> testMap = {'first': content, 'second': content2, 'id': 2};
Random random =  Random();

String getRandomFileName() => 'test_file_' + random.nextInt(99999).toString() + '.txt';
String getRandomFileDirectory() => 'test_directory_' + random.nextInt(99999).toString();

/*Test file shall be prepared in advance!*/
void main() async {
  dataFile = DataFile(correctName);
  flutter_tests.TestWidgetsFlutterBinding.ensureInitialized();
  await File(kApplicationDocumentsPath + r'\' + correctName).writeAsString(content);

  group('Initialization', () {
    setUp(() async {
      PathProviderPlatform.instance = FakePathProviderPlatform();
    });

    test('Local path is returned', () async {
      final String result = await dataFile.localPath;
      expect(result, kApplicationDocumentsPath);
    });

    test('File is returned', () async {
      expect(await dataFile.localFile is File, true);
    });

    test('File path filename within directory', () async {
      expect((await dataFile.localFile).path, kApplicationDocumentsPath + r'\' + correctName);
    });
  });

  group('Read and write operations', () {
    test('readString returns correct content', () async {
      expect(await dataFile.readString(), content);
    });

    test('clean sets content to empty', () async {
      await dataFile.clean();
      expect(await dataFile.readString(), '');
    });

    test('reWriteString rewrites everything when empty', () async {
      await dataFile.reWriteString(content);
      expect(await dataFile.readString(), content);
    });

    test('reWriteString rewrites everything when not empty', () async {
      await dataFile.reWriteString(content2);
      expect(await dataFile.readString(), content2);
    });

    test('appendString append content when not empty', () async {
      await dataFile.appendString(content);
      expect(await dataFile.readString(), content2 + content);
    });

    test('appendString append content when empty', () async {
      await dataFile.clean();
      await dataFile.appendString(content.toUpperCase());
      expect(await dataFile.readString(), content.toUpperCase());
    });
  });

  group('Json operations', () {
    test('reWriteJson rewrites file to json string', () async {
      await dataFile.reWriteString(content);
      await dataFile.reWriteJson(testMap);
      expect(jsonDecode(await dataFile.readString()), testMap);
    });

    test('writeJson rewrites file to json string', () async {
      await dataFile.reWriteString(content);
      await dataFile.reWriteJson(testMap);
      expect(jsonDecode(await dataFile.readString()), testMap);
    });
  });

  group('Error handling', () {
      test('correct names are not null', () async {
          expect(dataFile.checkIfNameIsCorrect(null), false);
      });

      test('writeJson throws exception if json is null', () async {
          expect((await dataFile.reWriteJson(null)), null);
      });
  });

  group('Write when file is non existent', () {
      test('File must appear on appendString', () async {
          DataFile newFile = DataFile(getRandomFileName());
          await newFile.appendString('empty');
          expect(await newFile.readString(), 'empty');

          await(await newFile.localFile).delete();
      });

      test('File must appear on reWriteString', () async {
          DataFile newFile = DataFile(getRandomFileName());
          await newFile.reWriteString('empty');
          expect(await newFile.readString(), 'empty');

          await(await newFile.localFile).delete();
      });

      test('File must appear on reWriteJson', () async {
          DataFile newFile = DataFile(getRandomFileName());
          await newFile.reWriteJson(testMap);
          expect(json.decode(await newFile.readString()), testMap);

          await(await newFile.localFile).delete();
      });
  });

  group('Write when folder is non existent', () {
      test('Directory and file must appear on writeString', () async {
          DataFile newFile = DataFile(getRandomFileDirectory() + r'\' + getRandomFileName());
          await newFile.reWriteString('empty');
          expect(await newFile.readString(), 'empty');

          String pathToDir = (await newFile.localFile).path;
          pathToDir = pathToDir.substring(0, pathToDir.lastIndexOf(r'\'));
          await (await newFile.localFile).delete();
          Directory(pathToDir).deleteSync();
      });
  });
}
